<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class ReminderMessage extends Mailable
{

    use Queueable,
        SerializesModels;

    public $Reminder;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($reminder)
    {
        $this->Reminder = $reminder;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('system@backoffice.infochi.net', 'infoChi Reminder')
            ->subject('infoChi Reminder - ' . $this->Reminder['subject'])
            ->view('emails.Reminder');
    }

}
