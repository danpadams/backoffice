<?php

namespace App\Http\Controllers\JaDaProperties;

use App\Http\Controllers\Controller;
use App\Models\RentalBooking;
use App\Models\UtilBill;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class UtilBillsController extends Controller
{
    private $request;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index($util_class_id = 0)
    {
        if (empty($util_class_id)) {
            $retval = UtilBill::with('UtilClass')
                ->get()->toArray();
        } else {
            $retval = UtilBill::with('UtilClass')
                ->where('util_class_id', '=', $util_class_id)
                ->get()->toArray();
        }

        foreach ($retval as $key => $value) {
            // Update Rental Platform
            $retval[$key]['util_class_value'] = $value['util_class']['name'];
            unset($retval[$key]['util_class']);
        }
        return $retval;
    }

//    public function booking($id)
//    {
//        $retval = RentalBooking::with('RentalPlatform')->where('id', '=', $id)->first()->toArray();
//
//        $retval['rental_platform_value'] = $retval['rental_platform']['value'];
//        unset($retval['rental_platform']);
//
//        return $retval;
//    }
//
//    public function save_data()
//    {
//        if (empty($this->request->id)) {
//            $Booking = new RentalBooking();
//        } else {
//            $id = $this->request->id;
//            $Booking = RentalBooking::find($id);
//        }
//        $Booking->setProperty('num_nights', $this->request)
//            ->setProperty('rental_platform_id', $this->request)
//            ->setProperty('access_code', $this->request)
//            ->setProperty('nightly_rate', $this->request)
//            ->setProperty('checkin', $this->request)
//            ->setProperty('checkout', $this->request)
//            ->setProperty('name', $this->request)
//            ->setProperty('pay_date', $this->request)
//            ->setProperty('service_fee', $this->request)
//            ->setProperty('taxes_lodging', $this->request)
//            ->setProperty('total_received', $this->request);
//        $Booking->save();
//        return $this->booking($Booking->id);
//    }
}