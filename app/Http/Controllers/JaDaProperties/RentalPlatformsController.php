<?php

namespace App\Http\Controllers\JaDaProperties;

use App\Http\Controllers\Controller;
use App\Models\RentalPlatform;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class RentalPlatformsController extends Controller
{
    private $request;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index()
    {
        $retval = RentalPlatform::get()->toArray();

        return $retval;
    }}