<?php

namespace App\Http\Controllers\JaDaProperties;

use App\Http\Controllers\Controller;
use App\Models\RentalBooking;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class RentalBookingsController extends Controller
{
    private $request;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index($rental_property_id)
    {
        $retval = RentalBooking::with('RentalPlatform')->
        where('rental_property_id', '=', $rental_property_id)
            ->where('cancelled','=','0')
            ->orderBy('checkin')
            ->get()->toArray();

        foreach ($retval as $key => $value) {
            // Update Rental Platform
            $retval[$key]['rental_platform_value'] = $value['rental_platform']['value'];
            unset($retval[$key]['rental_platform']);
        }
        return $retval;
    }

    public function booking($id)
    {
        $retval = RentalBooking::with('RentalPlatform')->where('id', '=', $id)->first()->toArray();

        $retval['rental_platform_value'] = $retval['rental_platform']['value'];
        unset($retval['rental_platform']);

        return $retval;
    }

    public function save_data()
    {
        if (empty($this->request->id)) {
            $Booking = new RentalBooking();
        } else {
            $id = $this->request->id;
            $Booking = RentalBooking::find($id);
        }
        $Booking->setProperty('num_nights', $this->request)
            ->setProperty('rental_platform_id', $this->request)
            ->setProperty('access_code', $this->request)
            ->setProperty('nightly_rate', $this->request)
            ->setProperty('checkin', $this->request)
            ->setProperty('checkout', $this->request)
            ->setProperty('name', $this->request)
            ->setProperty('pay_date', $this->request)
            ->setProperty('service_fee', $this->request)
            ->setProperty('taxes_lodging', $this->request)
            ->setProperty('total_received', $this->request);
        $Booking->save();
        return $this->booking($Booking->id);
    }
}