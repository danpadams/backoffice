<?php

namespace App\Http\Controllers\Tracker;

use App\Http\Controllers\Controller;
use App\Models\Ticket;
use App\Models\TrackerComment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\TrackerProject;

class CommentsController extends Controller
{
    private $request;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index($tracker_ticket_id = 0)
    {
        $Comments = TrackerComment::where('tracker_ticket_id', '=', $tracker_ticket_id)->orderBy('created_at','desc');
        $retval = $Comments->get()->toArray();

        return $retval;
    }

    public function save_data()
    {
        $TrackerComment = new TrackerComment();
        $TrackerComment->setProperty('tracker_ticket_id', $this->request)
            ->setProperty('comment', $this->request);
        $TrackerComment->user_id = 1;
        $TrackerComment->save();

        return $TrackerComment;
    }

}