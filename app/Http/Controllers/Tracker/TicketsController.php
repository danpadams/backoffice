<?php

namespace App\Http\Controllers\Tracker;

use App\Http\Controllers\Controller;
use App\Models\Ticket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\TrackerProject;

class TicketsController extends Controller
{
    private $request;

    private $ListProjects;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index($tracker_project_id = 0)
    {
        $Tickets = Ticket::with('TrackerStatus')->where('tracker_status_id', '!=', 4);

        if (!empty($tracker_project_id)) {
            $queryBuilder = TrackerProject::where('id', '=', $tracker_project_id);
            $Project = $queryBuilder->first();

            $prefix = $Project->prefix . '%';
            $Tickets = $Tickets->where('code', 'LIKE', $prefix);
        }
        $retval = $Tickets->get()->toArray();

        for ($i = 0; $i < sizeof($retval); $i++) {
            $retval[$i]['status_value'] = $retval[$i]['tracker_status']['value'];
            $retval[$i]['status_id'] = $retval[$i]['tracker_status']['id'];
            unset($retval[$i]['tracker_status']);
        }

        return $retval;
    }

    public function ticket($code)
    {
        $retval = Ticket::with('TrackerStatus')
            ->with('TrackerReason')
            ->where('code', '=', $code)
            ->first()->toArray();

        if (!empty($retval['tracker_reason'])) {
            $retval['reason_value'] = $retval['tracker_reason']['value'];
            $retval['reason_id'] = $retval['tracker_reason']['id'];
        }
        unset($retval['tracker_reason']);

        $retval['status_value'] = $retval['tracker_status']['value'];
        $retval['status_id'] = $retval['tracker_status']['id'];
        unset($retval['tracker_status']);

        $retval['optStatus'] = $this->statuses();
            // With Project
        $retval['optProjects'] = $this->projects();
        $retval['Project'] = $this->getProject($retval['code']);
        if (!empty($retval['Project'])) {
            $retval['project_id'] = $retval['Project']['id'];
        }
        return $retval;
    }

    public function save_data()
    {
        if (empty($this->request->id)) {
            $Ticket = new Ticket();
            $Ticket->code = $this->request->Prefix . '-' . $this->get_next_ticket($this->request->Prefix);
        } else {
            $Ticket = Ticket::where('id', '=', $this->request->id)->first();
        }
        $Ticket->setProperty('title', $this->request)
            ->setProperty('tracker_status_id', $this->request)
            ->setProperty('cost', $this->request)
            ->setProperty('description', $this->request);
        $Ticket->save();

        return $this->ticket($Ticket->code);
    }

    public function statuses()
    {
        return [
            ['id' => 1, 'value' => 'Open'],
            ['id' => 2, 'value' => 'Ready'],
            ['id' => 3, 'value' => 'In Progress'],
            ['id' => 4, 'value' => 'Done']
        ];
    }

    private function projects()
    {
        if (empty($this->ListProjects)) {
            $this->ListProjects = (new TrackerProject())->get();
        }

        return $this->ListProjects;
    }

    private function getProject($code)
    {
        $Projects = $this->projects();

        $Prefix = strtok($code, '-');

        $retval = [];
        for ($i = 0; $i < sizeof($Projects); $i++) {
            if ($Projects[$i]['prefix'] == $Prefix) {
                $retval = $Projects[$i];
            }
        }
        return $retval;
    }

    public function get_next_ticket($prefix)
    {
        $Codes = Ticket::where('code', 'like', $prefix . '%')->get(['code']);
        $next = 0;
        foreach ($Codes->toArray() as $Code) {
            $code = substr($Code['code'], strlen($prefix) + 1);
            if ($code > $next) {
                $next = $code;
            }
        }

        $next++;
        return $next;
    }
}