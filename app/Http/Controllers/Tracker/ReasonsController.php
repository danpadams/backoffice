<?php

namespace App\Http\Controllers\Tracker;

use App\Http\Controllers\Controller;
use App\Models\Ticket;
use App\Models\TrackerProject;
use App\Models\TrackerReason;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ReasonsController extends Controller
{
    private $request;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index()
    {
        $retval = TrackerReason::get();

        return $retval;
    }
}