<?php

namespace App\Http\Controllers\Tracker;

use App\Http\Controllers\Controller;
use App\Models\TrackerFrequency;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class FrequencyController extends Controller
{
    private $request;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index()
    {
        $retval = TrackerFrequency::get()->toArray();
        foreach ($retval as $key => $ret) {
            $retval[$key]['value'] = $ret['title'];
        }
        return $retval;
    }
}