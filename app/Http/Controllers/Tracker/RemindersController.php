<?php

namespace App\Http\Controllers\Tracker;

use App\Http\Controllers\Controller;
use App\Models\TrackerReminder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class RemindersController extends Controller
{
    private $request;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /*
     * Generate list for AJAX to display potential reminders
     */
    public function index()
    {
        $retval = TrackerReminder::with('TrackerFrequency')
            ->where('next', '>=', date('Y-m-d'))
//            ->orderBy('processed')
            ->orderBy('next')
            ->get()->toArray();

        for ($i = 0; $i < sizeof($retval); $i++) {
            $retval[$i]['Frequency'] = $retval[$i]['tracker_frequency']['title'];
            unset($retval[$i]['tracker_frequency']);
            $retval[$i]['nextDate'] = date('F j, Y', strtotime($retval[$i]['next']));
        }

        return $retval;
    }

    public function get_reminder($id)
    {
        $reminder = TrackerReminder::with('TrackerFrequency')->where('id', '=', $id)->first()->toArray();
        $reminder['Frequency'] = $reminder['tracker_frequency']['title'];
        unset($reminder['tracker_frequency']);

        return $reminder;
    }

    public function post_reminder()
    {
        if (empty($this->request->id)) {
            $reminder = new TrackerReminder();
        } else {
            $reminder = TrackerReminder::where('id', '=', $this->request->id)->first();
        }
        $reminder->setProperty('subject', $this->request)
            ->setProperty('body', $this->request)
            ->setProperty('next', $this->request)
            ->setProperty('tracker_frequency_id', $this->request);
        $reminder->user_id = $this->request->session()->get('user.id');
        if (empty($reminder->user_id)) {
            $reminder->user_id = 1;
        }
        $reminder->processed = '0';
        $reminder->save();
        return $this->get_reminder($reminder->id);
    }
}