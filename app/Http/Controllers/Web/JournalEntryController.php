<?php

namespace App\Http\Controllers\Web;

use App\Models\Journal;
use App\Models\JournalEntry;
//use App\Models\AccountTransaction;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class JournalEntryController extends Controller
{
    private $request;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index($journal_id)
    {
        $report = [
            'head' => [
                [
                    'name' => 'Date',
                    'var_name' => 'actdate',
                    'sortable' => 1
                ],
                [
                    'name' => 'Entry Subject',
                    'var_name' => 'subject',
                    'link_type' => 3,
                    'url_item' => 'journal',
                    'col_id' => 'id'
                ], [
                    'name' => 'Brief',
                    'var_name' => 'Brief'
                ]
            ]
        ];
        $report['report'] = JournalEntry::orderBy('actdate', 'desc')->where('journal_id', '=', $journal_id)->get()->toArray();
        $report['report'] = $this->genBriefs($report['report'], true);

        return $report;
    }

    public function view($journal_entry_id)
    {
        if ($journal_entry_id) {
            $journal_entry = JournalEntry::find($journal_entry_id);
        } else {
            $journal_entry = $this->getDefault();
        }
        return $journal_entry;
    }

    public function saveData($journal_entry_id)
    {
        if ($journal_entry_id) {
            /**
             * @var JournalEntry
             */
            $journal_entry = JournalEntry::find($journal_entry_id);
        } else {
            $journal_entry = new JournalEntry();
        }
        $journal_entry
            ->setProperty('body', $this->request)
            ->setProperty('actdate', $this->request)
            ->setProperty('subject', $this->request)
            ->setProperty('miles', $this->request)
            ->setProperty('journal_id', $this->request);
        $journal_entry->save();

        $this->evalActdate($this->request->journal_id, $this->request->actdate);
        return $journal_entry;
    }

    private function genBriefs($report, $remove = false)
    {
        foreach ($report as $key => $item) {
            $report[$key]['Brief'] = substr($item['body'], 0, 100);
            if ($remove) {
                unset($report[$key]['body']);
            }
        }

        return $report;
    }

    private function getDefault()
    {
        $retval = [
            'miles' => 0,
            'actdate' => date('Y-m-d'),
            'subject' => 'Default Subject'
        ];
        return $retval;
    }

    private function evalActdate($journal_id, $actdate)
    {
        $journal = Journal::find($journal_id);
        if ($actdate > $journal->actdate) {
            $journal->actdate = $actdate;
            $journal->save();
        }
    }
}
