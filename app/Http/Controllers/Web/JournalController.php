<?php

namespace App\Http\Controllers\Web;

use App\Models\Journal;
//use App\Models\AccountTransaction;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class JournalController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    function index()
    {
        $report = [
            'head' => [
                [
                    'name' => 'Journal Topic',
                    'var_name' => 'name',
                    'sortable' => 1,
                    'link_type' => 3,
                    'url_item' => 'journals',
                    'col_id' => 'id'
                ],
                [
                    'name' => 'Updated',
                    'var_name' => 'actdate',
                    'sortable' => 1
                ]
            ]
        ];
        $report['report'] = Journal::orderBy('name')->get();

        return $report;
    }
    
}
