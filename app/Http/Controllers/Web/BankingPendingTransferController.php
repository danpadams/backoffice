<?php

namespace App\Http\Controllers\Web;

use App\Models\BankingPendingTransfer as myObject;
use App\Http\Controllers\Controller;
use App\Models\BankingTransaction;
use App\Models\BankingTransfer;
use App\Models\LogWithholding;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class BankingPendingTransferController extends Controller
{
    private $request;

    /**
     *
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index()
    {
        $retval = [];
        $response = myObject::with('BankingAccountFrom')
            ->with('BankingAccountTo')
            ->where('processed', 0)
            ->with('BankingClass')
            ->get();

        return $response;
    }

    public function entry($id)
    {
        $retval = [];
        $response = myObject::with('BankingAccountFrom')
            ->with('BankingAccountTo')
            ->where('id', $id)
            ->with('BankingClass')
            ->first()->toArray();

        $response['banking_class_value'] = $response['banking_class']['descrip'];
        $response['banking_account_from_value'] = $response['banking_account_from']['name'];
        $response['banking_account_to_value'] = $response['banking_account_to']['name'];

        return $response;
    }

    public function save_data()
    {
        if (empty($this->request->id)) {
            $myObject = new myObject();
        } else {
            $myObject = myObject::where('id', $this->request->id)->first();
        }
        $myObject->setProperty('banking_account_from_id', $this->request)
            ->setProperty('banking_account_to_id', $this->request)
            ->setProperty('banking_class_id', $this->request)
            ->setProperty('amount', $this->request)
            ->setProperty('name', $this->request);
        $myObject->save();
    }

    public function summary()
    {
        $retval = [];
        $ret = [];
        $Data = $this->index();
        $data = $Data->toArray();
        foreach ($data as $key => $value) {
            $compositeKey = $value['banking_account_from_id'] . '-' . $value['banking_account_to_id'];
            $element = $value;
            if (!empty($ret[$compositeKey])) {
                $element['amount'] += $ret[$compositeKey]['amount'];
            }
            $ret[$compositeKey] = $element;
        }

        // Cleanup each item
        foreach ($ret as $key => $value) {
            // Reverse Values and Accounts
            if ($value['amount'] < 0) {
                $ret[$key]['banking_account_from_id'] = $value['banking_account_to_id'];
                $ret[$key]['banking_account_to_id'] = $value['banking_account_from_id'];
                $ret[$key]['banking_account_from'] = $value['banking_account_to'];
                $ret[$key]['banking_account_to'] = $value['banking_account_from'];
                $ret[$key]['amount'] = $value['amount'] * -1;
            }
        }

        foreach ($ret as $key => $value) {
            $retval[] = $ret[$key];
        }
        $this->addOpposites($retval);
        return $retval;
    }

    private function addOpposites(&$data)
    {
        // Grab List of compositeKey
        $cKeys = [];
        foreach ($data as $key => $value) {
            $compositeKey = $value['banking_account_from_id'] . '-' . $value['banking_account_to_id'];
            $cKeys[$compositeKey] = $key;
        }

        // Check the first occurance of each opposite in the list
        foreach ($data as $key => $value) {
            if (!empty($data[$key])) {
                continue;
            }
            $cKeyOpp = $value['banking_account_to_id'] . '-' . $value['banking_account_from_id'];
            if (!empty($cKeys[$cKeyOpp])) {
                $data[$key]['amount'] -= $data[$cKeys[$cKeyOpp]]['amount'];
                unset($data[$cKeys[$cKeyOpp]]);
            }
        }
    }

    /**
     * Process Pending Transfers
     *
     * @return array
     */
    public function process()
    {
        $Data = $this->request->toArray();
        Log::info(__METHOD__ . ':' . __LINE__ . ' ' . print_r($Data['log_withholding'], true));
        foreach ($Data['log_withholding'] as $key => $value) {
            // Save LogWithholding Entry
            $LogWithholding = new LogWithholding();
            $LogWithholding->amount = $value['amount'];
            $LogWithholding->transfer_date = date('Y-m-d');
            $LogWithholding->descrip = $value['name'];
            $LogWithholding->banking_class_id = $value['banking_class_id'];
            $LogWithholding->save();

            // Save Processed Data
            $PendingTransfer = (new myObject())->where('id', $value['id'])->first();
            $PendingTransfer->processed = 1;
            $PendingTransfer->save();
        }

        Log::info(__METHOD__ . ':' . __LINE__ . ' ' . print_r($Data['transfer'], true));
        foreach ($Data['transfer'] as $key => $value) {
            // From Transaction
            $Transaction = new BankingTransaction();
            $Transaction->amount = $value['amount'] * -1;
            $Transaction->account = $value['banking_account_from_id'];
            $Transaction->entrydate = date('Y-m-d');
            $Transaction->payee = 'Log Withholding Transaction';
            $Transaction->cat = 14;
            $Transaction->class = 8;
            $Transaction->comment = '';
            $Transaction->status = 0;
            $Transaction->save();
            $id_from = $Transaction->id;
            Log::info(__METHOD__ . ':' . __LINE__ . ' ' . print_r('$id_from: ' . $id_from, true));

            // TO Transaction
            $Transaction = new BankingTransaction();
            $Transaction->amount = $value['amount'];
            $Transaction->account = $value['banking_account_to_id'];
            $Transaction->entrydate = date('Y-m-d');
            $Transaction->payee = 'Log Withholding Transaction';
            $Transaction->cat = 14;
            $Transaction->class = 8;
            $Transaction->comment = '';
            $Transaction->status = 0;
            $Transaction->save();
            $id_to = $Transaction->id;
            Log::info(__METHOD__ . ':' . __LINE__ . ' ' . print_r('$id_to: ' . $id_to, true));

            // Create Transfer Link
            $Transfer = new BankingTransfer();
            $Transfer->trans_num_from = $id_from;
            $Transfer->trans_num_to = $id_to;
            $Transfer->save();
            Log::info(__METHOD__ . ':' . __LINE__ . ' ' . print_r('$transfer_id: ' . $Transfer->id, true));
        }

        return [];
    }
}
