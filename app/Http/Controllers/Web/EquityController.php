<?php

namespace App\Http\Controllers\Web;

use App\Models\Ticker;
use App\Models\TickerNotes;
use App\Models\MktIndex;
use App\Models\MktIndexTickers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class EquityController extends Controller
{

    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function getTickers()
    {
        $Tickers_Data = (new Ticker())->where('inactive', 0)
            ->get()->toArray();
        $this->calcTrend($Tickers_Data);

        $Tickers = [
            'head' => [
                [
                    'name' => 'Asof',
                    'sortable' => 1,
                    'var_name' => 'asof'
                ], [
                    'name' => 'Company Name (Ticker)',
                    'col_id' => 'ticker',
                    'link_type' => 3,
                    'url_item' => 'invest',
                    'var_name' => 'Name',
                    'sortable' => 1,

                ], [
                    'name' => 'Price',
                    'var_name' => 'price',
                    'css' => 'Diff_css'
                ], [
                    'name' => 'Diff %',
                    'var_name' => 'diff_pct',
                    'css' => 'Diff_css',
                    'suffix' => '%'
                ], [
                    'name' => 'MA 50 %',
                    'var_name' => 'dma_50_pct',
                    'css' => 'Dma_50_css',
                    'suffix' => '%'
                ], [
                    'name' => 'MA 200 %',
                    'var_name' => 'dma_200_pct',
                    'css' => 'Dma_200_css',
                    'suffix' => '%'
                ], [
                    'name' => 'Trend',
                    'var_name' => 'Up_50_200',
                    'css' => 'Up_50_200_css'
                ]
            ],
            'report' => $Tickers_Data
        ];
        $this->calcTrend($Tickers_Data);

        return $Tickers;
    }

    public function getTicker($ticker)
    {
        $Ticker = Ticker::where('ticker', '=', $ticker)
            ->first();
        return $Ticker;
    }

    public function getMktIndex()
    {
        $report = [
            'head' => [
                [
                    'name' => 'Group Name',
                    'var_name' => 'name',
                    'sortable' => 1,
                    'link_type' => 3,
                    'url_item' => 'invest/mkt-index',
                    'col_id' => 'id'
                ],
                [
                    'name' => 'Updated',
                    'var_name' => 'actdate'
                ]
            ]
        ];
        $report['report'] = MktIndex::orderBy('name')->get();

        return $report;
    }

    public function getMktIndexData($index_num)
    {
        $List = [];
        $Tickers_Index = MktIndexTickers::where('ticker_index_id', '=', $index_num)->get()->toArray();
        foreach ($Tickers_Index as $index) {
            $List[] = $index['ticker'];
        }
        $Tickers_Data = Ticker::where('inactive', '=', 0)->wherein('ticker',$List)->orderBy('name', 'asc')
            ->get()->toArray();
        $this->calcTrend($Tickers_Data);

        $Tickers = [
            'head' => [
                [
                    'name' => 'Asof',
                    'sortable' => 1,
                    'var_name' => 'asof'
                ], [
                    'name' => 'Company Name (Ticker)',
                    'col_id' => 'ticker',
                    'link_type' => 3,
                    'url_item' => 'invest',
                    'var_name' => 'Name',
                    'sortable' => 1,

                ], [
                    'name' => 'Price',
                    'var_name' => 'price',
                    'css' => 'Diff_css'
                ], [
                    'name' => 'Diff %',
                    'var_name' => 'diff_pct',
                    'css' => 'Diff_css',
                    'suffix' => '%'
                ], [
                    'name' => 'MA 50 %',
                    'var_name' => 'dma_50_pct',
                    'css' => 'Dma_50_css',
                    'suffix' => '%'
                ], [
                    'name' => 'MA 200 %',
                    'var_name' => 'dma_200_pct',
                    'css' => 'Dma_200_css',
                    'suffix' => '%'
                ], [
                    'name' => 'Trend',
                    'var_name' => 'Up_50_200',
                    'css' => 'Up_50_200_css'
                ]
            ],
            'report' => $Tickers_Data
        ];
        $this->calcTrend($Tickers_Data);

        return $Tickers;
    }

    public function getNotes($ticker)
    {
        $Notes = TickerNotes::where('ticker', '=', $ticker)
            ->get();
        foreach ($Notes as $Note) {
            /** @var $Note ->created Carbon */
            $Note->created = $Note->created_at->format('m/d/Y');
        }
        return $Notes;
    }

    public function saveData()
    {
        if ($this->request->id) {
            $Ticker = Ticker::find($this->request->id);
        } else {
            $Ticker = new Ticker();
            $Ticker->refresh = '0000-00-00';
            $Ticker->setProperty('ticker', $this->request);
        }
        $Ticker->setProperty('name', $this->request);
        $Ticker->save();
        return $Ticker;
    }

    private function calcTrend(&$Data)
    {
        foreach ($Data as $key => $dat) {
            $Data[$key]['Name'] = $Data[$key]['name'] . ' (' . $Data[$key]['ticker'] . ')';
            if ($Data[$key]['ma_50'] > $Data[$key]['ma_200']) {
                $Data[$key]['Up_50_200'] = 'Up';
                $Data[$key]['Up_50_200_css'] = 'text-green';
            } else {
                $Data[$key]['Up_50_200'] = 'Down';
                $Data[$key]['Up_50_200_css'] = 'text-red';
            }

            if ($Data[$key]['diff'] < 0) {
                $Data[$key]['Diff_css'] = 'text-red';
            } else {
                $Data[$key]['Diff_css'] = 'text-green';
            }

            if ($Data[$key]['ma_50'] < 0) {
                $Data[$key]['Dma_50_css'] = 'text-red';
            } else {
                $Data[$key]['Dma_50_css'] = 'text-green';
            }

            if ($Data[$key]['ma_200'] < 0) {
                $Data[$key]['Dma_200_css'] = 'text-red';
            } else {
                $Data[$key]['Dma_200_css'] = 'text-green';
            }

        }
    }
}
