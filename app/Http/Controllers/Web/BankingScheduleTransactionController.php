<?php

namespace App\Http\Controllers\Web;

use App\Models\BankingScheduleTransaction as myObject;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class BankingScheduleTransactionController extends Controller
{
    private $request;

    /**
     *
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index()
    {
        $retval = [];
        $response = myObject::with('BankingAccount')
            ->with('BankingCategory')
            ->with('BankingClass')
            ->orderBy('next')
            ->get()->toArray();

        foreach ($response as $element) {
            // BankingAccount
            $element['account_name'] = $element['banking_account']['name'];
            unset($element['banking_account']);

            $element['category_name'] = $element['banking_category']['descrip'];
            unset($element['banking_category']);

            $element['class_name'] = $element['banking_class']['descrip'];
            unset($element['banking_class']);

            $retval[] = $element;
        }

        return $retval;
    }
}
