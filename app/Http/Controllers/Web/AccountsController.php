<?php

namespace App\Http\Controllers\Web;

use App\Models\BankingAccount;
use App\Models\AccountTransaction;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class AccountsController extends Controller
{
    private $request;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index()
    {
        $accounts = [];
        $accounts = BankingAccount::get();
        $accounts = $accounts->toArray();
        $retval = array_merge($this->report_index(), $accounts);
        Log::info(print_r($retval, true));
        return $retval;
    }

    /**
     * @param $account_id
     * @return array
     */
    public function edit($account_id)
    {
        $account = BankingAccount::where('id', '=', $account_id)->first();
        return $account;
    }

    public function save()
    {
        $account_id = $this->request->id;
        if (!empty($account_id)) {
            $account = BankingAccount::where('id', '=', $account_id)->first();
        } else {
            $account = new BankingAccount();
        }

        // Set Properties & Save
        $account->setProperty('name', $this->request);
        $account->save();
        return $account;
    }

    /**
     * @param $account_id
     * @return array
     */
    public function view($account_id)
    {
        $activity = [];
        $activity = AccountTransaction::where('account_id', '=', $account_id)->orderBy('actdate')->get();
        $balance = 0;
        foreach ($activity as $key => $act) {
            $balance = $balance + $act['amount'];
            $activity[$key]['balance'] = $balance;
        }
        $this->recordBalance($account_id, $balance);
        return $activity;
    }

    public function create_entry()
    {
        $data = new AccountTransaction();
        $data
            ->setProperty('account_id', $this->request)
            ->setProperty('name', $this->request)
            ->setProperty('amount', $this->request)
            ->setProperty('actdate', $this->request);
        $data->save();
    }

    private function recordBalance($account_id, $balance)
    {
        $account = BankingAccount::where('id', '=', $account_id)->first();
        $account->balance = $balance;
        $account->save();
    }

    private function report_index()
    {
        $report = [
            'head' => [
                '1' => [
                    'name' => 'BankingAccount Name',
                    'sortable' => 1,
                    'col_id' => 'id',
                    'var_name' => 'name',
                    'url_item' => 'layaway',
                    'link_type' => 3
                ],
                '2' => [
                    'name' => 'Balance',
                    'var_name' => 'Balance'
                ]
            ]
        ];
        $report['report'] = BankingAccount::get();
        $report['report'] = $report['report']->toArray();
        foreach ($report['report'] as $key => $item) {
            $report['report'][$key]['Balance'] = '$' . number_format($item['balance'], 2);
        }

        return $report;
    }

}
