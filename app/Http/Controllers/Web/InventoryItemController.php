<?php

namespace App\Http\Controllers\Web;

use App\Models\InventoryItem;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class InventoryItemController extends Controller
{
    private $request;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index($inventory_id)
    {
        $report = [
            'head' => [
                [
                    'name' => 'ActDate',
                    'var_name' => 'actdate',
                    'sortable' => 1
                ],
                [
                    'name' => 'Item Name',
                    'sortable' => 1,
                    'var_name' => 'name',
                    'url_item' => 'inventory/edit',
                    'col_id' => 'id',
                    'link_type' => 3
                ],
                [
                    'name' => 'Qty',
                    'var_name' => 'qty'
                ],
                [
                    'name' => 'Qty Partial',
                    'var_name' => 'qty_partial'
                ],
                [
                    'name' => 'Costco',
                    'var_name' => 'costco',
                    'link_type' => 2
                ]

            ]
        ];
        $report['report'] = InventoryItem::where('inventory_groups_id', '=', $inventory_id)->get();
        $report['report'] = $report['report']->toArray();
        foreach ($report['report'] as $key => $item) {
            $report['report'][$key]['actdate'] = date('Y-m-d', strtotime($item['updated_at']));
        }

        return $report;
    }

    public function view($inventory_item_id)
    {
        if ($inventory_item_id) {
            $inventory_item = InventoryItem::find($inventory_item_id);
        } else {
            $inventory_item = $this->getDefault();
        }
        return $inventory_item;
    }

    public function saveData($inventory_item_id)
    {
        if ($inventory_item_id) {
            /**
             * @var InventoryItem
             */
            $inventory_item = InventoryItem::find($inventory_item_id);
        } else {
            $inventory_item = new InventoryItem();
        }
        $inventory_item
            ->setProperty('inventory_groups_id', $this->request)
            ->setProperty('name', $this->request)
            ->setProperty('qty', $this->request)
            ->setProperty('qty_partial', $this->request)
            ->setProperty('costco', $this->request);
        if (!empty($inventory_item->getDirty())) {
            $this->updateGroup($this->request->inventory_groups_id);
        }
        $inventory_item->save();

//        $this->evalActdate($this->request->journal_id, $this->request->actdate);
        return $inventory_item;
    }


    private function getDefault()
    {
        $retval = [
            'name' => 'Inventory Item',
            'qty' => 0,
            'qty_partial' => 0,
            'costco' => 0
        ];
        return $retval;
    }

    private function updateGroup($inventory_groups_id)
    {

    }
}
