<?php

namespace App\Http\Controllers\Web;

use App\Models\InventoryGroup;
use App\Models\InventoryItem;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class InventoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function index()
    {
        $accounts = [];
        $accounts = InventoryGroup::get()->toArray();
        $retval = array_merge($this->report_index(), $accounts);
        Log::info(print_r($retval, true));
        return $retval;
    }

    private function recordBalance($account_id, $balance)
    {
        $account = InventoryGroup::where('id', '=', $account_id)->first();
        $account->balance = $balance;
        $account->save();
    }

    private function report_index()
    {
        $report = [
            'head' => [
                [
                    'name' => 'Group Name',
                    'sortable' => 1,
                    'col_id' => 'id',
                    'var_name' => 'name',
                    'url_item' => 'inventory',
                    'link_type' => 3
                ],

            ]
        ];
        $report['report'] = InventoryGroup::get();
        $report['report'] = $report['report']->toArray();

        return $report;
    }

    public function view($inventory_id)
    {
        $report = [
            'head' => [
                [
                    'name' => 'ActDate',
                    'var_name' => 'actdate',
                    'sortable' => 1
                ],
                [
                    'name' => 'Item Name',
                    'sortable' => 1,
                    'var_name' => 'name',
                    'url_item' => 'inventory/edit',
                    'col_id' => 'id',
                    'link_type' => 3
                ],
                [
                    'name' => 'Qty',
                    'var_name' => 'qty'
                ],
                [
                    'name' => 'Qty Partial',
                    'var_name' => 'qty_partial'
                ],
                [
                    'name' => 'Costco',
                    'var_name' => 'costco',
                    'link_type' => 2
                ]

            ]
        ];
        $report['report'] = InventoryItem::where('inventory_groups_id', '=', $inventory_id)->get();
        $report['report'] = $report['report']->toArray();
        foreach ($report['report'] as $key => $item) {
            $report['report'][$key]['actdate'] = date('Y-m-d', strtotime($item['updated_at']));
        }

        return $report;
    }
}
