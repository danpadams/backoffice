<?php

namespace App\Http\Controllers\Banking;

use App\Component\BankingTransfer as TransferComponenet;
use App\Models\BankingSplit;
use App\Models\BankingTransaction;
use App\Models\BankingTransfer;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    private $request;

    /**
     *
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function entry($transaction_id)
    {
        $retval = BankingTransaction::with('TransferFrom')
            ->with('TransferTo')
            ->where('id', '=', $transaction_id)
            ->first()
            ->toArray();

        if (!empty($retval['transfer_from']['trans_num_to'])) {
            $meta_data['transfer_to'] = $retval['transfer_from']['trans_num_to'];
        } else {
            $meta_data['transfer_to'] = 0;
        }
        if (!empty($retval['transfer_to']['trans_num_from'])) {
            $meta_data['transfer_from'] = $retval['transfer_to']['trans_num_from'];
        } else {
            $meta_data['transfer_from'] = 0;
        }
        $retval['meta_data'] = $meta_data;
        unset($retval['transfer_from']);
        unset($retval['transfer_to']);

        return $retval;
    }

    public function split_data($transaction_id)
    {
        $retval = [];
        $retval = BankingSplit::where('trans_num', '=', $transaction_id)
            ->get()->toArray();

        return $retval;
    }

    public function save_data()
    {
        /** @var BankingTransaction $data */
        if (empty($this->request->id)) {
            $data = new BankingTransaction();
            $oldAmt = 0;
        } else {
            $data = BankingTransaction::where('id', '=', $this->request->id)->first();
            $oldAmt = $data->amount;
        }

        $data->setProperty('entrydate', $this->request)
            ->setProperty('amount', $this->request)
            ->setProperty('num', $this->request)
            ->setProperty('comment', $this->request)
            ->setProperty('payee', $this->request)
            ->setProperty('class', $this->request);
        $data->cat = $this->request->category;
        $data->account = $this->request->acct;
        if (!empty($this->request->status) && is_numeric($this->request->status)) {
            $data->status = $this->request->status;
        } else {
            $data->status = '0';
        }

        $data->save();
        Log::info(__METHOD__ . ':' . __LINE__ . ' Banking Transaction - ' . $data->id);
        if (!empty($this->request->CatSize)) {
            $this->saveSplitData($data->id);
        }
        Log::info(__METHOD__ . ':' . __LINE__ . ' ' . print_r($this->request->toarray(), true));
        if (($this->request->category == 14) && ($oldAmt != $data->amount)) {
            Log::info(__METHOD__ . ':' . __LINE__ . ' ' . print_r('', true));
            $this->updTransfer($data, $oldAmt);
        }
        return $data;
    }

    public function save_transfer()
    {
         if (empty($this->request->accountFrom)) {
            return [];
        }
        if (empty($this->request->accountTo)) {
            return [];
        }
        if (empty($this->request->amount)) {
            return [];
        }
        if (empty($this->request->payee)) {
            return [];
        }
        if (empty($this->request->entrydate)) {
            return [];
        }

        // Create From Transaction
        $params = [
            'account' => $this->request->accountFrom,
            'amount' => $this->request->amount,
            'payee' => $this->request->payee,
            'entrydate' => $this->request->entrydate
        ];
        $idTo = TransferComponenet::saveTransaction($params);

        // Create To Transaction
        $params = [
            'account' => $this->request->accountTo,
            'amount' => $this->request->amount * -1,
            'payee' => $this->request->payee,
            'entrydate' => $this->request->entrydate
        ];
        $idFrom = TransferComponenet::saveTransaction($params);

        // Create Join for editing
        $params = [
            'idFrom' => $idFrom,
            'idTo' => $idTo
        ];
        TransferComponenet::saveTransfer($params);
        return $params;
    }

    public function delete()
    {
        BankingTransaction::find($this->request->id)->delete();
        (new BankingSplit())->where('trans_num', '=', $this->request->idx)->delete();
    }

    private function saveSplitData($transaction_id)
    {
        // Delete Existing Items
        (new BankingSplit())->where('trans_num', '=', $transaction_id)->delete();

        // Add each item in split
        $splits = [];
        for ($i = 1; $i <= $this->request->CatSize; $i++) {
            $varCategory = 'Category_' . $i;
            $varAmount = 'Amount_' . $i;
            $varClass = 'Class_' . $i;
            if (!empty($this->request->$varCategory) || !empty($this->request->$varAmount)) {
                $BankingSplit = new BankingSplit();
                $BankingSplit->trans_num = $transaction_id;
                if (empty($this->request->$varCategory)) {
                    $this->request->$varCategory = 19;
                } else {
                    $BankingSplit->category = $this->request->$varCategory;
                }
                if (empty($this->request->$varAmount)) {
                    $this->request->$varAmount = 0;
                } else {
                    $BankingSplit->amount = $this->request->$varAmount;
                }
                if (empty($this->request->$varClass)) {
                    $BankingSplit->class = 8;
                } else {
                    $BankingSplit->class = $this->request->$varClass;
                }
                $varComment = 'Comment_' . $i;
                $BankingSplit->comment = $this->request->$varComment;
                $combined_key = $transaction_id . '-' . $this->request->$varCategory . '-' . $this->request->$varClass;
                $splits[$combined_key] = $BankingSplit->amount;

                $BankingSplit->save();
                Log::info(__METHOD__ . ':' . __LINE__ . ' ' . print_r($BankingSplit->toArray(), true));
                Log::info(__METHOD__ . ':' . __LINE__ . ' Split Item - ' . $i . '/' . $this->request->CatSize);
            } else {
                Log::info(__METHOD__ . ':' . __LINE__ . ' Split Item Skipped - ' . $i . '/' . $this->request->CatSize);
            }
        }
    }

    /**
     * Find and update the opposite transaction, for linked
     *
     * @param $data
     * @param $oldAmt
     * @return void
     */
    private function updTransfer($data, $oldAmt)
    {
        Log::info(__METHOD__ . ':' . __LINE__ . ' ' . print_r('', true));
        $transfer = (new BankingTransfer())->where('trans_num_from', $data->id)
            ->orWhere('trans_num_to', $data->id)
            ->first();
        Log::info(__METHOD__ . ':' . __LINE__ . ' ' . print_r($transfer->toArray(), true));
        if (!empty($transfer->trans_num_from)) {
            if ($transfer->trans_num_from == $data->id) {
                $otherId = $transfer->trans_num_to;
            } else {
                $otherId = $transfer->trans_num_from;
            }
            $newAmt = $data->amount * -1;
            BankingTransaction::where('id', $otherId)
                ->update(['amount' => $newAmt]);
        }
    }
}
