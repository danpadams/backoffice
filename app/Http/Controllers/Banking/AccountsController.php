<?php

namespace App\Http\Controllers\Banking;

use App\Models\BankingAccount;
use App\Models\BankingAccount as myObject;
use App\Http\Controllers\Controller;
use App\Models\BankingTransaction;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class AccountsController extends Controller
{
    private $request;

    /**
     *
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index()
    {
        $retval = myObject::with('BankingAccountType')
            ->where('client', '=', 1)
            ->where('open', '=', 1)
            ->whereIn('type', [1, 2])
            ->orderBy('type')
            ->orderBy('name')
            ->get()
            ->toArray();

        foreach ($retval as $key => $value) {
            $retval[$key]['type_id'] = $value['type'];
            $retval[$key]['type_value'] = $value['banking_account_type']['name'];
            unset($retval[$key]['type']);
            unset($retval[$key]['banking_account_type']);
        }

        return $retval;
    }

    public function get_balance($account_id)
    {
        $futureBalance = BankingTransaction
            ::where('account', '=', $account_id)
            ->sum('amount');
        $currentBalance = BankingTransaction
            ::where('account', '=', $account_id)
            ->where('entrydate', '<', date('Y-m-d'))
            ->sum('amount');
        $maxDate = BankingTransaction::where('account', $account_id)
            ->where('status', '!=', 0)
            ->max('entrydate');

        return [
            'account_id' => (int)$account_id,
            'futureBalance' => (float)$futureBalance,
            'currentBalance' => (float)$currentBalance,
            'maxDate' => $maxDate
        ];
    }


}
