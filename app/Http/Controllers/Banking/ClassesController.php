<?php

namespace App\Http\Controllers\Banking;

use App\Models\BankingClass as myObject;
use App\Http\Controllers\Controller;
use App\Models\BankingTransaction;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class ClassesController extends Controller
{
    private $request;

    /**
     *
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index()
    {
        $retval = myObject::get()
            ->toArray();

        return $retval;
    }

    public function get_balance($account_id)
    {
        $futureBalance = BankingTransaction
            ::where('account', '=', $account_id)
            ->sum('amount');
        $currentBalance = BankingTransaction
            ::where('account', '=', $account_id)
            ->where('entrydate', '<', date('Y-m-d'))
            ->sum('amount');

        return [
            'account_id' => (int)$account_id,
            'futureBalance' => (float)$futureBalance,
            'currentBalance' => (float)$currentBalance
        ];
    }


}
