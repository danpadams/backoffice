<?php

namespace App\Http\Controllers\Banking;

use App\Component\BankingReport;
use App\Models\BankingAccount;
use App\Models\BankingAccount as myObject;
use App\Http\Controllers\Controller;
use App\Models\BankingReportBusiness;
use App\Models\BankingTransaction;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class ReportsBusinessController extends Controller
{
    private $request;

    /**
     *
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index() {
        $retval = BankingReportBusiness::get();
        return $retval;
    }

    public function data($id) {
        $retval = BankingReportBusiness::find($id);
        return $retval;
    }
}
