<?php

namespace App\Http\Controllers\Banking;

use App\Http\Controllers\Controller;
use App\Models\BankingAccount;
use App\Models\BankingTransaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class RegisterController extends Controller
{
    private $request;

    /**
     *
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index($account_id, $status = 1)
    {
        if ($status == 1) {
            $balance = BankingTransaction::
            where('account', '=', $account_id)
                ->where('status', '=', -1)
                ->sum('amount');

            $retval = BankingTransaction::with('BankingCategory')
                ->where('account', '=', $account_id)
                ->where('status', '!=', -1)
                ->orderBy('entrydate')
                ->get()->toArray();

            foreach ($retval as $key => $value) {
                // Round is because of floating point error when balance is a finite 2 digit decimal
                $retval[$key]['balance'] = round($balance += $value['amount'], 2);

                // Set Debit/Credit Values
                if ($value['amount'] < 0) {
//                    $retval[$key]['debit'] = number_format(abs($value['amount']), 2);
                    $retval[$key]['debit'] = abs($value['amount']);
                } else {
//                    $retval[$key]['credit'] = number_format(abs($value['amount']), 2);
                    $retval[$key]['credit'] = abs($value['amount']);
                }

                $retval[$key]['category_value'] = $value['banking_category']['descrip'];

                $curdate = date('Y-m-d');
                if ($value['entrydate'] > $curdate) {
                    $retval[$key]['Future'] = 1;
                } else {
                    $retval[$key]['Future'] = 0;
                }
            }
        }

        return $retval;
    }

    public function account($account_id)
    {
        Log::info(__METHOD__ . ':' . __LINE__ . ' ' . print_r($account_id, true));
        $retval = BankingAccount::where('id', '=', $account_id)
            ->first();
        Log::info(__METHOD__ . ':' . __LINE__ . ' ' . print_r($retval->toArray(), true));
        return $retval;
    }

    public function upd_item_status($transaction_id)
    {
        $item = BankingTransaction::where('id', '=', $transaction_id)
            ->first();
        if ($item->status == 0) {
            $item->status = 1;
        } elseif ($item->status == 1) {
            $item->status = 0;
        }
        $item->save();

        return $this->index($item->account);
    }

    public function upd_register($account_id)
    {
        BankingTransaction::where('account', '=', $account_id)
            ->where('status', 1)
            ->update(['status' => -1]);

        return $this->index($account_id);
    }

}
