<?php

namespace App\Http\Controllers\Banking;

use App\Component\BankingReport;
use App\Models\BankingReport as myObject;
use App\Http\Controllers\Controller;
use App\Models\BankingReportAccount;
use App\Models\BankingReportCategory;
use App\Models\BankingReportClass;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Carbon\Carbon;

class ReportsController extends Controller
{
    private $request;

    /**
     *
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index()
    {
        $BankingReports = (new myObject())->get();

        return $BankingReports;
    }

    public function get_data($report_id)
    {
        $Data = (new myObject())
            ->where('id', $report_id)->first()->toArray();
        $Data['categories'] = $this->get_categories($report_id);
        $Data['classes'] = $this->get_classes($report_id);
        $Data['accounts'] = $this->get_accounts($report_id);
        return $Data;
    }

    public function get_categories($report_id)
    {
        $retval = [];
        $Data = (new BankingReportCategory())
            ->with('category')
            ->where('report', $report_id)
            ->get()->toArray();

        foreach ($Data as $Dat) {
            $retval[] = [
                'id' => $Dat['id'],
                'key' => $Dat['category']['id'],
                'value' => $Dat['category']['descrip'],
            ];
        }

        return $retval;
    }

    public function get_classes($report_id)
    {
        $retval = [];
        $Data = (new BankingReportClass())
            ->with('class')
            ->where('report', $report_id)
            ->get()->toArray();

        foreach ($Data as $Dat) {
            $retval[] = [
                'id' => $Dat['id'],
                'key' => $Dat['class']['id'],
                'value' => $Dat['class']['descrip'],
            ];
        }

        return $retval;
    }

    public function get_accounts($report_id)
    {
        $retval = [];
        $Data = (new BankingReportAccount())
            ->with('account')
            ->where('report', $report_id)
            ->get()->toArray();

        foreach ($Data as $Dat) {
            $retval[] = [
                'id' => $Dat['id'],
                'key' => $Dat['account']['id'],
                'value' => $Dat['account']['name'],

            ];
        }

        return $retval;
    }

    /**
     * POST
     *
     * @return void
     */
    public function add_category()
    {
        $BankingReportCategory = new BankingReportCategory();
        $BankingReportCategory->report = $this->request->report;
        $BankingReportCategory->category = $this->request->category;
        $BankingReportCategory->save();
    }

    public function del_category()
    {
        $BankingReportCategory = (new BankingReportCategory())->where('id', $this->request->id)->first()->delete();
    }

    /**
     * POST
     *
     * @return void
     */
    public function add_class()
    {
        $BankingReportClass = new BankingReportClass();
        $BankingReportClass->report = $this->request->report;
        $BankingReportClass->class = $this->request->class;
        $BankingReportClass->save();
    }

    public function del_class()
    {
        $BankingReportClass = (new BankingReportClass())->where('id', $this->request->id)->first()->delete();
    }

    /**
     * POST
     *
     * @return void
     */
    public function add_account()
    {
        $BankingReportAccount = new BankingReportAccount();
        $BankingReportAccount->report = $this->request->report;
        $BankingReportAccount->account = $this->request->account;
        $BankingReportAccount->save();
    }

    public function del_account()
    {
        $BankingReportAccount = (new BankingReportAccount())->where('id', $this->request->id)->first()->delete();
    }

    public function save_data()
    {
        if (empty($this->request->id)) {
            return [];
        } else {
            $Data = (new myObject())->where('id', $this->request->id)->first();
        }
        $Data->setProperty('name', $this->request)
            ->setProperty('startdate', $this->request)
            ->setProperty('enddate', $this->request);
        $Data->save();
        return $Data;
    }

    public function monthly($report_id)
    {
        $BankingReport = new BankingReport();

        $data = $BankingReport->getReport($report_id);
        $response = $BankingReport->getReportMonthly($data);

        $data = [];
        foreach ($response as $key => $value) {
            $value['key'] = $key;
            $data[] = $value;
        }

        return $data;
    }

    public function transaction($report_id, $year = 0, $month = 0)
    {
        $BankingReport = (new myObject())->where('id', $report_id)->first();

        if (!empty($BankingReport->data)) {
            $data = json_decode($BankingReport->data, true);
        } else {
            $BankingReportCategories = (new BankingReportCategory())->where('report', $report_id)->get();
            $Categories = [];
            foreach ($BankingReportCategories as $bankingReportCategory) {
                $Categories[] = $bankingReportCategory->category;
            }

            $BankingReportClass = (new BankingReportClass())->where('report', $report_id)->get();
            $Classes = [];
            foreach ($BankingReportClass as $bankingReportClass) {
                $Classes[] = $bankingReportClass->class;
            }

            $data = [
                'cat' => $Categories,
                'class' => $Classes,
                'account' => [],
            ];

            $year = date('Y', strtotime($BankingReport->startdate));
        }

        // Get the actual data
        $retval = [];
        $retval['report'] = $BankingReport;
        $retval['data'] = $data;
        $retval['response'] = BankingReport::getReportTransaction($data, $year, $month);

        return $retval;
    }
}
