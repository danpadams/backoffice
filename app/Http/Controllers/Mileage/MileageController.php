<?php

namespace App\Http\Controllers\Mileage;

use App\Http\Controllers\Controller;
use App\Models\Miles;
use App\Models\MileType;
use App\Models\Ticket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class MileageController extends Controller
{
    private $request;

    /**
     *
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function entries()
    {
        Log::info(__METHOD__ . ':' . __LINE__ . ' ' . print_r('', true));
        $retval = Miles
            ::with('MileType')
            ->orderBy('effdate', 'desc')
            ->get()->toArray();

        foreach ($retval as $key => $value) {
            $retval[$key]['mile_type_value'] = $value['mile_type']['name'];
            unset($value['mile_type']);
        }

        return $retval;
    }

    public function entry($id)
    {
        $retval = Miles
            ::with('MileType')
            ->where('id', '=', $id)
            ->first()->toArray();

        if (!empty($retval['mile_type'])) {
            $retval['mile_type_value'] = $retval['mile_type']['name'];
        }
        $retval['optMileTypes'] = $this->getMileTypes();

        return $retval;
    }

    public function save_data()
    {
        if (empty($this->request->id)) {
            $Mile = new Miles();
        } else {
            $Mile = Miles::where('id', '=', $this->request->id)->first();
        }
        $Mile->setProperty('effdate', $this->request)
            ->setProperty('mile_type_id', $this->request)
            ->setProperty('miles', $this->request)
            ->setProperty('comment', $this->request);
        $Mile->save();

        return $this->entry($Mile->id);
    }

    public function getMileTypes()
    {
        $retval = MileType::get()->toArray();

        return $retval;
    }
}
