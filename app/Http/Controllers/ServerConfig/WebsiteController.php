<?php

namespace App\Http\Controllers\ServerConfig;

use App\Models\SCWebsite;
use Illuminate\Support\Facades\Log;
class WebsiteController
{
    public function index() {
        $webs = (new SCWebsite())->get();
        $retval = [];
        foreach ($webs as $web) {
            $element = $web->getAttributes();
            $domain_parts = explode('.', $element['website']);
                $domain_reverse = array_reverse($domain_parts);

            //            $last_two = ;
            $element['domain'] = implode('.', array_reverse([$domain_reverse[0], $domain_reverse[1]]));
            if ($element['website'] == 'infochi.net') {
                Log::info(__METHOD__ . __LINE__ . print_r($domain_parts, true));
                Log::info(__METHOD__ . __LINE__ . print_r($domain_reverse, true));
            }
            $retval[] = $element;
        }
        return $retval;
    }
}