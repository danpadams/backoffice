<?php

namespace App\Http\Controllers\LogWithholding;

use App\Http\Controllers\Controller;
use App\Models\BankingClass;
use App\Models\LogWithholding;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class LogWithholdingController extends Controller
{
    private $request;

    /**
     *
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function entries()
    {
        $response = LogWithholding::with('BankingClass')
            ->orderBy('transfer_date', 'desc')
            ->get()->toArray();
        $retval = [];
        foreach ($response as $value) {
            $element = $value;
            $element['class_id'] = $value['banking_class']['id'];
            $element['class_name'] = $value['banking_class']['descrip'];
            $retval[] = $element;
        }

        return $retval;
    }

    public function positions()
    {
        $LogWithholding =  LogWithholding::with('BankingClass')
            ->select('banking_class_id', \DB::raw('sum(amount) as total'))
            ->groupBy(['banking_class_id'])
            ->get()->toArray();
        $retval = [];
        foreach ($LogWithholding as $value) {
            if ($value['banking_class']['active'] && $value['banking_class_id'] != 8) {
                $retval[] = [
                    'banking_class_id' => $value['banking_class_id'],
                    'banking_class_descrip' => $value['banking_class']['descrip'],
                    'total' => $value['total'],
                ];
            }
        }
        return $retval;
    }

    public function position($id)
    {
        $balance = 0;
        $LogWithholding =  LogWithholding::where('banking_class_id', $id)->orderBy('transfer_date', 'asc')->get()->toArray();
        $retval = [];
        foreach ($LogWithholding as $value) {
            $balance = $balance + $value['amount'];
            $element = [
                'id' => $value['id'],
                'amount' => $value['amount'],
                'balance' => $balance,
                'banking_class_id' => $value['banking_class_id'],
                'descrip' => $value['descrip'],
                'date' => $value['transfer_date'],
            ];
            $retval[] = $element;
        }

        return array_reverse($retval);
    }

    public function entry($id)
    {
        $retval = LogWithholding
            ::with('BankingClass')
            ->where('id', '=', $id)
            ->first()->toArray();

        if (!empty($retval['banking_class'])) {
            $retval['banking_class_value'] = $retval['banking_class']['descrip'];
        }
        $retval['optClasses'] = $this->getClasses();

        return $retval;
    }

    public function save_data()
    {
        if (empty($this->request->id)) {
            $Mile = new LogWithholding();
        } else {
            $Mile = LogWithholding::where('id', '=', $this->request->id)->first();
        }
        $Mile->setProperty('transfer_date', $this->request)
            ->setProperty('banking_class_id', $this->request)
            ->setProperty('amount', $this->request)
            ->setProperty('descrip', $this->request);
        $Mile->save();

        return $this->entry($Mile->id);
    }

    public function getClasses()
    {
        $retval = BankingClass::where('active', 1)
            ->get()->toArray();

        return $retval;
    }
}
