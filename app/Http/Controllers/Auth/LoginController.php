<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function authenticated(Request $request, $user)
    {
        $request->session()->put('user.id', $user->id);
        $request->session()->put('user.email', $user->email);
        // Set Default Client
        //        Change to Default Client
        Log::info(__METHOD__ . ': ' . __LINE__);
        // Add to Logins Table - for Logout by admin
        // Load Session Variables - Temporal Storage
        $urlHash = '#';
        if (!empty($request->urlHash)) {
            $urlHash = $request->urlHash;
        }
        return redirect('/' . $urlHash);
    }
}

