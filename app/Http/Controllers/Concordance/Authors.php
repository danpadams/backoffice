<?php

namespace App\Http\Controllers\Concordance;

use App\Http\Controllers\Controller;
use App\Models\Author;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use App\infoChi\Sort;

class Authors extends Controller
{
    private $request;

    // Created as a class variable inside a method
    private $Nodes;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index($author_id = 0)
    {
        $retval = Author::where('id', '=', $author_id)->first()->toArray();

        return $retval;
    }

    public function save_data()
    {
        // Create/Update Node
        if ($this->request->id) {
            $Author = Author::where('id', '=', $this->request->id)->first();
            $orig_id = $this->request->id;
        } else {
            $Author = new Author();
            $orig_id = 0;
        }
        if (empty($this->request->author_id)) {
            $this->request->author_id = 0;
        }
        $Author->setProperty('display', $this->request);
        $Author->save();

        return $Author;
    }
}
