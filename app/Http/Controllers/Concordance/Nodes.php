<?php

namespace App\Http\Controllers\Concordance;

use App\Http\Controllers\Controller;
use App\Models\Node;
use App\Models\Topic;
use App\Models\Author;
use App\Models\Book;
use App\Models\Topic_Node;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use App\infoChi\Sort;

class Nodes extends Controller
{
    private $request;

    // Created as a class variable inside a method
    private $Nodes;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index($topic_id = 0)
    {
        $retval = Node::where('id', '=', $topic_id)->first()->toArray();
        return $retval;
    }

    public function types()
    {
        $retval = [
            [
                'id' => 1,
                'name' => 'Verse'
            ], [
                'id' => 2,
                'name' => 'Quote'
            ]
        ];

        return $retval;
    }

    public function topics()
    {
        $Topics = Topic::with('parent')
            ->with('parent.parent')
            ->get()->toArray();

        $retval = [];
        
        foreach ($Topics as $Topic) {
            if (!empty($Topic['parent']['parent']['name'])) {
                $PName = $Topic['parent']['parent']['name'];
            } else {
                $PName = 'Base';
            }
            $retval[] = [
                'id' => $Topic['id'],
                'name' => $PName . ':' . $Topic['name'] . ' (' . $Topic['id'] . ')',
            ];
        }

        return $retval;
    }

    public function books()
    {
        $retval = Book::where('abbrev', '=', '0')
            ->orderBy('Unsorted')
            ->get();
        return $retval;
    }

    public function authors()
    {
        $retval = Author::orderBy('display')->get();
        return $retval;
    }


    public function save_data()
    {
        // Create/Update Node
        if ($this->request->id) {
            $Node = Node::where('id', '=', $this->request->id)->first();
            $orig_id = $this->request->id;
        } else {
            $Node = new Node();
            $orig_id = 0;
        }
        if (empty($this->request->author_id)) {
            $this->request->author_id = 0;
        }
        $Node->setProperty('type', $this->request);
        $Node->setProperty('data', $this->request);
        $Node->setProperty('author_id', $this->request);
        $Node->save();

        // Save New Node with Topic_ID
        if ($orig_id == 0 && !empty($this->request->topic_id)) {
            $Topic_Node = new Topic_Node();
            $Topic_Node->topic_id = $this->request->topic_id;
            $Topic_Node->node_id = $Node->id;
            $Topic_Node->save();
        }

        return $Node;
    }
}
