<?php

namespace App\Http\Controllers\Items;

use App\Models\BankingAccount;
use App\Models\AccountTransaction;
use App\Http\Controllers\Controller;
use App\Models\InventoryItem;
use App\Models\Item;
use App\Models\ItemContain;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class ItemsController extends Controller
{
    private $request;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index()
    {
        $itemDb = new Item();
        $items = $itemDb->orderBy('barcode_id')
            ->get();
        return $items;
    }

    /**
     * API Call - Retrieve Item Data by barcode_id
     *
     * @param $barcode_id
     * @return mixed
     */
    public function barcode($barcode_id)
    {
        $itemDb = new Item();
        $item = $itemDb->where('barcode_id', $barcode_id)->first();
        if (!empty($item->container)) {
            $item->container = true;
        }
        return $item;
    }

    /**
     * API Call - Retrieve Item Data by barcode_id
     *
     * @param $item_id
     * @return mixed
     */
    public function found_in($item_id)
    {
        $itemContainDb = new ItemContain();
        $item = $itemContainDb->with('container')
            ->where('child_id', $item_id)
            ->first();
        if (empty($item->container)) {
            $retval = [];
        } else {
            $retval = $item->container->toArray();
            $retval['item_contains_id'] = $item->id;
            $retval['parent_id'] = $item->parent_id;
            $retval['child_id'] = $item->child_id;
        }
        return $retval;
    }

    /**
     * API Call - Retrieve Item Data by barcode_id
     *
     * @param $item_id
     * @return mixed
     */
    public function contains($item_id)
    {
        $itemContainDb = new ItemContain();
        $items = $itemContainDb->with('contains')
            ->where('parent_id', $item_id)
            ->get();
        $retval = [];
        foreach ($items as $item) {
            if (!empty($item->contains)) {
                $element = $item->contains->toArray();
                $element['item_contains_id'] = $item->id;
                $element['parent_id'] = $item->parent_id;
                $element['child_id'] = $item->child_id;
                $retval[] = $element;
            }
        }
        return $retval;
    }
    /**
     * API Call - Retrieve Item Data by barcode_id
     *
     * @param $item_id
     * @return mixed
     */
    public function contain()
    {
        $item = new Item();
        $items = $item->orderBy('barcode_id')->get();
        $retval = [];
        foreach ($items as $item) {
                $retval[] = [
                    'id' => $item->id,
                    'value' => $item->barcode_id . ' / ' . $item->brief,
                    'container' => $item->container
                ];
        }
        return $retval;
    }

    public function save_data() {
        Log::info(__METHOD__ . ':' . __LINE__ . ' ' . print_r($this->request->toarray(), true));
        if (!empty($this->request->id)) {
            $item = (new Item())->where('id', $this->request->id)->first();
        }else {
            $checkBarcode = (new Item())->where('barcode_id', $this->request->barcode)->first();
            if (empty($checkBarcode)) {
                $item = new Item();
                $item->setProperty('barcode_id', $this->request);
            } else {
                return;
            }
        }

        if (empty($this->request->container)) {
            $item->container = 0;
        } else {
            $item->container = 1;
        }
        $item->setProperty('brief', $this->request)
            ->setProperty('notes', $this->request);

        $item->save();
    }

    public function choiceContains()
    {
        if (!empty($this->request->id)) {
//            Log::info(__METHOD__ . ':' . __LINE__ . ' ' . print_r($this->request->toarray(), true));
            $itemContain = (new ItemContain())->where('child_id', $this->request->id)->first();
//            Log::info(__METHOD__ . ':' . __LINE__ . ' ' . print_r($itemContain->toArray(), true));
            if (empty($itemContain)) {
                // New Container
                $itemContain = (new ItemContain());
                $itemContain->child_id = $this->request->id;
            }
            $itemContain->parent_id = $this->request->item_id;
            $itemContain->save();
        }
    }

    public function choiceContainer()
    {
        if (!empty($this->request->id)) {
            Log::info(__METHOD__ . ':' . __LINE__ . ' ' . print_r($this->request->toarray(), true));
            $itemContain = (new ItemContain())->where('child_id', $this->request->item_id)->first();
            if (empty($itemContain)) {
                // New Container
                $itemContain = (new ItemContain());
                $itemContain->child_id = $this->request->item_id;
            }
            $itemContain->parent_id = $this->request->id;
            $itemContain->save();
        }
    }

    public function remove_item_container() {
        $data = $this->request;
        if (!empty($data->item_contains_id)) {
            Log::info(__METHOD__ . ':' . __LINE__ . ' ' . print_r($data->item_contains_id, true));
            $itemContain = (new ItemContain())->where('id', $data->item_contains_id)->first();
            Log::info(__METHOD__ . ':' . __LINE__ . ' ' . print_r($itemContain->toArray(), true));
            $itemContain->delete();
        }
    }
}
