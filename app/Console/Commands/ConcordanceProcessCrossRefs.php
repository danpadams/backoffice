<?php

namespace App\Console\Commands;

use App\Component\ApiEsv;
use App\Component\BankingReport;
use App\Component\Concordance;
use App\Models\Node;
use Illuminate\Console\Command;
use Illuminate\Http\Request;

class ConcordanceProcessCrossRefs extends Command
{

    private $request;

    /**
     * The name and signature of the console command.
     *
     * @var string
     *
     */
    protected $signature = 'concordance:process_x_refs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Concordance - Get Cross Refs';

    protected $concordance;
    /*
     * Create a new command instance.
     *
     * @return void
     */

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->concordance = new Concordance();
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        // Grab a Verse(s) from the DB
        $Nodes = (new Node())
            ->where('id', 14)
            ->where('type', 1)
//            ->where('data.Text', '')
            ->get();

        foreach ($Nodes as $Node) {
            $Data = json_decode($Node->data, true);
            if (empty($Data['XRefs'])) {
                $xRefs = json_decode($Data['xRefs']);
                $result = [];
                foreach ($xRefs as $xref) {
                    $data = $this->concordance->verseAsArray($xref);
                    $data = $this->concordance->verseAsNode($data);
                    foreach ($data as $node) {
                        $result[] = $node;
                    }
                }
                print_r($result);
            } else {
                echo "Text already downloaded: " . $Data['XRefs'] . "\n";
            }

            break;
        }
    }

}
