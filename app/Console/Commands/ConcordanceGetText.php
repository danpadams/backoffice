<?php

namespace App\Console\Commands;

use App\Component\ApiEsv;
use App\Component\BankingReport;
use App\Component\Concordance;
use App\Models\Node;
use Illuminate\Console\Command;
use Illuminate\Http\Request;

class ConcordanceGetText extends Command
{

    private $request;

    /**
     * The name and signature of the console command.
     *
     * @var string
     *
     */
    protected $signature = 'concordance:get_text';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Concordance - Get Text of Verse';

    protected $concordance;
    /*
     * Create a new command instance.
     *
     * @return void
     */

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->concordance = new Concordance();
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $Nodes = (new Node())
            ->where('id', 14)
            ->where('type', 1)
            ->limit(1)
            ->get();

        foreach ($Nodes as $node) {
            $Data = json_decode($node->data, true);
            echo "\t" . $this->concordance->verseAsString($Data) . "\n";
            echo "\n";
            if (empty($Data['Text']) || ($Data['Text'] == '-')) {
//                if (empty($Data['Verse_End']) || empty($Data['Verse_End']) || empty($Data['Chapter_End'])) {
//                    $Data['Text'] = '-';
//                } else {
                    echo __METHOD__ . ':' . __LINE__ . "\n";
                    $reference = $this->concordance->verseAsString($Data);
                    $response = (new ApiEsv())->getText($reference);
                    $Data['Text'] = $response;
//                }
                $node->data = json_encode($Data);
                $node->save();
            } else {
                echo "Text already downloaded: " . $Data['Text'] . "\n";
            }

//            break;
        }
    }

}
