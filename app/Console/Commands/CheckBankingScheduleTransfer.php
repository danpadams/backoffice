<?php

namespace App\Console\Commands;

use App\Component\BankingTransfer as TransferComponent;
use Illuminate\Console\Command;
use Illuminate\Http\Request;

class CheckBankingScheduleTransfer extends Command
{

    private $request;

    /**
     * The name and signature of the console command.
     *
     * @var string
     *
     */
    protected $signature = 'check:banking_schedule_tranfer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Queue for Banking Transfers';

    /*
     * Create a new command instance.
     *
     * @return void
     */

    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        (new TransferComponent())->getList();
    }

}
