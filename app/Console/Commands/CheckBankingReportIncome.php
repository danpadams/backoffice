<?php

namespace App\Console\Commands;

use App\Component\BankingReport;
use Illuminate\Console\Command;
use Illuminate\Http\Request;

class CheckBankingReportIncome extends Command
{

    private $request;

    /**
     * The name and signature of the console command.
     *
     * @var string
     *
     */
    protected $signature = 'check:banking_report_income';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Banking - Get Report Income';

    /*
     * Create a new command instance.
     *
     * @return void
     */

    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $BankingReport = new BankingReport();
        $data = [
            'year' => 2023,
            'cat' => [25],
            'class' => [16],
        ];

        $response = $BankingReport->getReportMonthly($data);
        $data = [];
        foreach ($response as $key=>$value) {
            $data[$key] = $value['subtotal'];
        }
        print_r($data);
        print_r($response);
    }

}
