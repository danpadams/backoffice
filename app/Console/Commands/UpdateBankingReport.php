<?php

namespace App\Console\Commands;

use App\Models\BankingReport;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Http\Request;

class UpdateBankingReport extends Command
{

    private $request;

    /**
     * The name and signature of the console command.
     *
     * @var string
     *
     */
    protected $signature = 'update:banking_report {report_id} {year_from} {year_to}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Banking - Get Report';

    /*
     * Create a new command instance.
     *
     * @return void
     */

    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $BankingReport = (new BankingReport())->where('id', $this->argument('report_id'))->first();
        $BankingReport->startdate = (new Carbon($BankingReport->startdate))->year($this->argument('year_to'));
        $BankingReport->enddate = (new Carbon($BankingReport->enddate))->year($this->argument('year_to'));
        $BankingReport->name = str_replace($this->argument('year_from'), $this->argument('year_to'), $BankingReport->name);
        print_r($BankingReport->toArray());
        $BankingReport->save();
    }

}
