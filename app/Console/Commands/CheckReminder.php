<?php

namespace App\Console\Commands;

use App\Mail\ReminderMessage;
use App\Models\TrackerFrequency;
use App\Models\TrackerReminder;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class CheckReminder extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check the reminder queue';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $Reminders = TrackerReminder::with('User')
            ->with('TrackerFrequency')
            ->where('next','<=', date('Y-m-d H:i:s'))
            ->where('processed','=', '0')
            ->get();
        foreach ($Reminders as $reminder) {
            $this->sendEmail($reminder->toArray());
            $reminder_a = $reminder->toArray();
            if (!empty($reminder_a['tracker_frequency']['type'])) {
                $reminder->next = TrackerFrequency::getNext($reminder_a['next'], $reminder_a['tracker_frequency']['type']);
            } else {
                $reminder->processed = '1';
            }

            $reminder->save();
        }
    }

    private function sendEmail($reminder) {
        Mail::to($reminder['user']['email'])
            ->send(new ReminderMessage($reminder));

        return true;
    }

}
