<?php

namespace App\Console\Commands;

use App\Component\CheckTicker;
use App\Models\Ticker;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class CheckTickerData extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:ticker_data {ticker?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check the ticker Data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // where here there is your calculation for now How many days
        $ticker = $this->arguments()['ticker'] ?? '';

        $dayAgo = 2;
        // Thanksgiving, + Black Friday + Weekend
        $dayToCheck = Carbon::now()->subDays($dayAgo);

        $Tickers = Ticker::where('inactive', '=', 0);
        if (empty($ticker)) {
            $Tickers = $Tickers->where("asof", '<', $dayToCheck);
        } else {
            $Tickers = $Tickers->where("ticker", $ticker);
        }

        $Tickers = $Tickers->orderBy('asof')->limit(5)->get();

        foreach ($Tickers as $Ticker) {
            CheckTicker::checkTicker($Ticker);
        }
    }

    public function wait()
    {
        echo "\t\tWait 5\n";
        sleep(5);
        echo "\t\tWait 5\n";
        sleep(5);
        echo "\t\tWait 5\n";
        sleep(5);
        echo "\t\tWait 5\n";
        sleep(5);
        echo "\t\tWait 5\n";
        sleep(5);
    }

}
