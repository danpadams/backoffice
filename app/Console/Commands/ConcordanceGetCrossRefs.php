<?php

namespace App\Console\Commands;

use App\Component\ApiEsv;
use App\Component\BankingReport;
use App\Component\Concordance;
use App\Models\Node;
use Illuminate\Console\Command;
use Illuminate\Http\Request;

class ConcordanceGetCrossRefs extends Command
{

    private $request;

    /**
     * The name and signature of the console command.
     *
     * @var string
     *
     */
    protected $signature = 'concordance:get_x_refs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Concordance - Get Cross Refs';

    protected $concordance;
    /*
     * Create a new command instance.
     *
     * @return void
     */

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->concordance = new Concordance();
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        // Grab a Verse(s) from the DB
        $Nodes = (new Node())
            ->where('id', 14)
            ->where('type', 1)
//            ->where('data.Text', '')
            ->get();

        foreach ($Nodes as $node) {
            $Data = json_decode($node->data, true);
            if (empty($Data['XRefs'])) {
                $reference = $this->concordance->verseAsString($Data);
                echo "Reference: " . $reference . "\n";
                echo "\tAPI Call Made\n\n";
                $response = (new ApiEsv())->getXRefs($reference);
                echo "\tResponse: "; print_r($response);
                $Data['xRefs'] = json_encode($response);
                $node->data = json_encode($Data);
                $node->save();
            } else {
                echo "Text already downloaded: " . $Data['XRefs'] . "\n";
            }

            break;
        }
    }

}
