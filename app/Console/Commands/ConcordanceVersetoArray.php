<?php

namespace App\Console\Commands;

use App\Component\ApiEsv;
use App\Component\BankingReport;
use App\Component\Concordance;
use App\Models\Node;
use Illuminate\Console\Command;
use Illuminate\Http\Request;

class ConcordanceVersetoArray extends Command
{

    private $request;

    /**
     * The name and signature of the console command.
     *
     * @var string
     *
     */
    protected $signature = 'concordance:verse_to_array';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Concordance - Get Cross Refs';

    protected $concordance;
    /*
     * Create a new command instance.
     *
     * @return void
     */

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->concordance = new Concordance();
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $Verse = '1 Cor 5:6-7,10,15';
        $Verse = 'Exodus 20:5';
        echo $Verse . "\n";
        $response = (new Concordance())->verseAsArray($Verse);
        print_r($response);
        $response = (new Concordance())->verseAsNode($response);
        print_r($response);
    }
}
