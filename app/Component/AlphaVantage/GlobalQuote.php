<?php

namespace App\Component\AlphaVantage;

use App\Component\CurlData;
use App\Component\AlphaVantage;

class GlobalQuote extends AlphaVantage
{

    private $Quote;

    public function __construct($Ticker)
    {
        $this->Ticker = $Ticker;
        $this->func = 'GLOBAL_QUOTE';
        $this->fetchData(); // Using Parent Class for general fetch Data functions
        if (empty($this->Fetched)) {
            throw new \Exception('Missing Quote');
        }
        if (!empty($this->Fetched['Global Quote'])) {
            $this->Quote = $this->Fetched['Global Quote'];
        } else {
            die($this->Fetched['Information'] . "\n");
        }
    }

    public function getJSON()
    {
        return json_encode($this->Quote);
    }

    public function getSymbol()
    {
        return $this->Quote['01. symbol'];
    }

    public function getOpen()
    {
        return $this->Quote['02. open'];
    }

    public function getHigh()
    {
        return $this->Quote['03. high'];
    }

    public function getLow()
    {
        return $this->Quote['04. low'];
    }

    public function getPrice()
    {
        return $this->Quote['05. price'];
    }

    public function getVolume()
    {
        return $this->Quote['06. volume'];
    }

    public function getAsof()
    {
        return $this->Quote['07. latest trading day'];
    }

    public function getPrevClose()
    {
        return $this->Quote['08. previous close'];
    }

    public function getChange()
    {
        return $this->Quote['09. change'];
    }

    public function getChangePct()
    {
        $retval = $this->Quote['10. change percent'];
//        str_replace ( mixed $search , mixed $replace , mixed $subject [, int &$count ] ) : mixed
        return str_replace('%', '', $retval);
    }

}
