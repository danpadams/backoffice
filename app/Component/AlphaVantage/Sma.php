<?php

namespace App\Component\AlphaVantage;

use App\Component\CurlData;
use App\Component\AlphaVantage;

class Sma extends AlphaVantage
{

    private $Sma;
    private $DmaPct;

    public function __construct($Ticker, $timeperiod = 50, $interval = 'daily')
    {
        $this->func = 'SMA';
        $this->Ticker = $Ticker;
        $this->timeperiod = $timeperiod;
        $this->interval = $interval;
        $this->Query = 'interval=' . $this->interval . '&';
        $this->Query .= 'time_period=' . $this->timeperiod . '&';
        $this->Query .= 'series_type=close&';

        $this->fetchData();
        $this->Sma = $this->Fetched['Technical Analysis: SMA'];
    }

    public function getJSON() {
        return json_encode($this->Sma);
    }

    public function getMa()
    {
        $retval = null;
        foreach ($this->Sma as $Sma) {
            $retval = $Sma['SMA'];
            break;
        }
        return $retval;
    }

    public function getDma()
    {
        foreach ($this->Sma as $Sma) {
            if (!isset($Val_1)) {
                $Val_1 = $Sma['SMA'];
            } else {
                $Val_2 = $Sma['SMA'];
                break;
            }
        }
        $this->DmaPct = (($Val_2 - $Val_1) / $Val_2) * 100;
        return $Val_2 - $Val_1;
    }

    public function getDmaPct()
    {
        if (!isset($this->DmaPct)) {
            $this->getDma();
        }
        return $this->DmaPct;
    }

}
