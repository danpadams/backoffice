<?php

namespace App\Component;



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CurlData
 *
 * @author dan
 */
class CurlData {

    private $Data;

    public function __construct($url) {
        $session = curl_init($url);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);

        //Uncomment if you are behind a proxy
        //curl_setopt($session, CURLOPT_PROXY, 'Your proxy url');
        //curl_setopt($session, CURLOPT_PROXYPORT, 'Your proxy port');
        //curl_setopt($session, CURLOPT_PROXYUSERPWD, 'Your proxy password');

        $json = curl_exec($session);
        curl_close($session);
        $this->Data = json_decode($json, true);
    }

    //put your code here
    public function getData() {
        return $this->Data;
    }

}
