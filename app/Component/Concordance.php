<?php

namespace App\Component;

use App\Models\Book;

class Concordance
{
    public function verseAsString($data)
    {
        $Book_Name = (new Book())->where('id', $data['Book_Num'])->where('abbrev', 0)->first()->toArray();
        $Verse_End = false;

        $retval = $Book_Name['Name'] . ' ' . $data['Chapter_Start'];
        if ($data['Verse_Start']) {
            $retval .= ':' . $data['Verse_Start'];
        }
        if ($data['Chapter_End'] != 0 || $data['Verse_End'] != 0) {
            $retval .= '-';
        }
        if (($data['Chapter_End']) && ($data['Chapter_End'] != '0')) {
            $retval .= $data['Chapter_End'] . ':';
            $Verse_End = true;
        }
        if ($data['Verse_End'] != 0 || $Verse_End) {
            $retval .= $data['Verse_End'];
        }

        return $retval;
    }

    public function verseAsArray(string $input) {
        // split verses from book chapters
        $parts = preg_split('/\s*:\s*/', trim($input, " ;/"));

        // Create the book array
        $book = array('name' => "", 'chapter' => "", 'verses' => array());

        // $part[0] = book + chapter, if isset $part[1] is verses
        if(isset($parts[0]))
        {
            // 1.) get chapter
            if(preg_match('/\d+\s*$/', $parts[0], $out)) {
                $book['chapter'] = rtrim($out[0]);
            }

            // 2.) book name
            $book['name'] = trim(preg_replace('/\d+\s*$/', "", $parts[0]));
        }

        // 3.) verses
        if(isset($parts[1])) {
            $book['verses'] = preg_split('~\s*,\s*~', $parts[1]);
        }

        return $book;
    }

    public function verseAsNode($input) {
        $retval = [];
        $element = [];

        $Book = (New Book())->where('Name', $input['name'])->first()->toArray();
        if (!empty($Book)) {
            if ($Book['abbrev'] == 1) {
                $Book = (New Book())->where('ID', $Book['ID'])->where('abbrev', 0)->first()->toArray();
                $element['Boon_Name'] = $Book['Name'];
            } else {
                $element['Boon_Name'] = $Book['Name'];
            }
            $element['Book_Num'] = $Book['ID'];
        }
        if (!empty($input['chapter'])) {
            $element['Chapter_Start'] = $input['chapter'];
        }
        foreach ($input['verses'] as $verse) {
            $data = $element;
            $value = explode('-', $verse);
            $data['Verse_Start'] = $value[0];
            if (empty($value[1])) {
                $data['Chapter_End'] = 0;
                $data['Verse_End'] = 0;
            } else {
                $data['Chapter_End'] = 0;
                $data['Verse_End'] = $value[1];
            }
            $data['Verse_String'] = $this->verseAsString($data);

            $retval[] = $data;
        }

        return $retval;
    }

    public function writeVerse($data)
    {
        $Node = new Node();
        $Node->type = 1;
        $Node->data = json_encode($data);
//        $Node->save();
    }
}