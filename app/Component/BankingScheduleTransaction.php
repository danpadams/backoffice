<?php

namespace App\Component;

use App\Models\BankingScheduleTransaction as myObject;
use App\Models\BankingTransaction;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class BankingScheduleTransaction
{
    public function getList()
    {
        $checkdate = (new Carbon())->addMonth(1);
        $BankingScheduleTransactions = myObject::where('next', '<', $checkdate)
            ->get();

        $i = 0; //Counter
        foreach ($BankingScheduleTransactions as $Item) {
            $this->process($Item);
            $i++;
        }
        echo __CLASS__ . "\n";
        echo "\tItems Processed: $i\n";
    }

    private function process($Item)
    {
        $NextNew = $this->getNext($Item->next, $Item->freq);

        $BankingTransaction = new BankingTransaction();
        $BankingTransaction->account = $Item->account;
        $BankingTransaction->entrydate = $Item->next;
        $BankingTransaction->payee = $Item->payee;
        $BankingTransaction->amount = $Item->amount;
        $BankingTransaction->cat = $Item->cat;
        $BankingTransaction->class = $Item->class;
        $BankingTransaction->comment = '';
        $BankingTransaction->save();

        $Item->next = $NextNew;
        $Item->save();

        echo "<p>" . __METHOD__ . ':' . __LINE__ . "</p>\n";
        print_r($Item->toArray());
        echo "<p>" . __METHOD__ . ':' . __LINE__ . "</p>\n";
        print_r($BankingTransaction->toArray());
    }

    private function getNext($next, $freq)
    {
        $retval = (new Carbon($next));
        switch ($freq) {
            case 5: // Yearly
                $retval = (new Carbon($next))->addMonth(12);
                break;
            case 4: // Every 2 Weeks
                $retval = (new Carbon($next))->addWeek(2);
                break;
            case 3: // Quarterly
                $retval = (new Carbon($next))->addMonth(3);
                break;
            case 1: // Monthly
                $retval = (new Carbon($next))->addMonth(1);
                break;
        }
        return $retval->year . '-' . $retval->month . '-' . $retval->day;
    }
}