<?php

namespace App\Component;

use GuzzleHttp\Client;
use App\Component\Concordance;

const API_KEY = '4370fb13fc9bbe9fb40d89e2e90dcde7223a7af2';

const API_HTML_URL = 'https://api.esv.org/v3/passage/html/';

const API_TEXT_URL = 'https://api.esv.org/v3/passage/text/';

class ApiEsv
{
    private $concordance;

    public function __construct()
    {
        $this->concordance = new Concordance();
    }
    private function getHeaders()
    {
        $headers = ['Authorization'=> 'Token ' . API_KEY];

        return $headers;
    }

    public function getText($passage) {
        $retval = [];
        $params = [
            'q' => $passage,
            'include-headings' => 'False',
            'include-footnotes' => 'False',
            'include-verse-numbers' => 'False',
            'include-short-copyright' => 'False',
            'include-passage-references' => 'False',
            'include-crossrefs' => 'False',
        ];

        $url = API_TEXT_URL . '?' . http_build_query($params, '', '&');
        $client = new Client( ['headers' => $this->getHeaders()]);
        $response = $client->request('GET', $url);
        $result = $response->getBody()->getContents();

        $data = json_decode($result, true);

        $retval = trim($data['passages'][0]);
        return $retval;
    }

    public function getXRefs($passage) {
        $refs = [];
        $verses = [];
        $params = [
            'q' => $passage,
            'include-headings' => 'False',
            'include-footnotes' => 'False',
            'include-verse-numbers' => 'False',
            'include-copyright' => 'False',
            'include-short-copyright' => 'False',
            'include-passage-references' => 'False',
            'include-audio-link' => 'False',
            'include-crossrefs' => 'True',
        ];

        $url = API_HTML_URL . '?' . http_build_query($params, '', '&');

        $client = new Client( ['headers' => $this->getHeaders()]);
        $response = $client->request('GET', $url);
        $result = $response->getBody()->getContents();

        $data = json_decode($result, true);

        $result = $data['passages'][0];

        preg_match_all('~<a\s+.*?</a>~is',$result,$anchors);
        foreach ($anchors[0] as $anchor) {
            $a = new \SimpleXMLElement($anchor);
            $refs[] = $a['href'];
        }
        foreach ($refs as $ref) {
            $out = explode(";", $ref);
            foreach ($out as $ou) {
                $verses[] = $item = trim($ou, " ;/");
            }
        }

        return $verses;
    }
}