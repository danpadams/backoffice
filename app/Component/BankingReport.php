<?php

namespace App\Component;

use App\Models\BankingReport as BankingReportDb;
use App\Models\BankingSplit;
use App\Models\BankingTransaction;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class BankingReport
{
    public function getReportMonthly($data)
    {
        $retval = [];
        $BankingTransaction = new BankingTransaction();
        $BankingTransaction = $BankingTransaction->select(DB::raw('Year(entrydate) as `year`, MONTH(entrydate) month')
            , DB::raw('sum(amount) as `subtotal`'));
        $BankingSplit = (new BankingSplit())->join('banking_transaction', function ($join) {
            $join->on('banking_transaction.id', '=', 'banking_split.trans_num');
        });
        $BankingSplit = $BankingSplit->select(DB::raw('Year(banking_transaction.entrydate) as `year`, MONTH(banking_transaction.entrydate) month'), DB::raw('sum(banking_split.amount) as `subtotal`'));

        if (!empty($data['class'])) {
            $BankingTransaction = $BankingTransaction->whereIn('class', $data['class']);
            $BankingSplit = $BankingSplit->whereIn('banking_split.class', $data['class']);
        }
        if (!empty($data['not_cat'])) {
            $BankingTransaction = $BankingTransaction->whereNotIn('cat', $data['not_cat']);
            $BankingSplit = $BankingSplit->whereNotIn('banking_split.category', $data['not_cat']);
        }
        if (!empty($data['cat'])) {
            $BankingTransaction = $BankingTransaction->whereIn('cat', $data['cat']);
            $BankingSplit = $BankingSplit->whereIn('banking_split.category', $data['cat']);
        }
        if (!empty($data['year'])) {
            $BankingTransaction = $BankingTransaction->whereYear('entrydate', $data['year']);
            $BankingSplit = $BankingSplit->whereYear('entrydate', $data['year']);
        } else {
            if (!empty($data['startDate'])) {
                $BankingTransaction = $BankingTransaction->where('entrydate', '>=', $data['startDate']);
                $BankingTransaction = $BankingTransaction->where('entrydate', '>=', $data['startDate']);
            }
        }

        $BankingTransaction = $BankingTransaction->groupBy('year', 'month')->get()->toArray();
//        echo ">{$BankingSplit->toSql()}\n";
//        print_r($BankingSplit->getBindings());

        $BankingSplit = $BankingSplit->groupBy('year', 'month')->get()->toArray();
        foreach ($BankingTransaction as $value) {
            $combinedKey = $value['year'] . '-' . $value['month'];
            if (empty($retval[$combinedKey])) {
                $retval[$value['year'] . '-' . $value['month']] = $value;
            }
        }
        foreach ($BankingSplit as $value) {
            $combinedKey = $value['year'] . '-' . $value['month'];
            if (empty($retval[$combinedKey])) {
                $retval[$combinedKey] = $value;
            } else {
                $retval[$combinedKey] = [
                    'year' => $value['year'],
                    'month' => $value['month'],
                    'subtotal' => $retval[$combinedKey]['subtotal'] + $value['subtotal']
                ];
            }
        }

        return $retval;
    }

    public static function getReportTransaction($data, $year = null, $month = null)
    {
        $BankingTransaction = (new BankingTransaction())
            ->with('BankingCategory')
            ->with('BankingClass')
            ->with('BankingAccount');
        $BankingSplit = (new BankingSplit())
            ->with('BankingCategory')
            ->with('BankingClass')
            ->with('BankingAccount')
            ->with('BankingTransaction');

        if (!empty($data['class'])) {
            $BankingTransaction = $BankingTransaction->whereIn('class', $data['class']);
            $BankingSplit = $BankingSplit->whereIn('class', $data['class']);
        }
        if (!empty($data['not_cat'])) {
            $BankingTransaction = $BankingTransaction->whereNotIn('cat', $data['not_cat']);
            $BankingSplit = $BankingSplit->whereNotIn('category', $data['not_cat']);
        }
        if (!empty($data['cat'])) {
            $BankingTransaction = $BankingTransaction->whereIn('cat', $data['cat']);
            $BankingSplit = $BankingSplit->whereIn('category', $data['cat']);
        }

        if (!empty($year)) {
            $BankingTransaction = $BankingTransaction->whereYear('entrydate', $year);
        }
        if (!empty($month)) {
            $BankingTransaction = $BankingTransaction->whereMonth('entrydate', $month);
        }

        $splits = $BankingSplit->get();

        $response = $BankingTransaction->get()->toArray();
        $retval = [];
        foreach ($response as $value) {
            $element = [
                'id' => $value['id'],
                'entrydate' => $value['entrydate'],
                'payee' => $value['payee'],
                'amount' => $value['amount'],
                'account_id' => $value['banking_account']['id'],
                'account_name' => $value['banking_account']['name'],
                'category_id' => $value['banking_category']['id'],
                'category_name' => $value['banking_category']['descrip'],
                'class_id' => $value['banking_class']['id'],
                'class_name' => $value['banking_class']['descrip']
            ];
            $retval[] = $element;
        }
        foreach ($splits as $split) {
            $split = $split->toArray();
            $add_item = false;
            $entrydate = $split['banking_transaction']['entrydate'];
            if (!empty($entrydate)) {
                if (!empty($year) && $year == Carbon::createFromFormat('Y-m-d', $entrydate)->year) {
                    $add_item = true;
                }
                if (!empty($month) && $month == Carbon::createFromFormat('Y-m-d', $entrydate)->month) {
                    $add_item = true;
                }
            }
            if ($add_item) {
                $element = [
                    'id' => $split['banking_transaction']['id'],
                    'entrydate' => $split['banking_transaction']['entrydate'],
                    'payee' => $split['banking_transaction']['payee'],
                    'amount' => $split['amount'],
                    'account_id' => $value['banking_account']['id'],
                    'account_name' => $value['banking_account']['name'],
                    'category_id' => $value['banking_category']['id'],
                    'category_name' => 'Split: ' . $value['banking_category']['descrip'],
                    'class_id' => $value['banking_class']['id'],
                    'class_name' => $value['banking_class']['descrip']
                ];
                $retval[] = $element;
            }
        }
        // Re-Sort $retval by 'entrydate'
        array_multisort(array_column($retval, 'entrydate'), SORT_DESC, $retval);

        return $retval;
    }

    public function getReport($id = null)
    {
        $BankingReport = new BankingReportDb();
        $BankingReport = $BankingReport->where('id', $id);
        $BankingReport = $BankingReport->first();

        return json_decode($BankingReport->data, true);
    }
}