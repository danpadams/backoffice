<?php

namespace App\Component;

use App\Component\BankingTransfer as TransferComponenet;
use App\Models\BankingScheduleTransfer as myObject;
use App\Models\BankingTransaction as DbTransaction;
use App\Models\BankingTransfer as DbTransfer;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class BankingTransfer
{

    public function getList()
    {
        $checkdate = (new Carbon())->addMonth(1);
        $BankingScheduleTransfers = myObject::where('next', '<', $checkdate)
            ->get();

        $i = 0; //Counter
        foreach ($BankingScheduleTransfers as $Item) {
            $this->process($Item);
            $i++;
        }
        echo __CLASS__ . "\n";
        echo "\tItems Processed: $i\n";
    }

    private function process($Item)
    {

        // Create From Transaction
        $params = [
            'account' => $Item->account_from,
            'amount' => $Item->amount,
            'payee' => $Item->payee,
            'entrydate' => $Item->next
        ];
        $idTo = TransferComponenet::saveTransaction($params);

        // Create To Transaction
        $params = [
            'account' => $Item->account_to,
            'amount' => $Item->amount * -1,
            'payee' => $Item->payee,
            'entrydate' => $Item->next
        ];
        $idFrom = TransferComponenet::saveTransaction($params);

        // Create Join for editing
        $params = [
            'idFrom' => $idFrom,
            'idTo' => $idTo
        ];
        TransferComponenet::saveTransfer($params);

        Log::info(__METHOD__ . ':' . __LINE__ . ' ' . print_r($Item->toArray(), true));
echo "<p>" . __METHOD__ . ':' . __LINE__ . " " .  print_r($Item->toArray(), true) . "</p>\n";

        $NextNew = $this->getNext($Item->next, $Item->schedule_frequency_id);
        $Item->next = $NextNew;
        $Item->save();
    }

    private function getNext($next, $freq)
    {
        $retval = (new Carbon($next));
        switch ($freq) {
            case 5: // Yearly
                $retval = (new Carbon($next))->addMonth(12);
                break;
            case 4: // Every 2 Weeks
                $retval = (new Carbon($next))->addWeek(2);
                break;
            case 3: // Quarterly
                $retval = (new Carbon($next))->addMonth(3);
                break;
            case 1: // Monthly
                $retval = (new Carbon($next))->addMonth(1);
                break;
        }
        return $retval->year . '-' . $retval->month . '-' . $retval->day;
    }


    public static function saveTransaction($params)
    {
        $bankingTransaction = new DbTransaction();
        $bankingTransaction->account = $params['account'];
        $bankingTransaction->amount = $params['amount'];
        $bankingTransaction->payee = $params['payee'];
        $bankingTransaction->entrydate = $params['entrydate'];
        $bankingTransaction->cat = 14;
        $bankingTransaction->save();
        return $bankingTransaction->id;
    }

    public static function saveTransfer($params)
    {
        $bankingTransfer = new DbTransfer();
        $bankingTransfer->trans_num_from = $params['idFrom'];
        $bankingTransfer->trans_num_to = $params['idTo'];
        $bankingTransfer->save();
        return true;
    }
}