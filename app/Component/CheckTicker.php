<?php

namespace App\Component;

use App\Component\AlphaVantage\GlobalQuote;
use App\Component\AlphaVantage\Sma;
use App\Models\Ticker;
use Illuminate\Support\Facades\Log;

class CheckTicker
{
    public static function checkTicker(Ticker $Ticker)
    {
        try {
            $symbol = $Ticker->ticker;
            self::wait();
            echo "$symbol\n";

            // Basic Price Data
            echo "\tBasic Data\n";
            $GlobalQuote = new GlobalQuote($symbol);
            $Ticker->price = $GlobalQuote->getPrice();
            $Ticker->diff = $GlobalQuote->getChange();
            $Ticker->diff_pct = $GlobalQuote->getChangePct();
            $Ticker->data_gen = $GlobalQuote->getJSON();

            // If asof is more than 1 week ago, mark as inactive
            if (strtotime($GlobalQuote->getAsof()) < strtotime('1 week ago')) {
                $Ticker->inactive = 1;
            } else {
                if ($GlobalQuote->getAsof() > $Ticker->asof) {
                    $Ticker->asof = $GlobalQuote->getAsof();
                    $Ticker->inactive = 0;
                    self::wait();
                    echo "\tSMA-50\n";
                    try {
                        $Sma50 = new Sma($symbol, 50, 'daily');

                        $Ticker->ma_50 = $Sma50->getMa();
                        $Ticker->data_50 = $Sma50->getJSON();
                        $Ticker->dma_50 = $Sma50->getDma();
                        $Ticker->dma_50_pct = $Sma50->getDmaPct();
                    } catch (\Exception $e) {
                        Log::info(__METHOD__ . ':' . __LINE__ . ' - ' . $symbol . ' ' . $e->getMessage());
                    }

                    // SMA 200 Data
                    self::wait();
                    echo "\tSMA-200\n";
                    try {
                        $Sma200 = new Sma($symbol, 200, 'daily');
                        $Ticker->ma_200 = $Sma200->getMa();
                        $Ticker->data_200 = $Sma200->getJSON();
                        $Ticker->dma_200 = $Sma200->getDma();
                        $Ticker->dma_200_pct = $Sma200->getDmaPct();
                    } catch (\Exception $e) {
                        Log::info(__METHOD__ . ':' . __LINE__ . ' ' . $symbol . ' ' . $e->getMessage());
                    }
                }
            }
        } catch (\Exception $e) {
            echo "\t" . $e->getMessage() . "\n";
            $Ticker->inactive = 2;
            $Ticker->message = $e->getMessage();
        }
        $Ticker->save();
    }

    public static function wait()
    {
        echo "\t\tWait 5\n";
        sleep(5);
        echo "\t\tWait 5\n";
        sleep(5);
        echo "\t\tWait 5\n";
        sleep(5);
        echo "\t\tWait 5\n";
        sleep(5);
        echo "\t\tWait 5\n";
        sleep(5);
    }

}