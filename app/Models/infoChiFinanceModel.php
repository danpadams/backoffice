<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class infoChiFinanceModel extends Model
{

    protected $connection = 'mysql1';

    // Function to set the property desired from the request object
    public function setProperty($name, $object)
    {
        if (isset($object->$name)) {
            $this->$name = $object->$name;
        }
        return $this;
    }

    public function setFromArray($name, array $data)
    {
        if (isset($data[$name])) {
            $this->$name = $data[$name];
        }
        return $this;
    }

    public function assignFromArray($name, array $data, $key)
    {
        if (isset($data[$name])) {
            $this->$key = $data[$name];
        }
        return $this;
    }
}
