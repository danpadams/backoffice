<?php

namespace App\Models;

use App\Models\infoChiFinanceModel;
use Illuminate\Support\Facades\Log;
use App\Models\RentalPlatform;

class RentalBooking extends infoChiFinanceModel {

    protected $table = "rental_bookings";

    //put your code here
    protected $fillable = [
    ];

    public function RentalPlatform() {
        return $this->belongsTo(RentalPlatform::class);
    }

}
