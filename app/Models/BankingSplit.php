<?php

namespace App\Models;

use App\Models\infoChiFinanceModel;
use Illuminate\Support\Facades\Log;

class BankingSplit extends infochiFinanceModel {
    protected $table = 'banking_split';
// Join

    //put your code here
    protected $fillable = [
    ];

    public function BankingCategory() {
        return $this->belongsTo(BankingCategory::class, 'category');
    }

    public function BankingClass() {
        return $this->belongsTo(BankingClass::class, 'class');
    }

    public function BankingAccount() {
        return $this->belongsTo(BankingAccount::class, 'account');
    }

    public function BankingTransaction() {
        return $this->belongsTo(BankingTransaction::class, 'trans_num');
    }
}
