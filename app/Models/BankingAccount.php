<?php

namespace App\Models;

use App\Models\infoChiModel;
use App\Models\BankingAccountType;
use Illuminate\Support\Facades\Log;

class BankingAccount extends infoChiFinanceModel {

    protected $table = 'accounts';

    //put your code here
    protected $fillable = [
    ];

    public function BankingAccountType() {
        return $this->belongsTo(BankingAccountType::class, 'type');
    }

}
