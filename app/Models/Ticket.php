<?php

namespace App\Models;

use App\Models\infoChiModel;
use Illuminate\Support\Facades\Log;

class Ticket extends infochiModel {

    protected $table = "tracker_tickets";

    //put your code here
    protected $fillable = [
    ];

    public function TrackerStatus() {
        return $this->belongsTo(TrackerStatus::class);
    }

    public function TrackerReason() {
        return $this->belongsTo(TrackerReason::class);
    }
}
