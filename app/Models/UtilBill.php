<?php

namespace App\Models;

use App\Models\infoChiFinanceModel;
use Illuminate\Support\Facades\Log;

class UtilBill extends infochiFinanceModel {
    protected $table = 'util_bill_raw';

// Join

    //put your code here
    protected $fillable = [
    ];

    public function UtilClass() {
        return $this->belongsTo(UtilClass::class);
    }
}
