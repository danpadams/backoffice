<?php

namespace App\Models;

use App\Models\BankingCategory;
use App\Models\infoChiFinanceModel;
use App\Models\BankingTransfer;
use Illuminate\Support\Facades\Log;

class BankingTransaction extends infochiFinanceModel {
    protected $table = 'banking_transaction';

    //put your code here
    protected $fillable = [
    ];

    public function isFuture() {
        return $this->effdate > date('Y-m-d');
    }

    public function BankingCategory() {
        return $this->belongsTo(BankingCategory::class, 'cat');
    }

    public function BankingClass() {
        return $this->belongsTo(BankingClass::class, 'class');
    }

    public function BankingAccount() {
        return $this->belongsTo(BankingAccount::class, 'account');
    }

    public function TransferFrom() {
        return $this->belongsTo(BankingTransfer::class, 'id', 'trans_num_from');
    }
   public function TransferTo() {
        return $this->belongsTo(BankingTransfer::class, 'id', 'trans_num_to');
    }

}
