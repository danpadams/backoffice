<?php

namespace App\Models;

use App\Models\infoChiFinanceModel;
use Illuminate\Support\Facades\Log;

class BankingReportCategory extends infochiFinanceModel {

    protected $table = 'banking_report_category';
// Join

    //put your code here
    protected $fillable = [
    ];

    public function category()
    {
        return $this->belongsTo(BankingCategory::class, 'category');
    }
}
