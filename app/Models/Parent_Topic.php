<?php

namespace App\Models;

use App\Models\infoChiConcordanceModel;
use Illuminate\Support\Facades\Log;

class Parent_Topic extends infoChiConcordanceModel {
    protected $table = 'parent_topics';

    //put your code here
    protected $fillable = [
    ];

//    public function topic() {
//        return $this->belongsTo(Topic::class);
//    }

    public function parent() {
        return $this->belongsTo(Topic::class, 'parent', 'id');
    }
}
