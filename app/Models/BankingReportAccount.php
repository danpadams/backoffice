<?php

namespace App\Models;

use App\Models\infoChiFinanceModel;
use Illuminate\Support\Facades\Log;

class BankingReportAccount extends infochiFinanceModel {

    protected $table = 'banking_report_account';
// Join

    //put your code here
    protected $fillable = [
    ];

    public function account()
    {
        return $this->belongsTo(BankingAccount::class, 'account');
    }
}
