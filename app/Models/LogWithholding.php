<?php

namespace App\Models;

use App\Models\infoChiFinanceModel;
use Illuminate\Support\Facades\Log;

class LogWithholding extends infochiFinanceModel {
    protected $table = 'log_withholding';

    //put your code here
    protected $fillable = [
    ];

    public function BankingClass() {
        return $this->belongsTo(BankingClass::class);
    }

}
