<?php

namespace App\Models;

use App\Models\infoChiModel;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class TrackerFrequency extends infochiModel {

    protected $table = "tracker_frequencies";

    //put your code here
    protected $fillable = [
    ];

    public static function getNext($next, $freq) {
        $retval = (new Carbon($next));
        switch ($freq) {
            case 4: // Every 2 Weeks
                $retval = (new Carbon($next))->addWeek(2);
                break;
            case 2: // 3 Weeks
                $retval = (new Carbon($next))->addWeek(3);
                break;
            case 1: // Monthly - 1 Month
                $retval = (new Carbon($next))->addMonth(1);
                break;
             case 3: // Quarterly - 3 Months
                $retval = (new Carbon($next))->addMonth(3);
                break;
            case 6: // Every 6 Months
                $retval = (new Carbon($next))->addMonth(6);
                break;
            case 5: // Yearly - 12 Months
                $retval = (new Carbon($next))->addMonth(12);
                break;
          }
        return $retval->year . '-' . $retval->month . '-' . $retval->day;
    }
}
