<?php

namespace App\Models;

use Illuminate\Support\Facades\Log;
use App\Models\infoChiModel;
use App\Models\TrackerFrequency;
use App\User;

class TrackerReminder extends infochiModel {

    protected $table = "tracker_reminders";

    //put your code here
    protected $fillable = [
    ];

    public function TrackerFrequency() {
        return $this->belongsTo(TrackerFrequency::class);
    }

    public function User() {
        return $this->belongsTo(User::class);
    }

}
