<?php

namespace App\Models;

use App\Models\infoChiModel;
use Illuminate\Support\Facades\Log;

class ItemContain extends infochiModel
{

    //put your code here
    protected $fillable = [
    ];

    public function container()
    {
        return $this->belongsTo(Item::class, 'parent_id', 'id');
    }
    public function contains()
    {
        return $this->hasOne(Item::class, 'id', 'child_id');
    }

}
