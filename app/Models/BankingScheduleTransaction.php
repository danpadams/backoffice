<?php

namespace App\Models;

use App\Models\infoChiFinanceModel;
use Illuminate\Support\Facades\Log;

class BankingScheduleTransaction extends infochiFinanceModel {
    protected $table = 'banking_schedule_transaction';

    // Joins
    public function BankingAccount() {
        return $this->belongsto('App\Models\BankingAccount', 'account');
    }

    public function BankingCategory() {
        return $this->belongsto('App\Models\BankingCategory', 'cat');
    }

    public function BankingClass() {
        return $this->belongsto('App\Models\BankingClass', 'class');
    }

    //put your code here
    protected $fillable = [
    ];

}
