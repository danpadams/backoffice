<?php

namespace App\Models;

use App\Models\infoChiFinanceModel;
use Illuminate\Support\Facades\Log;

class BankingPendingTransfer extends infochiFinanceModel {
    protected $table = 'banking_pending_transfer';

    // Joins
    public function BankingAccountFrom() {
        return $this->belongsto('App\Models\BankingAccount', 'banking_account_from_id');
    }

    public function BankingAccountTo() {
        return $this->belongsto('App\Models\BankingAccount', 'banking_account_to_id');
    }

    public function BankingClass() {
        return $this->belongsto('App\Models\BankingClass', 'banking_class_id');
    }

    //put your code here
    protected $fillable = [
    ];

}
