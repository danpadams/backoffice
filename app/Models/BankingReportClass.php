<?php

namespace App\Models;

use App\Models\infoChiFinanceModel;
use Illuminate\Support\Facades\Log;

class BankingReportClass extends infochiFinanceModel {

    protected $table = 'banking_report_class';
// Join

    //put your code here
    protected $fillable = [
    ];

    public function class()
    {
        return $this->belongsTo(BankingClass::class, 'class');
    }
}
