/*
 This directive allows us to pass a function in on an enter key to do what we want.
 */
infoChi.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            // Enter
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.ngEnter);
                });
                event.preventDefault();
            }
            // Space Bar
            if (event.which === 32) {
                scope.$apply(function () {
                    scope.$eval(attrs.ngEnter);
                });
                event.preventDefault();
            }
        });
    };
});

infoChi.factory('infoChiSpinner', ['spinnerService',
    function (spinnerService) {
        service = {};
        count = 0;

        service.show = function () {
            if (count == 0) {
                spinnerService.show('mainSpinner');
            }
            count++;
        };
        service.hide = function () {
            if (count > 0) {
                count -= 1;
                if (count == 0) {
                    spinnerService.hide('mainSpinner');
                }
            }
        };
        return service;
    }
]);

infoChi.factory('infoChiTooltip', ['$http', function ($http) {
        service = {};
        service.tooltip = function (tooltip_id) {
            return $http.get('/web/tooltip/view/' + tooltip_id).then(function (data) {

                retval = data.data.body;
                return retval;
            });
        };
        return service;
    }
]);

infoChi.factory('infoChiTooltips', ['$localStorage',
    'infoChiTooltip',
    function ($localStorage, infoChiTooltipService) {
        service = {};
        service.tooltip = function (newId) {
//            window.localStorage.clear();
            $localStorage.$default({
                tooltips: {newId: null}
            });
            if ($localStorage.tooltips[newId] == undefined || $localStorage.tooltips[newId] == null) {
//                $localStorage.tooltips[newId] = '';
                infoChiTooltipService.tooltip(newId).then(function (response) {
                    console.log('#' + newId + ' : ' + response);
                    $localStorage.tooltips[newId] = response;

                });
            }
//            console.log($localStorage.tooltips[newId]);
            return $localStorage.tooltips[newId];

        };
        return service;
    }
]);

infoChi.factory('infoChiArray', [function () {
        service = {};
        service.in_array = function (needle, haystack, key_value, keys = []) {
            key0 = keys[0];

            for (var i = 0; i < haystack.length; i++) {
                if (haystack[i][key0][key_value] == needle) {
                    return true;
                }
            }
            return false;
        };
        service.removeFromList = function (with_id, haystack) {
            var retval = [];
            for (var i = 0; i < haystack.length; i++) {
                if (haystack[i].id != with_id.id) {
                    retval.push(haystack[i]);
                }
            }
            return retval;
        };
        service.setFromList = function (id, List) {
            for (var i = 0; i < List.length; i++) {
                if (List[i].id == id) {
                    return List[i];
                }
            }
        };
        return service;
    }
]);

// Function for filtering on a universal level
infoChi.factory('infoChiList', [function () {
        service = {};
        // Allow filter to be used in a generic arena
        filter = function (num, haystack, list_id) {
            list_id = typeof list_id !== 'undefined' ? list_id : '42';
            // Logic is to avoid items already in the list.
            if (haystack) {
                for (var i = 0; i < haystack.length; i++) {
                    if (num == haystack[i][list_id])
                        return false;
                }
            }
            return true;
       };
        service.filter = function (num, haystack, list_id) {
            return filter(num, haystack, list_id);
        };
        service.exists = function (num, haystack, list_id) {
//            list_id = typeof list_id !== 'undefined' ? list_id : 'id';
            // Logic is to avoid items already in the list.
            if (haystack) {
                for (var i = 0; i < haystack.length; i++) {
                    if (num == haystack[i][list_id]) {
                        return true;
                    }
                }
            }
            return false;
        };
        // Allow filter to be tested to determine size of list
        service.filterAll = function (needles, key, haystack, list_id) {
            var num = 0;
            for (var i = 0; i < needles.length; i++) {
                if (filter(needles[i][key], haystack, list_id)) {
                    num++;
                }
            }
            return num;
        };
        /** 
         * type_id = the type of the Method to pick
         * detail = array rep for the data to pick within
         * choice = array of the dropdown boxes
         * runFilter = callback function to the filter item
         */
        service.setMethod = function (type_id, detail, choice, runFilter) {
            for (key = 0; key < detail.length; key++) {
                value = detail[key];
                if (runFilter(value)) {
                    if (type_id) {
                        for (skey = 0; skey < value.PersonMethod.length; skey++) {
                            svalue = value.PersonMethod[skey];
                            if (svalue.person_method_type_id == type_id) {
                                choice[svalue.person_id] = svalue;
                            }
                        }
                    } else {
                        choice[value.id] = $scope.no_method;
                        // Set NO Contact
                    }
                }
            }

        };

        return service;
    }
]);

