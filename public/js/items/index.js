// $location is for Future Development
infoChi.controller('itemsIndexCtrl', ['$scope', '$http', '$localStorage',
    '$location', 'infoChiSpinner', 'infoChiFlash', '$routeParams',
    function ($scope, $http, $localStorage, $location, infoChiSpinner, infoChiFlash, $routeParams) {
        getData = function () {
            infoChiSpinner.show();
            $http.get('/web/items/').then(function (response) {
                $scope.Data = response.data;
                console.log($scope.data);
                infoChiSpinner.hide();
            });
        }

        // --
        $scope.$parent.setSecond('inventory');
        getData();
    }]
);
