// $location is for Future Development
infoChi.controller('itemsEditCtrl', ['$scope', '$http', '$localStorage',
    '$location', 'infoChiSpinner', 'infoChiFlash', '$routeParams',
    function ($scope, $http, $localStorage, $location, infoChiSpinner, infoChiFlash, $routeParams) {
        // infoChiFlash.show('This is a test.', 'success');
        getContainers = function () {
            infoChiSpinner.show();
            $http.get('/web/items/' + $scope.Data.id + '/found_in').then(function (response) {
                $scope.Container = response.data;
                infoChiSpinner.hide();
            });
            infoChiSpinner.show();
            $http.get('/web/items/' + $scope.Data.id + '/contains').then(function (response) {
                $scope.Contains = response.data;
                infoChiSpinner.hide();
            });
        }
        $scope.lookupBarcode = function () {
            console.log($scope.inputBarcode)
            lookupBarcode($scope.inputBarcode);
        }
        lookupBarcode = function (barcode_id) {
            barcodeId = $scope.inputBarcode;
            infoChiSpinner.show();
            $http.get('/web/items/' + barcode_id + '/barcode').then(function (response) {
                $scope.Data = response.data;
                getContainers();
                infoChiSpinner.hide();
            });
        }
        $scope.saveData = function () {
            data = $scope.Data;
            console.log(data);
            infoChiSpinner.show();
            $http.post('/web/items/save_data', data).then(function (result) {
                // location.href = "#!/banking/register/" + $scope.Data.account;
                infoChiSpinner.hide();
            });
        }

        $scope.newItem = function () {
            if ($routeParams.barcode_id === 'create') {
                retval = true;
            } else {
                retval = false;
            }
            return retval;
        }
        getContain = function () {
            infoChiSpinner.show();
            $http.get('/web/items/contain').then(function (response) {
                $scope.Contain = response.data;
                $scope.ContainArray = {};
                for (i = 0; i < $scope.Contain.length; i++) {
                    $scope.ContainArray[$scope.Contain[i].id] = $scope.Contain[i];
                }
                infoChiSpinner.hide();
            });
        }
        $scope.getContain = function () {
            getContain();
        }

        // Filter Contain
        $scope.runFilterContain = function (item) {
            length = 0;
            retval = true;
            id = item.id
            // Cannot add an item to the 25
            // list that is already in that list
            if ($scope.Contains != undefined) {
                length = $scope.Contains.length;
            }
            for (i = 0; i < length; i++) {
                // if ($scope.Contains[i].container)
                if (id === $scope.Contains[i].id) {
                    retval = false;
                    break;
                }

            }
            return retval;
        };
        // Filter Container
        $scope.runFilterContainer = function (item) {
            length = 0;
            retval = true;
            if ($scope.ContainArray[item.id].container !== 1) {
                retval = false;
            }
            id = item.id
            // Cannot add an item to the Contains list that is already in that list
            if ($scope.Container !== undefined) {
                if (item.id === $scope.Container.id) {
                    retval = false;
                }
            }
             return retval;
        };
        // Container/Contains Choices
        $scope.choiceContains = function (item) {
            console.log('choiceContains()');
            console.log(item);
            infoChiSpinner.show();
            item.item_id = $scope.Data.id;
            $http.post('/web/items/contains', item).then(function (result) {
                getContainers();
                infoChiSpinner.hide();
            });

        }
        $scope.choiceContainer = function (item) {
            console.log('choiceContainer()');
            console.log(item);
            infoChiSpinner.show();
            item.item_id = $scope.Data.id;
            $http.post('/web/items/container', item).then(function (result) {
                getContainers();
                infoChiSpinner.hide();
            });
        }
        $scope.removeContains = function (item) {
            infoChiSpinner.show();
            $http.post('/web/items/remove_item_container', item).then(function (result) {
                getContainers();
                infoChiSpinner.hide();
            });

        }

        // --
        $scope.$parent.setSecond('inventory');
        if ($routeParams.barcode_id === 'create') {
            console.log('Create!');
        } else {
            $scope.inputBarcode = $routeParams.barcode_id;
            lookupBarcode($routeParams.barcode_id);
            getContain();
        }
        $scope.$parent.setSecond('inventory');
    }]
);
