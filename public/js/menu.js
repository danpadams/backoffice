infoChi.controller('menuCtrl', ['$scope', '$http', '$localStorage',
    '$location', 'infoChiSpinner', 'infoChiFlash',
    function ($scope, $http, $localStorage, $location, infoChiSpinner, infoChiFlash) {
        $scope.template = {'menu': "html/menu.html"};
        $scope.groups = [
            {
                "label": "Banking",
                "var_name": "banking",
                "items": [
                    {
                        "href": "#!/banking/accounts",
                        "label": "Accounts"
                    },
                    // {
                    //     "href": "#!/layaway",
                    //     "label": "Index"
                    // },
                    {
                        "href": "#!/banking/schedule_transaction",
                        "label": "Schedule Transaction"
                    },
                    {
                        "href": "#!/banking/pending_transfer",
                        "label": "Pending Transfers"
                    },
                    {
                        "href": "#!/banking/reports",
                        "label": "Reports"
                    },                   {
                        "href": "#!/banking/reports/business",
                        "label": "Reports - Business List"
                    },
                    {
                        "href": "#!/banking/reports/payee",
                        "label": "Reports - Payee History"
                    },
                    {
                        "href": "#!/mileage/entries",
                        "label": "Log Mileage"
                    },
                    {
                        "href": "#!/banking/log_withholding/entries",
                        "label": "Log Withholding"
                    }
                ]
            },
            {
                "label": "Concordance",
                "var_name": "concordance",
                "items": [
                    {
                        "href": "http://concordance.infochi.net/",
                        "label": "View"
                    },
                    {
                        "href": "#!/concordance/nodes/0",
                        "label": "Nodes"
                    },
                    {
                        "href": "#!/concordance/topics/0",
                        "label": "Topics"
                    },
                    {
                        'href': '#!/concordance/authors/0',
                        'label': 'Authors'
                    }
                ]
            },
            {
                "label": "Tracker",
                "var_name": "tracker",
                "items": [{
                    "href": "#!/tracker/tickets",
                    "label": "Tickets"
                }, {
                    "href": "#!/tracker/projects",
                    "label": "Projects"
                }, {
                    "href": "#!/tracker/reminders",
                    "label": "Reminders"
                }]
            },
            // {
            //     "label": "JaDaProperties",
            //     "var_name": "jada_properties",
            //     "items": [{
            //         "href": "#!/jada_properties/rentals/1",
            //         "label": "Rentals"
            //     }, {
            //         "href": "#!/jada_properties/util_bills",
            //         "label": "Utility Bills"
            //     }, {
            //         "href": "#!/jada_properties/util_class",
            //         "label": "Utilities"
            //     }]
            // },
            {
                "label": "Inventory",
                "var_name": "inventory",
                "items": [{
                    "href": "#!/inventory",
                    "label": "Index"
                },
                    {
                        "href": "#!/items",
                        "label": "Items"
                    }
                ]
            },
            {
                "label": "Invest",
                "var_name": "invest",
                "items": [{
                    "href": "#!invest",
                    "label": "All Tickers",
                }, {
                    "href": "#!invest/mkt-index",
                    "label": "Market Indexes"
                }, {
                    "href": "#!invest/equity/input",
                    "label": "Equity Input"
                }]
            },
            {
                "label": "Journal",
                "var_name": "journal",
                "items": [{
                    "href": "#!journals",
                    "label": "Index",
                }]
            },
            {
                "label": "ServerConfig",
                "var_name": "server_config",
                "items": [{
                    "href": "#!server_config/apache",
                    "label": "Apache (HTTP)"
                }, {
                    "href": "#!server_config/username",
                    "label": "Username (FTP)"
                }, {
                    "href": "#!server_config/database",
                    "label": "Database (RDS - MySQL)"
                }, {
                    "href": "#!server_config/dns",
                    "label": "DNS (Bind)"
                }]
            }
        ];
        second = '';
        Breadcrumbs = {
            'log_withholding': [
                {
                    'name': 'Create Entry',
                    'id': 'CE',
                    'link': '#!/banking/log_withholding/edit/0'
                },
                {
                    'name': 'Create Transfer',
                    'id': 'CT',
                    'link': '#!/banking/log_withholding/transfer'
                },
                {
                    'name': 'Entries',
                    'id': 'E',
                    'link': '#!/banking/log_withholding/entries'
                },
                {
                    'name': 'Positions',
                    'id': 'POS',
                    'link': '#!/banking/log_withholding/positions'
                }
            ]
        };
        $scope.getBreadcrumbs = function(name) {
            return Breadcrumbs[name];
        }
        $scope.setSecond = function (menuGroup) {
            // console.log('setSecond() - '+ menuGroup);
            second = menuGroup;
        }
        $scope.valSecond = function () {
            return second;
        }
        $scope.getFirst = function () {
            retval = '';
            console.log();
            for (i = 0; i < $scope.groups.length; i++) {
                if (i > 0) retval += ' | ';
                retval += '<a on-click="setMenuSecond(\'' + $scope.groups[i].var_name + '\')">' + $scope.groups[i].label + '</a>';
            }
            return retval;
        }
    }
])
;
