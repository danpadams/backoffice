// $location is for Future Development
infoChi.controller('mileageEntryCtrl', ['$scope', '$http', '$localStorage',
    '$location', 'infoChiSpinner', 'infoChiFlash', '$routeParams',
    function ($scope, $http, $localStorage, $location, infoChiSpinner, infoChiFlash, $routeParams) {
        // infoChiFlash.show('This is a test.', 'success');
        getData = function () {
            if ($routeParams.mile_id != 0) {
                // Existing
                infoChiSpinner.show();
                $http.get('/web/mileage/entry/' + $routeParams.mile_id).then(function (response) {
                    parseData(response.data);
                    setMileType();
                    infoChiSpinner.hide();
                });
            } else {
                // Create
                $scope.Data = {};
                getProjects()
                $scope.updMileType({'id': 0, 'value': 'Choose Type'});
            }
        };
        // Used for Create Ticket
        getProjects = function () {
            infoChiSpinner.show();
            $scope.selectStatus = {'name': 'Choose Project'};
            $http.get('/web/mileage/mile_types').then(function (response) {
                $scope.optMileTypes = response.data;
                // $scope.optMileTypes = data.optMileTypes;
                infoChiSpinner.hide();
            });
        }
        // Used for Create Ticket - Above

        $scope.updMileType = function (item) {
            console.log('updMileType()');
            console.log($scope.selectMileType);
            $scope.Data.mile_type_id = item.id;
            $scope.Data.mile_type_value = item.name;
            setMileType();
            console.log($scope.selectMileType);
            // console.log($scope.Data);
        }
        $scope.saveData = function () {
            infoChiSpinner.show();
            $http.post('/web/mileage/entry/save_data', $scope.Data).then(function (response) {
                parseData(response.data);
                infoChiSpinner.hide();
            });
        }

        parseData = function (data) {
            $scope.Data = data;
            $scope.optMileTypes = data.optMileTypes;
            if ($routeParams.action_type == 'clone') {
                $scope.Data.id = 0;
                $routeParams = '';
            }
        }
        setMileType = function () {
            console.log('setMileType()');
            $scope.selectMileType = {
                'id': $scope.Data.mile_type_id,
                'value': $scope.Data.mile_type_value
            }
        }


        // $scope.Data = {'code': $routeParams.code};
        // $scope.Data.code = $routeParams.code;

        getData();
    }
]);
