infoChi.config(function ($routeProvider) {
    $routeProvider.when('/layaway/:account_id', {
        templateUrl: "/html/layaway/view.html",
        controller: "layawayViewCtrl"
    });
    $routeProvider.when("/layaway", {
        templateUrl: "/html/layaway/index.html",
        controller: "layawayIndexCtrl"
    });
    $routeProvider.when('/layaway/create/:account_id', {
        templateUrl: "/html/layaway/create.html",
        controller: "layawayCreateCtrl"
    });
    $routeProvider.when('/layaway/edit/:account_id', {
        templateUrl: "/html/layaway/edit.html",
        controller: "layawayEditCtrl"
    });
    $routeProvider.when('/banking/schedule_transaction', {
        templateUrl: "/html/banking/schedule_transaction/index.html",
        controller: "bankingScheduleTransactionIndexCtrl"
    });
    $routeProvider.when('/banking/pending_transfer', {
        templateUrl: "/html/banking/pending_transfer/index.html",
        controller: "bankingPendingTransferIndexCtrl"
    });
    $routeProvider.when('/banking/pending_transfer/:pending_transfer_id', {
        templateUrl: "/html/banking/pending_transfer/entry.html",
        controller: "bankingPendingTransferEntryCtrl"
    });
    $routeProvider.when('/banking/accounts', {
        templateUrl: "/html/banking/accounts.html",
        controller: "bankingAccountsCtrl"
    });
    $routeProvider.when('/banking/register/:account_id', {
        templateUrl: "/html/banking/register.html",
        controller: "bankingRegisterCtrl"
    });
    $routeProvider.when("/banking/create_transfer/:account_id", {
        templateUrl: "/html/banking/create_transfer.html",
        controller: "bankingCreateTransferCtrl"
    });
    $routeProvider.when("/banking/transaction/:transaction_id/:account_id", {
        templateUrl: "/html/banking/transaction.html",
        controller: "bankingTransactionCtrl"
    });
    $routeProvider.when('/banking/transaction/:transaction_id', {
        templateUrl: "/html/banking/transaction.html",
        controller: "bankingTransactionCtrl"
    });
    $routeProvider.when('/banking/reports/transaction/:report_id/:year/:month', {
        templateUrl: "/html/banking/reports/transaction.html",
        controller: "bankingReportsTransactionCtrl"
    });
    $routeProvider.when('/banking/reports/transaction/:report_id', {
        templateUrl: "/html/banking/reports/transaction.html",
        controller: "bankingReportsTransactionCtrl"
    });
    $routeProvider.when('/banking/reports/monthly/:report_id', {
        templateUrl: "/html/banking/reports/monthly.html",
        controller: "bankingReportsMonthlyCtrl"
    });
   $routeProvider.when('/banking/reports/business/:business_id', {
        templateUrl: "/html/banking/reports/business.html",
        controller: "bankingReportsBusinessCtrl"
    });
   $routeProvider.when('/banking/reports/business', {
        templateUrl: "/html/banking/reports/businesses.html",
        controller: "bankingReportsBusinessesCtrl"
    });
    $routeProvider.when('/banking/reports/payee', {
        templateUrl: "/html/banking/reports/payee.html",
        controller: "bankingReportsPayeeCtrl"
    });
    $routeProvider.when('/banking/reports', {
        templateUrl: "/html/banking/reports/reports.html",
        controller: "bankingReportsCtrl"
    });
    $routeProvider.when('/banking/reports/:report_id', {
        templateUrl: "/html/banking/reports/report.html",
        controller: "bankingReportEditCtrl"
    });
    $routeProvider.when('/banking/log_withholding/edit/:mile_id', {
        templateUrl: "/html/banking/log_withholding/entry.html",
        controller: "logWithholdingEntryCtrl"
    });
    $routeProvider.when('/banking/log_withholding/edit/:mile_id/:action_type', {
        templateUrl: "/html/banking/log_withholding/entry.html",
        controller: "logWithholdingEntryCtrl"
    });
    $routeProvider.when('/banking/log_withholding/transfer/', {
        templateUrl: "/html/banking/log_withholding/transfer.html",
        controller: "logWithholdingTransferCtrl"
    });
    $routeProvider.when('/banking/log_withholding/entries', {
        templateUrl: "/html/banking/log_withholding/entries.html",
        controller: "logWithholdingEntriesCtrl"
    })
    $routeProvider.when('/banking/log_withholding/positions', {
        templateUrl: "/html/banking/log_withholding/positions.html",
        controller: "logWithholdingPositionsCtrl"
    })
    $routeProvider.when('/banking/log_withholding/position/:position_id', {
        templateUrl: "/html/banking/log_withholding/position.html",
        controller: "logWithholdingPositionCtrl"
    })


    $routeProvider.when('/concordance/nodes/:node_id', {
        templateUrl: "/html/concordance/node.html",
        controller: "concordanceNodesCtrl"
    });
    $routeProvider.when('/concordance/topics/:topic_id', {
        templateUrl: "/html/concordance/topic.html",
        controller: "concordanceTopicsCtrl"
    });
    $routeProvider.when('/concordance/authors/:author_id', {
        templateUrl: "/html/concordance/author.html",
        controller: "concordanceAuthorsCtrl"
    });


    $routeProvider.when('/mileage/edit/:mile_id', {
        templateUrl: "/html/mileage/entry.html",
        controller: "mileageEntryCtrl"
    });
    $routeProvider.when('/mileage/edit/:mile_id/:action_type', {
        templateUrl: "/html/mileage/entry.html",
        controller: "mileageEntryCtrl"
    });
    $routeProvider.when('/mileage/entries', {
        templateUrl: "/html/mileage/entries.html",
        controller: "mileageEntriesCtrl"
    })

    $routeProvider.when('/inventory/:inventory_group_id', {
        templateUrl: "/html/inventory/view.html",
        controller: "inventoryViewCtrl"
    });
    $routeProvider.when('/inventory', {
        templateUrl: "/html/inventory/index.html",
        controller: "inventoryIndexCtrl"
    });
    $routeProvider.when('/inventory/edit/:inventory_item_id', {
        templateUrl: "/html/inventory/edit.html",
        controller: "inventoryEditCtrl"
    });
    $routeProvider.when('/inventory/edit/:inventory_item_id/inventory_id/:inventory_id', {
        templateUrl: "/html/inventory/edit.html",
        controller: "inventoryEditCtrl"
    });

    $routeProvider.when('/items', {
        templateUrl: "/html/items/index.html",
        controller: "itemsIndexCtrl"
    });
    $routeProvider.when('/items/:barcode_id', {
        templateUrl: "/html/items/edit.html",
        controller: "itemsEditCtrl"
    });

    // ----------------------------

    $routeProvider.when("/invest", {
        templateUrl: "/html/invest/index.html",
        controller: "investIndexCtrl"
    });
    $routeProvider.when("/invest/mkt-index", {
        templateUrl: "/html/invest/mkt-index.html",
        controller: "investMktIndexCtrl"
    });
    $routeProvider.when("/invest/mkt-index/:index_num", {
        templateUrl: "/html/invest/mkt-index-data.html",
        controller: "investMktIndexDataCtrl"
    });
    $routeProvider.when("/invest/:ticker", {
        templateUrl: "/html/invest/view.html",
        controller: "investViewCtrl"
    });


    // ----------------------------

    $routeProvider.when("/journals", {
        templateUrl: "/html/journal/index.html",
        controller: "journalIndexCtrl"
    });
    $routeProvider.when('/journals/:journal_id', {
        templateUrl: "/html/journal/entry.html",
        controller: "journalEntryCtrl"
    });
    $routeProvider.when('/journal/:journal_entry_id', {
        templateUrl: "/html/journal/edit.html",
        controller: "journalEditCtrl"
    });
    $routeProvider.when('/journal/:journal_entry_id/journal_id/:journal_id', {
        templateUrl: "/html/journal/edit.html",
        controller: "journalEditCtrl"
    });

    // $routeProvider.when("/invest/equity/input", {
    //     templateUrl: "/html/invest/equity/input.html",
    //     controller: "investEquityInputCtrl"
    // });
    // $routeProvider.when("/invest/equity/input/:trade_id", {
    //     templateUrl: "/html/invest/equity/input.html",
    //     controller: "investEquityInputCtrl"
    // });
    $routeProvider.when('/tracker/tickets', {
        templateUrl: "/html/tracker/tickets.html",
        controller: "trackerTicketsCtrl"
    });
    $routeProvider.when('/tracker/tickets/:tracker_project_id', {
        templateUrl: "/html/tracker/tickets.html",
        controller: "trackerTicketsCtrl"
    });
    $routeProvider.when('/tracker/projects', {
        templateUrl: "/html/tracker/projects.html",
        controller: "trackerProjectsCtrl"
    });
    $routeProvider.when('/tracker/create', {
        templateUrl: "/html/tracker/ticket.html",
        controller: "trackerTicketCtrl"
    });
    $routeProvider.when('/tracker/create/:tracker_project_id', {
        templateUrl: "/html/tracker/ticket.html",
        controller: "trackerTicketCtrl"
    });
    $routeProvider.when('/tracker/reminders', {
        templateUrl: "/html/tracker/reminders.html",
        controller: "trackerRemindersCtrl"
    });
    $routeProvider.when('/tracker/reminder', {
        templateUrl: "/html/tracker/reminder.html",
        controller: "trackerReminderCtrl"
    });
    $routeProvider.when('/tracker/reminder/:id', {
        templateUrl: "/html/tracker/reminder.html",
        controller: "trackerReminderCtrl"
    });
    $routeProvider.when('/tracker/:code', {
        templateUrl: "/html/tracker/ticket.html",
        controller: "trackerTicketCtrl"
    });

    $routeProvider.when('/jada_properties/rentals/:rental_property_id', {
        templateUrl: "/html/jada_properties/rentals.html",
        controller: "jadaPropertiesRentalsCtrl"
    });
    $routeProvider.when('/jada_properties/rental/:id', {
        templateUrl: "/html/jada_properties/rental.html",
        controller: "jadaPropertiesRentalCtrl"
    });
    $routeProvider.when('/jada_properties/rental', {
        templateUrl: "/html/jada_properties/rental.html",
        controller: "jadaPropertiesRentalCtrl"
    });
    $routeProvider.when('/jada_properties/util_bill/:id', {
        templateUrl: "/html/jada_properties/util_bill.html",
        controller: "jadaPropertiesUtilBillCtrl"
    });
    $routeProvider.when('/jada_properties/util_bills/:util_class_id', {
        templateUrl: "/html/jada_properties/util_bills.html",
        controller: "jadaPropertiesUtilBillsCtrl"
    });
    $routeProvider.when('/jada_properties/util_bills', {
        templateUrl: "/html/jada_properties/util_bills.html",
        controller: "jadaPropertiesUtilBillsCtrl"
    });
    $routeProvider.when('/jada_properties/util_class', {
        templateUrl: "/html/jada_properties/util_class.html",
        controller: "jadaPropertiesUtilClassCtrl"
    });

    // ServerConfig
    $routeProvider.when('/server_config/apache', {
        templateUrl: "/html/server_config/apache.html",
        controller: "serverConfigApacheIndexCtrl"
    });
    $routeProvider.when('/server_config/username', {
        templateUrl: "/html/server_config/username.html",
        controller: "serverConfigUsernameIndexCtrl"
    });
    $routeProvider.when('/server_config/database', {
        templateUrl: "/html/server_config/database.html",
        controller: "serverConfigDatabaseIndexCtrl"
    });
    $routeProvider.when('/server_config/dns', {
        templateUrl: "/html/server_config/dns.html",
        controller: "serverConfigDnsIndexCtrl"
    });

    $routeProvider.otherwise({
        templateUrl: "/html/sitemap.html",
        controller: "menuCtrl"
    });

});
