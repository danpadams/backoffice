// 2021-06-21 Add SortOrder via Calling Controller
infoChi.controller("reportGeneratorCtrl", ['$scope', '$http', 'infoChiSpinner',
    function ($scope, $http, infoChiSpinner) {
        $scope.$on('getData', function(event, opt) {
            $scope.data = opt.data;
            console.log(opt);
            if (opt.predicate != undefined) {
                $scope.predicate = opt.predicate;
                $scope.sortColumn = opt.predicate;
            }
            if (opt.reverse != undefined) {
                $scope.reverse = opt.reverse;
            }
        });
        $scope.order = function (predicate) {
            $scope.predicate = predicate;
            $scope.reverse = ($scope.sortColumn === predicate) ? !$scope.reverse : false;
            $scope.sortColumn = predicate;
        };

        $scope.setFilter = function (item, type) {
            // Store the filter data, allowing for multiple types
            if (type) {
                $scope.valFilterType = type;
            }
            $scope.valFilter = item;
        };
        $scope.runFilter = function (item) {
            // Process the filter, allowing for a valFilter of 0 to reset the filter
            if ($scope.valFilter == '') {
                return true;
            } else {
                if (item[$scope.valFilterType] == $scope.valFilter) {
                    return true;
                }
            }
            return false;
        };

        // --
        $scope.valFilterType = '';
        $scope.valFilter = '';
        $scope.pageSize = 10;
        $scope.predicate = '';
        $scope.reverse = false;
        $scope.Url = {};
        // $scope.getData();
        console.log("reportGeneratorCtrl");
        $scope.template = {'report-generator': "html/util/report-generator.html"};
    }
]);