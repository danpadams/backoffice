// $location is for Future Development
infoChi.controller('layawayIndexCtrl', ['$scope', '$http', '$localStorage',
    '$location', 'infoChiSpinner', 'infoChiFlash',
    function ($scope, $http, $localStorage, $location, infoChiSpinner, infoChiFlash) {
        // infoChiFlash.show('This is a test.', 'success');
        getData = function () {
            infoChiSpinner.show();
            $http.get('/web/accounts').then(function (response) {
                $scope.$broadcast('getData', { data: response.data});
                infoChiSpinner.hide();
            });
        };


        // --
        console.log('LayAway');
        getData();
    }]
);
