// $location is for Future Development
infoChi.controller('layawayViewCtrl', ['$scope', '$http', '$localStorage',
    '$location', 'infoChiSpinner', 'infoChiFlash', '$routeParams',
    function ($scope, $http, $localStorage, $location, infoChiSpinner, infoChiFlash, $routeParams) {
        // infoChiFlash.show('This is a test.', 'success');
        getData = function () {
            infoChiSpinner.show();
            infoChiSpinner.show();
            $http.get('/web/accounts/' + account_id + '/view').then(function (response) {
                $scope.data = response.data;

                // infoChiSpinner.hide();
                infoChiSpinner.hide();
            });
            $http.get('/web/accounts/' + account_id + '/edit').then(function (response) {
                $scope.top = response.data;

                // infoChiSpinner.hide();
                infoChiSpinner.hide();
            });

        };

        getBreadcrumbs = function () {
            $scope.Breadcrumbs = [
                {
                    'name': 'Layaway Accounts',
                    'link': '#!/layaway'
                },
                {
                    'name': 'Current Page',
                    'link': ''
                },
                {
                    'name': 'Create New',
                    'link': '#!/layaway/create/'  + account_id
                },
            ];
        }

        // --
        var account_id = $routeParams.account_id;
        $scope.account_id = account_id;
        $scope.Breadcrumbs = [];
        $scope.BreadcrumbsUrl = '/html/util/breadcrumbs.html';
        getBreadcrumbs();
        getData();
    }]
);
