// $location is for Future Development
infoChi.controller('layawayCreateCtrl', ['$scope', '$http', '$localStorage',
    '$location', 'infoChiSpinner', 'infoChiFlash', '$routeParams',
    function ($scope, $http, $localStorage, $location, infoChiSpinner, infoChiFlash, $routeParams) {
        // infoChiFlash.show('This is a test.', 'success');
        $scope.saveData = function () {
            infoChiSpinner.show();
            $http.post('/web/accounts/create_entry', $scope.Data).then(function (response) {
                window.location.replace('#!/layaway/' + account_id);
                infoChiSpinner.hide();
            });
        };

        getBreadcrumbs = function () {
            $scope.Breadcrumbs = [
                {
                    'name': 'Layaway Accounts',
                    'link': '#!/layaway'
                },
                {
                    'name': 'Account List',
                    'link': '#!/layaway/' + $scope.Data.account_id
                },
                {
                    'name': 'Current Page',
                    'link': ''
                },
            ];
        }

        function formatDate(dateStringToParse) {
            // var msec = Date.parse(dateStringToParse)
            var d = new Date();

            var month = '' + (d.getMonth() + 1);
                var day = '' + d.getDate();
                var year = d.getFullYear();

            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;

            retval = [year, month, day].join('-');
            return retval;
        }

        // --
        var account_id = $routeParams.account_id;
        $scope.Data = {
            account_id: account_id,
            amount: 0,
            actdate: formatDate('today'),
            name: 'Transaction Name'
        };
        $scope.Breadcrumbs = [];
        $scope.BreadcrumbsUrl = '/html/util/breadcrumbs.html';
        getBreadcrumbs();
         console.log($scope.Data);
    }]
);
