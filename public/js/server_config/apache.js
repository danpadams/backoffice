// $location is for Future Development
infoChi.controller('serverConfigApacheIndexCtrl', ['$scope', '$http', '$localStorage',
    '$location', 'infoChiSpinner', 'infoChiFlash',
    function ($scope, $http, $localStorage, $location, infoChiSpinner, infoChiFlash) {
        // infoChiFlash.show('This is a test.', 'success');
        getData = function () {
            infoChiSpinner.show();
            $http.get('/web/server_config/website').then(function (response) {
                console.log(response.data);
                $scope.Data = response.data;
                infoChiSpinner.hide();
            });
        };
        $scope.getData = function getData() {
            getData();
        }

        // --
        console.log("Apache");
        getData();
    }]
);

