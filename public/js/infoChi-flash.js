infoChi.factory('infoChiFlash', ['Flash',
    function (Flash) {
        service = {};
        // Wrapper function using either array or text for Flash message
        service.show = function (message, cssClass, timeout) {
            if (!Array.isArray(message)) {
                message = [message];
            }
            for (var i = 0; i < message.length; i++) {
                Flash.create(cssClass, message[i], timeout, {class: 'custom-class', id: 'custom-id'}, true);
            }
        };
        // Passthrough to clear ALL flash messages
        service.clear = function () {
            Flash.clear();
        };
        return service;
    }
]);