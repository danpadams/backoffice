// $location is for Future Development
infoChi.controller('jadaPropertiesRentalCtrl', ['$scope', '$http', '$localStorage',
    '$location', 'infoChiSpinner', 'infoChiFlash', '$routeParams',
    function ($scope, $http, $localStorage, $location, infoChiSpinner, infoChiFlash, $routeParams) {
        // infoChiFlash.show('This is a test.', 'success');
        getData = function () {
            getPlatforms();
            if ($routeParams.id) {
                // Existing
                infoChiSpinner.show();
                $http.get('/web/jada_properties/booking/' + $routeParams.id).then(function (response) {
                    $scope.Data = response.data;
                    $scope.selectPlatform={"value":response.data.rental_platform_value};
                    console.log(response.data);
                    infoChiSpinner.hide();
                });
            } else {
                // Create
                console.log('Create');
                $scope.Data = {"property":1};
            }
        };
        $scope.getData = function () {
            getData();
        }
        getPlatforms = function () {
            infoChiSpinner.show();
            $http.get('/web/jada_properties/platforms/').then(function (response) {
                $scope.optPlatforms = response.data;
                console.log(response.data);
                infoChiSpinner.hide();
            });
        }
        // Get Data Above
        $scope.setPlatform = function (item) {
            $scope.Data.rental_platform_id = item.id;
        }

        $scope.saveData = function () {
            infoChiSpinner.show();
            console.log('saveData()');
            $http.post('/web/jada_properties/booking', $scope.Data).then(function (response) {
                $scope.Data = response.data;
                console.log($scope.Data);
                infoChiSpinner.hide();
            });
        }

        var id = $routeParams.rental_property_id;
        getData();
    }
]);
