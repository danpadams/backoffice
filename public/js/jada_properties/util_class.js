// $location is for Future Development
infoChi.controller('jadaPropertiesUtilClassCtrl', ['$scope', '$http', '$localStorage',
    '$location', 'infoChiSpinner', 'infoChiFlash', '$routeParams',
    function ($scope, $http, $localStorage, $location, infoChiSpinner, infoChiFlash, $routeParams) {
        // infoChiFlash.show('This is a test.', 'success');
        getData = function () {
            infoChiSpinner.show();
            $http.get('/web/jada_properties/util_class').then(function (response) {
                $scope.Data = response.data;
                console.log($scope.Data);
                infoChiSpinner.hide();
            });
        };
        $scope.getData = function () {
            getData();
        }
        $scope.filterItem = function (item) {
            // console.log(item);
            // console.log('filterItem');
            return true;
        }

        // --
        var rental_property_id = $routeParams.rental_property_id;
        getData();
    }]
);
