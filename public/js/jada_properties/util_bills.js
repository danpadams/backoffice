// $location is for Future Development
infoChi.controller('jadaPropertiesUtilBillsCtrl', ['$scope', '$http', '$localStorage',
    '$location', 'infoChiSpinner', 'infoChiFlash', '$routeParams',
    function ($scope, $http, $localStorage, $location, infoChiSpinner, infoChiFlash, $routeParams) {
        // infoChiFlash.show('This is a test.', 'success');
        getData = function () {
            console.log(util_class_id);
            if (util_class_id) {
                url='/web/jada_properties/util_bills/' + util_class_id;
            } else {
                url='/web/jada_properties/util_bills';
            }
            console.log(url);
            infoChiSpinner.show();
            $http.get(url).then(function (response) {
                $scope.Data = response.data;
                console.log($scope.Data);
                infoChiSpinner.hide();
            });
        };
        $scope.getData = function () {
            getData();
        }
        $scope.filterItem = function (item) {
            // console.log(item);
            // console.log('filterItem');
            return true;
        }

        // --
        util_class_id = $routeParams.util_class_id;
        getData();
    }]
);
