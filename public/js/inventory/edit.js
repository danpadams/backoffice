// $location is for Future Development
infoChi.controller('inventoryEditCtrl', ['$scope', '$http', '$localStorage',
    '$location', 'infoChiSpinner', 'infoChiFlash', '$routeParams',
    function ($scope, $http, $localStorage, $location, infoChiSpinner, infoChiFlash, $routeParams) {
        // infoChiFlash.show('This is a test.', 'success');
        $scope.saveData = function() {
            infoChiSpinner.show();
            $http.post('/web/inventory/item/' + inventory_item_id, $scope.Data).then(function (response) {
                console.log(response.data);
                infoChiSpinner.hide();
                $scope.Data = response.data;
                if (inventory_item_id == 0) {

                }
                infoChiSpinner.hide();

            });
        };

        getData = function () {
            infoChiSpinner.show();
            $http.get('/web/inventory/item/' + inventory_item_id).then(function (response) {
                console.log(inventory_item_id);
                if (inventory_item_id == 0) {
                    response.data.inventory_id = $routeParams.inventory_id;
                }
                $scope.Data = response.data;
                console.log($scope.Data);
                getBreadcrumbs();
                infoChiSpinner.hide();
            });
        };
        getBreadcrumbs = function () {
            $scope.Breadcrumbs = [
                {
                    'name': 'Inventory Groups',
                    'link': '#!/inventory'
                },
                {
                    'name': 'Inventory Index',
                    'link': '#!/inventory/' + $scope.Data.inventory_groups_id
                },
                {
                    'name': 'Current Page',
                    'link': ''
                },
            ];
        }

        // --
        var inventory_item_id = $routeParams.inventory_item_id;
        $scope.Breadcrumbs = [];
        $scope.BreadcrumbsUrl = '/html/util/breadcrumbs.html'
        getData();
    }]
);
