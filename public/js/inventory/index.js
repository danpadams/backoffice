// $location is for Future Development
infoChi.controller('inventoryIndexCtrl', ['$scope', '$http', '$localStorage',
    '$location', 'infoChiSpinner', 'infoChiFlash',
    function ($scope, $http, $localStorage, $location, infoChiSpinner, infoChiFlash) {
        // infoChiFlash.show('This is a test.', 'success');
        getData = function () {
            infoChiSpinner.show();
            $http.get('/web/inventory').then(function (response) {
                $scope.$broadcast('getData', { data: response.data});
                infoChiSpinner.hide();
            });
        };

        // --
        getData();
    }]
);

