// $location is for Future Development
infoChi.controller('inventoryViewCtrl', ['$scope', '$http', '$localStorage',
    '$location', 'infoChiSpinner', 'infoChiFlash', '$routeParams',
    function ($scope, $http, $localStorage, $location, infoChiSpinner, infoChiFlash, $routeParams) {
        // infoChiFlash.show('This is a test.', 'success');
        getData = function () {
            infoChiSpinner.show();
            $http.get('/web/inventory/' + inventory_group_id).then(function (response) {
                $scope.$broadcast('getData', { data: response.data});
                infoChiSpinner.hide();
            });
        };
        
        // --
        var inventory_group_id = $routeParams.inventory_group_id;
        getData();
    }]
);
