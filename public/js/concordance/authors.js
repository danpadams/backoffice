// $location is for Future Development
infoChi.controller('concordanceAuthorsCtrl', ['$scope', '$http', '$localStorage',
        '$location', 'infoChiSpinner', 'infoChiFlash', '$routeParams', '$sce',
        function ($scope, $http, $localStorage, $location, infoChiSpinner, infoChiFlash, $routeParams, $sce) {
            getData = function () {
                console.log("Get Author")
                console.log("author_id = " + author_id);
                 if (author_id != 0) {
                     // infoChiSpinner.show();
                     $http.get('/web/concordance/authors/' + author_id).then(function (response) {
                        // infoChiSpinner.hide();
                        $scope.Data = response.data;
                        console.log($scope.Data);
                    });
                } else {
                    $scope.Data = {id: 0, sort: '', display: ''};
                }

                $scope.List = [];
            }

            $scope.saveData = function () {
                console.log('saveData() - WIP');
                infoChiSpinner.show();
                $http.post('/web/concordance/authors/saveData', $scope.Data).then(function (response) {
                    $scope.Data.id=response.data.id;
                    infoChiSpinner.hide();
                });
                console.log($scope.Data);
            }

            $scope.startFresh = function () {
                console.log('startFresh() - WIP');
                getData();
            }

            if ($routeParams.author_id) {
                author_id = $routeParams.author_id;
            } else {
                author_id = 0;
            }
            // $scope.Books = [];
            // $scope.Authors = [];
            $scope.List = [];
            getData();
            $scope.$parent.setSecond('concordance');
        }
    ]
);
