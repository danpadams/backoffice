// $location is for Future Development
infoChi.controller('concordanceNodesCtrl', ['$scope', '$http', '$localStorage',
        '$location', 'infoChiSpinner', 'infoChiFlash', '$routeParams', '$sce',
        function ($scope, $http, $localStorage, $location, infoChiSpinner, infoChiFlash, $routeParams, $sce) {
            // infoChiFlash.show('This is a test.', 'success');
            getData = function () {
                getTypesData();
                if (node_id > 0) {
                    infoChiSpinner.show();
                    $http.get('/web/concordance/node/' + node_id).then(function (response) {
                        $scope.Data = response.data;
                        // Set Type in Dropdown
                        setTypesDropdown();
                        if ($scope.Data.type == 1) {
                            getVerseData();
                            setBooksDropdown();
                        } else {
                            console.log("Type <> 1")
                            getAuthorsData();
                        }
                        infoChiSpinner.hide();
                    });
                    getBooksData();
                } else {
                    $scope.Data.type = 0;
                }
            };

            getVerseData = function () {
                $scope.Data.verse = JSON.parse($scope.Data.data);
            }

            getTypesData = function () {
                infoChiSpinner.show();
                $http.get('/web/concordance/node/types').then(function (response) {
                    $scope.Types = response.data;
                    infoChiSpinner.hide();
                });
            }

            getTopicsData = function () {
                console.log('getTopicsData()');
                infoChiSpinner.show();
                $http.get('/web/concordance/node/topics').then(function (response) {
                    $scope.Topics = response.data;
                    $scope.Topics.push(defaultData.Topic)
                    console.log('getTopicsData');
                    console.log(response.data);
                    infoChiSpinner.hide();
                });
            }

            getBooksData = function () {
                if ($scope.Books.length == 0) {
                    infoChiSpinner.show();
                    $http.get('/web/concordance/node/books').then(function (response) {
                        $scope.Books = response.data;
                        infoChiSpinner.hide();
                    });
                }
            }

            getAuthorsData = function () {
                console.log("getAuthorsData()");
                if ($scope.Authors.length == 0) {
                    infoChiSpinner.show();
                    $http.get('/web/concordance/node/authors').then(function (response) {
                        $scope.Authors = response.data;
                        setAuthorsDropdownW();
                        infoChiSpinner.hide();
                    });
                } else {
                    setAuthorsDropdownW();
                }

            }

            setAuthorsDropdownW = function () {
                console.log("setAuthorsDropdownW()");
                for (i = 0; i < $scope.Authors.length; i++) {
                    if ($scope.Authors[i].id == $scope.Data.author_id) {
                        $scope.s_author.display = $scope.Authors[i].display
                        break;
                    }
                }
            }

            setTypesDropdown = function () {
                if ($scope.Types.length > 0) {
                    // console.log('setTypeDropdown - Have it');
                    setTypesDropdownW()
                } else {
                    // console.log('setTypeDropdown - Not Yet');
                    $scope.$watch('Types', function () {
                        setTypesDropdownW();
                    });
                }
            }

            setTypesDropdownW = function () {
                for (i = 0; i < $scope.Types.length; i++) {
                    if ($scope.Types[i].id == $scope.Data.type) {
                        $scope.s_type.name = $scope.Types[i].name
                        break;
                    }
                }
            }

            setBooksDropdown = function () {
                if ($scope.Books.length > 0) {
                    // console.log('setBooksDropdown - Have it');
                    setBooksDropdownW();
                } else {
                    // console.log('setBooksDropdown - Not Yet.');
                    $scope.$watch('Books', function () {
                        setBooksDropdownW();
                    });
                }
            }

            setBooksDropdownW = function () {
                for (i = 0; i < $scope.Books.length; i++) {
                    if ($scope.Books[i].ID == $scope.Data.verse.Book_Num) {
                        $scope.s_book.Name = $scope.Books[i].Name
                    }
                }
            }

            $scope.updateType = function (item) {
                $scope.Data.type = item.id;
                if ($scope.Data.type == 1) {
                    getBooksData();
                    if ($scope.Data.verse == undefined) {
                        $scope.Data.verse = {Book_Num:0,Chapter_Start:0, Chapter_End:0,Verse_Start:0,Verse_End:0};
                    }
                } else {
                    getAuthorsData();
                }
            }

           $scope.updateTopic = function (item) {
                if (item.id == 0) {
                    $scope.Data.topic_id = '';
                } else {
                    $scope.Data.topic_id = item.id;
                }
               console.log('New Topic: \'' + $scope.Data.topic_id + '\'');
            }

            $scope.updateBook = function (item) {
                $scope.Data.verse.Book_Num = item.ID;

                // $scope.s_book.name = item.Name;
                $scope.updateVerse();
            }

            $scope.updateVerse = function () {
                $scope.Data.data = JSON.stringify($scope.Data.verse);
            }

            $scope.refreshData = function () {
                console.log('refreshData()');
                $scope.Authors = [];
                getAuthorsData();

            }

            $scope.updateAuthor = function (item) {
                $scope.Data.author_id = item.id;
            }

            $scope.saveData = function () {
                console.log('saveData()');
                if ($scope.Data.type > 1) {
                    $scope.Data.data = JSON.stringify($scope.Data.Data);
                }
                if ($scope.Data.data == undefined) {
                    // Not Colored Correctly
                    infoChiFlash.show('data value must be present.', 'danger');
                    console.log('data value must be present');
                }
                infoChiSpinner.show();
                $http.post('/web/concordance/node/saveData', $scope.Data).then(function (response) {
                    $scope.Data.id=response.data.id;
                    infoChiSpinner.hide();
                });
            }

            $scope.startFresh = function () {
                console.log("Test");
                if ($routeParams.node_id) {
                    node_id = $routeParams.node_id;
                } else {
                    node_id = 0;
                }
                $scope.Data = {id: node_id};

                // $scope.Data = {};
                $scope.s_type = {name: 'Select Type'};
                $scope.s_book = {Name: 'Select Book'};
                $scope.s_author = {Name: 'Select Author'};
                $scope.Data.topic_num = '';
                $scope.s_topic = defaultData.Topic;
                // getData();
            }

            // --
            if ($routeParams.node_id) {
                node_id = $routeParams.node_id;
            } else {
                node_id = 0;
            }
            $scope.Data = {id: node_id};

            defaultData = {Topic: {id:0, name:"No Topic Selected (0)"}};
            $scope.Books = [];
            $scope.Authors = [];
            $scope.Types = [];
            $scope.startFresh();
            getData();
            getTopicsData();
            console.log('nodes.js');
            $scope.$parent.setSecond('concordance');
        }
    ]);
