// $location is for Future Development
infoChi.controller('trackerReminderCtrl', ['$scope', '$http', '$localStorage',
    '$location', 'infoChiSpinner', 'infoChiFlash', '$routeParams',
    function ($scope, $http, $localStorage, $location, infoChiSpinner, infoChiFlash, $routeParams) {
        // infoChiFlash.show('This is a test.', 'success');
        getData = function () {
            if ($routeParams.id) {
                // Existing
                infoChiSpinner.show();
                $http.get('/web/tracker/reminder/' + $routeParams.id).then(function (response) {
                    $scope.Data = response.data;
                    $scope.selectFrequency = {
                        'id': $scope.Data.tracker_frequncy_id,
                        'value': $scope.Data.Frequency
                    }
                    console.log($scope.Data);
                    infoChiSpinner.hide();
                });
            } else {
                // Create
                console.log('Create');
                $scope.Data = {};
                $scope.selectFrequency = {"value":'Select Frequency'}
            }
            getFrequencies();
        };
        // Used for Create Ticket
        getFrequencies = function () {
            infoChiSpinner.show();
            $scope.selectStatus = {'name':'Choose Project'};
            $http.get('/web/tracker/frequency').then(function (response) {
                console.log(response.data)
                $scope.optFrequencies = response.data;
                infoChiSpinner.hide();
            });
        }
        $scope.setProject = function (item) {
            $scope.Data.Prefix = item.prefix
            getNextTicket(item.prefix);
        }
        getNextTicket = function (prefix) {
            infoChiSpinner.show();
            $http.get('/web/tracker/' + prefix + '/get_next_ticket').then(function (response) {
                $scope.Data.nextProject = prefix + '-' + response.data;
                console.log($scope.Data);
                infoChiSpinner.hide();
            });
        }
        // Used for Create Ticket - Above

        $scope.updFrequency = function (item) {
            console.log('updFrequency()');
            console.log(item);
            $scope.Data.tracker_frequency_id = item.id;
            $scope.selectFrequency = {
                'id': item.status_id,
                'value': item.status_value
            }
        }
        $scope.saveData = function () {
            infoChiSpinner.show();
            console.log('saveData()');
            $http.post('/web/tracker/reminder', $scope.Data).then(function (response) {
                $scope.Data = response.data;
                console.log($scope.Data);
                infoChiSpinner.hide();
            });
        }


        // $scope.Data = {'code': $routeParams.code};
        // $scope.Data.code = $routeParams.code;
        $scope.$parent.setSecond('tracker');
        getData();
    }
]);
