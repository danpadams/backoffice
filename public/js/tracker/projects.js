// $location is for Future Development
infoChi.controller('trackerProjectsCtrl', ['$scope', '$http', '$localStorage',
    '$location', 'infoChiSpinner', 'infoChiFlash', '$routeParams',
    function ($scope, $http, $localStorage, $location, infoChiSpinner, infoChiFlash, $routeParams) {
        // infoChiFlash.show('This is a test.', 'success');
        getData = function () {
            infoChiSpinner.show();
            $http.get('/web/tracker/projects').then(function (response) {
                $scope.Data = response.data;
                console.log($scope.Data);
                infoChiSpinner.hide();
            });
        };
        $scope.getData = function () {
            getData();
        }
        $scope.filterItem = function (item) {
            // console.log(item);
            // console.log('filterItem');
            return true;
        }

        // --
        $scope.Breadcrumbs = [];
        $scope.BreadcrumbsUrl = '/html/util/breadcrumbs.html'
        $scope.$parent.setSecond('tracker');
        getData();
    }]
);
