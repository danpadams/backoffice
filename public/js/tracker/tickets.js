// $location is for Future Development
infoChi.controller('trackerTicketsCtrl', ['$scope', '$http', '$localStorage',
    '$location', 'infoChiSpinner', 'infoChiFlash', '$routeParams',
    function ($scope, $http, $localStorage, $location, infoChiSpinner, infoChiFlash, $routeParams) {
        // infoChiFlash.show('This is a test.', 'success');
        getData = function () {
            infoChiSpinner.show();
            let URL = '/web/tracker/tickets';
            if (tracker_project_id) URL += '/' + tracker_project_id
            $http.get(URL).then(function (response) {
                $scope.Data = response.data;
                console.log($scope.Data);
                infoChiSpinner.hide();
            });
        };
        $scope.getData = function () {
            getData();
        }
        $scope.filterItem = function (item) {
            // console.log(item);
            // console.log('filterItem');
            return true;
        }
        $scope.getLinkCreate  = function () {
            retval = '#!/tracker/create';

            if ($routeParams.tracker_project_id) {
                retval += '/' + tracker_project_id;
            }
            return retval;
        }

        // --
        var tracker_project_id = $routeParams.tracker_project_id;
        $scope.Breadcrumbs = [];
        $scope.BreadcrumbsUrl = '/html/util/breadcrumbs.html'
        getData();
    }]
);
