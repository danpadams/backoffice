// $location is for Future Development
infoChi.controller('trackerTicketCtrl', ['$scope', '$http', '$localStorage',
    '$location', 'infoChiSpinner', 'infoChiFlash', '$routeParams',
    function ($scope, $http, $localStorage, $location, infoChiSpinner, infoChiFlash, $routeParams) {
        // infoChiFlash.show('This is a test.', 'success');
        getData = function () {
            if ($routeParams.code) {
                // Existing
                infoChiSpinner.show();
                $http.get('/web/tracker/ticket/' + $routeParams.code).then(function (response) {
                    $scope.Data = response.data;
                    console.log($scope.Data);
                    $scope.optStatus = response.data.optStatus;
                    $scope.selectStatus = {
                        'id': $scope.Data.status_id,
                        'value': $scope.Data.status_value
                    }
                    getComments(response.data.id);
                    infoChiSpinner.hide();
                });

            } else {
                // Create
                console.log('Create');
                $scope.Data = {};
                getProjects()
                console.log('After');
                $scope.Data.tracker_status_id = 1;
                $scope.updStatus({'id': 1, 'value': 'Open'});
            }
        };
        // Used for Create Ticket
        getProjects = function () {
            console.log('getProjects()');
            infoChiSpinner.show();
            $scope.selectProject = {'name': 'Choose Project'};
            $http.get('/web/tracker/projects').then(function (response) {
                $scope.optProjects = response.data;
                infoChiSpinner.hide();
                if ($routeParams.tracker_project_id) {
                    $scope.setProject($routeParams.tracker_project_id);
                    for (setProject = 0; setProject < $scope.optProjects.length; setProject++) {
                        if ($scope.optProjects[setProject].id == $routeParams.tracker_project_id) {
                            $scope.selectProject.name=$scope.optProjects[setProject].name;
                            $scope.setProject({'prefix': $scope.optProjects[setProject].prefix});
                        }
                    }
                }
            });
        }
        $scope.setProject = function (item) {
            $scope.Data.Prefix = item.prefix
            getNextTicket(item.prefix);
        }
        getNextTicket = function (prefix) {
            infoChiSpinner.show();
            $http.get('/web/tracker/' + prefix + '/get_next_ticket').then(function (response) {
                $scope.Data.nextProject = prefix + '-' + response.data;
                infoChiSpinner.hide();
            });
        }
        // Used for Create Ticket - Above

        $scope.updStatus = function (item) {
            $scope.Data.tracker_status_id = item.id;
            $scope.selectStatus = {
                'id': item.status_id,
                'value': item.status_value
            }
        }
        $scope.saveData = function () {
            infoChiSpinner.show();
            console.log('saveData()');
            $http.post('/web/tracker/ticket/save_data', $scope.Data).then(function (response) {
                $scope.Data = response.data;
                getComments(response.data.id);
                infoChiSpinner.hide();
            });
        }
        $scope.saveComment = function () {
            console.log('saveData()');
            console.log($scope.Data.newComment);
            // infoChiSpinner.show();
            Data = {
                'tracker_ticket_id': $scope.Data.id,
                'comment': $scope.Data.newComment
            }
            console.log(Data);
            $http.post('/web/tracker/save_comment', Data).then(function (response) {
               getComments($scope.Data.id)
                // infoChiSpinner.hide();
            });
        }
        getComments = function (id) {
            infoChiSpinner.show();
            $http.get('/web/tracker/comment/' + id).then(function (response) {
                $scope.Data.comments = response.data;
                infoChiSpinner.hide();
            });
        }
        $scope.getLinkList  = function () {
            retval = '#!/tracker/tickets';
            if ($routeParams.tracker_project_id) {
                retval += '/' + $routeParams.tracker_project_id;
            } else if ($scope.Data && $scope.Data.project_id) {
                retval += '/' + $scope.Data.project_id;
            }
            return retval;
        }

        $scope.$parent.setSecond('tracker');
        getData();
    }
]);
