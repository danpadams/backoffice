infoChi.controller("investViewCtrl", function ($scope, $http, $routeParams) {
    getData = function () {
        $http.get('/web/equity/' + $routeParams.ticker).then(function (result) {
            $scope.Data = result.data;
           console.log($scope.Data);
            chk_sma();
            //            console.log($scope.data);
        }, function (response) {
            // Unused if Error
        });
    };
    getNotes = function () {
        $scope.Notes = [];
        $http.get('/web/equity/' + $routeParams.ticker + '/notes').then(function (result) {
            $scope.Notes = result.data;
                        console.log($scope.Notes);
        }, function (response) {
            // Unused if Error
        });
    };
    $scope.saveData = function () {
        alert('Save Data');
        data = $scope.Data;
//            infoChiSpinner.show();
        $http.post('/web/equity/save', data).then(function (response) {
            $scope.Data = response.data;
            alert('Return from Save');
//                infoChiSpinner.hide();
        });
    };
    chk_sma = function () {
        if ($scope.Data.ma_50 > $scope.Data.ma_200) {
            $scope.Data.up_50_200 = 'Up';
            $scope.Data.Up_50_200 = 'text-green';
        } else {
            $scope.Data.up_50_200 = 'Down';
            $scope.Data.Up_50_200 = 'text-red';
        }

        if ($scope.Data.diff < 0) {
            $scope.Data.Diff = 'text-red';
        } else {
            $scope.Data.Diff = 'text-green';
        }

        if ($scope.Data.dma_50 < 0) {
            $scope.Data.Dma_50 = 'text-red';
        } else {
            $scope.Data.Dma_50 = 'text-green';
        }

        if ($scope.Data.dma_200 < 0) {
            $scope.Data.Dma_200 = 'text-red';
        } else {
            $scope.Data.Dma_200 = 'text-green';
        }

    };
    startFresh = function () {
        alert('Start Fresh');
        $scope.Data = {
            id: 0,
            ticker: '',
            name: '',
        }
    };
    // --
    if ($routeParams.ticker == 0) {
        // Start Fresh
        startFresh();
    } else {
        getData();
        getNotes();
    }
});