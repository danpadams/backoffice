infoChi.controller("investMktIndexDataCtrl", function ($scope, $http, $timeout, $routeParams) {
    $scope.getData = function () {
        $http.get('/web/equity/mkt-index/' + $routeParams.index_num).then(function (response) {
            $scope.$broadcast('getData', { data: response.data});
            // $scope.Data = result.data;
            // $scope.prepData();
            console.log('Data Loaded');
        }, function (response) {
            // Unused if Error
        });
    };
    $scope.setOrderBy = function (predicate) {
        $scope.predicate = predicate;
    };
    $scope.prepData = function () {
        size = $scope.Data.length;
        for (i = 0; i < size; i++) {
            if ($scope.Data[i].ma_50 > $scope.Data[i].ma_200) {
                $scope.Data[i].up_50_200 = 'Up';
                $scope.Data[i].Up_50_200 = 'text-green';
            } else {
                $scope.Data[i].up_50_200 = 'Down';
                $scope.Data[i].Up_50_200 = 'text-red';
            }

            if ($scope.Data[i].diff < 0) {
                $scope.Data[i].Diff = 'text-red';
            } else {
                $scope.Data[i].Diff = 'text-green';
            }

            if ($scope.Data[i].dma_50 < 0) {
                $scope.Data[i].Dma_50 = 'text-red';
            } else {
                $scope.Data[i].Dma_50 = 'text-green';
            }

            if ($scope.Data[i].dma_200 < 0) {
                $scope.Data[i].Dma_200 = 'text-red';
            } else {
                $scope.Data[i].Dma_200 = 'text-green';
            }
        }
    };


    $scope.getData();
    $scope.pageSize = 10;
    $scope.predicate = 'ticker';
    $scope.reverse = false;
});