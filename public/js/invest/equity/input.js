infoChi.controller("investEquityInputCtrl", ['$scope', '$http', '$routeParams', 'infoChiSpinnerService',
    function ($scope, $http, $routeParams, infoChiSpinnerService) {
        /* 
         * To change this license header, choose License Headers in Project Properties.
         * To change this template file, choose Tools | Templates
         * and open the template in the editor.
         */
        $scope.saveTrade = function () {
            data = {
                account: $scope.Data.account,
                actdate: $scope.Data.actdate,
                commission: $scope.Data.commission,
                price: $scope.Data.price,
                qty: $scope.Data.qty,
                action: $scope.Data.reinvest,
                ticker: $scope.Data.ticker
            };
            console.log($scope.Data.trade_id == undefined);
            if ($scope.Data.trade_id != undefined) {
                data['id'] = $scope.Data.id;
            }
//            if ($scope.Data.delete == "1") {
//                data['delete'] = 1;
//            }
            $http.post('/makeJson.php?choice=f_152&area=invest&controller=equity&action=updateTrade', JSON.stringify(data)).then(function (result) {
                // infoChiSpinnerService.hide('mainSpinner');
                // If delete, take to register page
                if ($scope.Data.delete == "1") {
                    location.href = "?#/banking/account/" + $scope.Data.account;
                }
                $scope.Data.status = 'Transaction ID: ' + result.data.id;
                if (data.id == undefined) {
//                    $routeParams.trade_id = result.data.id;
//                    $scope.Data.trade_id = result.data.id;
//                    $scope.setupTrade();
                    location.href = "?#/invest/equity/input/" + $scope.Data.trade_id;
                }
            });
        };
        $scope.setupTrade = function () {
            if ($scope.Data.trade_id == undefined) {
                $scope.Data.tr_id = "To Be Defined.";
            } else {
                $scope.Data.tr_id = $routeParams.trade_id;
                infoChiSpinnerService.show();
                $http.get('/makeJson.php?area=invest&controller=equity&action=getTrade&trade_id=' + $scope.Data.tr_id).then(function (result) {
                    console.log('Loading');
                    $scope.Data = result.data;
                    $scope.Data.tr_id = $routeParams.trade_id;
                    $scope.Data.reinvest = $scope.Data.action
                    infoChiSpinnerService.hide();
                }, function (response) {
//              infoChiSpinnerService.hide();
                });
            }
        };
 
        $scope.Data = {
            trade_id: 0
        }
        
        //if ($routeParams.trade_id != undefined) {
            $scope.Data.trade_id = $routeParams.trade_id;
        //}
        $scope.setupTrade();
    }]);