// $location is for Future Development
infoChi.controller('journalEditCtrl', ['$scope', '$http', '$localStorage',
    '$location', 'infoChiSpinner', 'infoChiFlash', '$routeParams',
    function ($scope, $http, $localStorage, $location, infoChiSpinner, infoChiFlash, $routeParams) {
        // infoChiFlash.show('This is a test.', 'success');
        $scope.saveData = function () {
            infoChiSpinner.show();
            $http.post('/web/journal/' + journal_entry_id, $scope.Data).then(function (response) {
                $scope.Data = response.data;
                infoChiSpinner.hide();
                if (journal_entry_id == 0) {

                }
                infoChiSpinner.hide();
            });
        };

        getData = function () {
            infoChiSpinner.show();
            $http.get('/web/journal/' + journal_entry_id).then(function (response) {
                console.log(journal_entry_id);
                if (journal_entry_id == 0) {
                    response.data.journal_id = $routeParams.journal_id;
                }
                $scope.Data = response.data;
                console.log($scope.Data);
                getBreadcrumbs();
                infoChiSpinner.hide();
            });
        };
        getBreadcrumbs = function () {
            $scope.Breadcrumbs = [
                {
                    'name': 'Journal Subjects',
                    'link': '#!/journals'
                },
                {
                    'name': 'Journal Index',
                    'link': '#!/journals/' + $scope.Data.journal_id
                },
                {
                    'name': 'Current Page',
                    'link': ''
                },
            ];
        }

        // --
        var journal_entry_id = $routeParams.journal_entry_id;
        $scope.Breadcrumbs = [];
        $scope.BreadcrumbsUrl = '/html/util/breadcrumbs.html'
        getData();
    }]
);
