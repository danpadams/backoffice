// $location is for Future Development
infoChi.controller('journalEntryCtrl', ['$scope', '$http', '$localStorage',
    '$location', 'infoChiSpinner', 'infoChiFlash', '$routeParams',
    function ($scope, $http, $localStorage, $location, infoChiSpinner, infoChiFlash, $routeParams) {
        // infoChiFlash.show('This is a test.', 'success');
        getData = function () {
            infoChiSpinner.show();
            $http.get('/web/journals/' + $scope.journal_id).then(function (response) {
                $scope.$broadcast('getData', {data: response.data});
                infoChiSpinner.hide();
            });
        };

        // --
        $scope.journal_id = $routeParams.journal_id;
        $scope.Breadcrumbs = [
            {
                'name': 'Journal Subjects',
                'link': '#!/journals'
            },
            {
                'name': 'Current Page',
                'link': ''
            }
        ];
        getData();
    }]
);
