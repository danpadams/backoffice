// $location is for Future Development
infoChi.controller('journalIndexCtrl', ['$scope', '$http', '$localStorage',
    '$location', 'infoChiSpinner', 'infoChiFlash',
    function ($scope, $http, $localStorage, $location, infoChiSpinner, infoChiFlash) {
        // infoChiFlash.show('This is a test.', 'success');
        getData = function () {
            infoChiSpinner.show();
            $http.get('/web/journals').then(function (response) {
                $scope.$broadcast('getData', {
                    data: response.data,
                    predicate:'actdate',
                    reverse: true}
                );

                infoChiSpinner.hide();
            });
        };

        // --
        getData();
    }]
);

