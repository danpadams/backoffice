// $location is for Future Development
infoChi.controller('bankingReportsBusinessesCtrl', ['$scope', '$http', '$localStorage',
    '$location', 'infoChiSpinner', 'infoChiFlash', '$routeParams',
    function ($scope, $http, $localStorage, $location, infoChiSpinner, infoChiFlash, $routeParams) {
        // infoChiFlash.show('This is a test.', 'success');
        $scope.getData = function () {
            infoChiSpinner.show();
            $http.get('/web/banking/reports/business/').then(function (response) {
                $scope.Data = response.data;
                infoChiSpinner.hide();
            });
        }

        $scope.getData();
        $scope.$parent.setSecond('banking');
    }]
);
