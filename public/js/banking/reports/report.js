// $location is for Future Development
infoChi.controller('bankingReportEditCtrl', ['$scope', '$http', '$localStorage',
    '$location', 'infoChiSpinner', 'infoChiFlash', '$routeParams',
    function ($scope, $http, $localStorage, $location, infoChiSpinner, infoChiFlash, $routeParams) {
        getData = function () {
            infoChiFlash.show('Retrieved Data', 'success', 1000);
            infoChiSpinner.show();
            $http.get('/web/banking/report/' + $routeParams.report_id).then(function (response) {
                $scope.Data = response.data;
                console.log('getData');
                console.log($scope.Data);
                getDataCategories();
                getDataClasses();
                getDataAccounts();
                infoChiSpinner.hide();
            });
        }
        $scope.addCategory = function (item) {
            console.log(item);
            Data = {
                'report': $routeParams.report_id,
                'category': item.id
            };
            infoChiSpinner.show();
            console.log(Data);
            $http.post('/web/banking/report/add_category', Data).then(function (response) {
                // $scope.Data = response.data;
                // console.log(response.Data);
                getData();
                infoChiSpinner.hide();
            });
        }
        $scope.delCategory = function (item) {
            console.log(item);
            Data = {
                'id': item.id
            };
            infoChiSpinner.show();
            console.log(Data);
            $http.post('/web/banking/report/del_category', Data).then(function (response) {
                // $scope.Data = response.data;
                // console.log(response.Data);
                getData();
                infoChiSpinner.hide();
            });
        }
        $scope.addClass = function (item) {
            console.log(item);
            Data = {
                'report': $routeParams.report_id,
                'class': item.id
            };
            infoChiSpinner.show();
            console.log(Data);
            $http.post('/web/banking/report/add_class', Data).then(function (response) {
                // $scope.Data = response.data;
                // console.log(response.Data);
                getData();
                infoChiSpinner.hide();
            });
        }
        $scope.delClass = function (item) {
            console.log(item);
            Data = {
                'id': item.id
            };
            infoChiSpinner.show();
            console.log(Data);
            $http.post('/web/banking/report/del_class', Data).then(function (response) {
                // $scope.Data = response.data;
                // console.log(response.Data);
                getData();
                infoChiSpinner.hide();
            });
        }
        $scope.addAccount = function (item) {
            console.log(item);
            Data = {
                'report': $routeParams.report_id,
                'account': item.id
            };
            infoChiSpinner.show();
            console.log(Data);
            $http.post('/web/banking/report/add_account', Data).then(function (response) {
                // $scope.Data = response.data;
                // console.log(response.Data);
                getData();
                infoChiSpinner.hide();
            });
        }
        $scope.delAccount = function (item) {
            console.log(item);
            Data = {
                'id': item.id
            };
            infoChiSpinner.show();
            console.log(Data);
            $http.post('/web/banking/report/del_account', Data).then(function (response) {
                // $scope.Data = response.data;
                // console.log(response.Data);
                getData();
                infoChiSpinner.hide();
            });
        }
        getDataCategories = function () {
            console.log('getDataCategories()');
            $scope.Categories_Data = $scope.Data.categories;
        }
        getDataAccounts = function () {
            console.log('getDataAccounts()');
            $scope.Accounts_Data = $scope.Data.accounts;
        }
        getDataClasses = function () {
            console.log('getDataClass()');
            $scope.Classes_Data = $scope.Data.classes;
        }
        $scope.getData = function () {
            getData();
        }
        $scope.saveData = function () {
            console.log('Save Data');
            console.log($scope.Data);
            infoChiSpinner.show();
            $http.post('/web/banking/report', $scope.Data).then(function (response) {
                infoChiSpinner.hide();
                $scope.Data = response.data;
                console.log(response.Data);

                infoChiSpinner.hide();
                infoChiFlash.show('Retrieved Data', 'success', 1000);
            });
        }
        // Get list from Server
        $scope.getCategories = function () {
            infoChiSpinner.show();
            $http.get('/web/banking/categories').then(function (result) {
                $scope.Categories = result.data;
                infoChiSpinner.hide();
            }, function (response) {
                infoChiSpinner.hide();
            });
        };
        // Get List from Server
        $scope.getClasses = function () {
            infoChiSpinner.show();
            $http.get('/web/banking/classes').then(function (result) {
                $scope.Classes = result.data;
                infoChiSpinner.hide();
            }, function (response) {
                infoChiSpinner.hide();
            });
        };
        // Get list from Server
        $scope.getAccounts = function () {
            $scope.Accounts = [];
            infoChiSpinner.show();
            $http.get('/web/banking/accounts').then(function (result) {
                $scope.Accounts = result.data;
                infoChiSpinner.hide();
            }, function (response) {
                infoChiSpinner.hide();
            });
        };
        // --
        console.log('Report.Edit');
        $scope.getData();
        $scope.getCategories();
        $scope.getClasses();
        $scope.getAccounts();
        $scope.$parent.setSecond('banking');
    }]
);
