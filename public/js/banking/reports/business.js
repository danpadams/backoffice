// $location is for Future Development
infoChi.controller('bankingReportsBusinessCtrl', ['$scope', '$http', '$localStorage',
    '$location', 'infoChiSpinner', 'infoChiFlash', '$routeParams',
    function ($scope, $http, $localStorage, $location, infoChiSpinner, infoChiFlash, $routeParams) {
        // infoChiFlash.show('This is a test.', 'success');
        getIncome = function () {
            infoChiSpinner.show();
            $http.get('/web/banking/reports/monthly/' + $scope.income_id).then(function (response) {
                setData(response.data, 'income');
                infoChiSpinner.hide();
            });
        };
        getExpenses = function () {
            infoChiSpinner.show();
            $http.get('/web/banking/reports/monthly/' + $scope.expense_id).then(function (response) {
                setData(response.data, 'expense');
                infoChiSpinner.hide();
            });
        };
        $scope.getData = function () {
            getExpenses();
            getIncome();
        }
        setData = function (data, name) {
            if ($scope.Data === undefined) {
                $scope.Info = {};
            }
            for (i = 0; i < data.length; i++) {
                key = data[i].key
                if ($scope.Info[key] === undefined) {
                    $scope.Info[key] = {};
                }
                if ($scope.Info[key].key === undefined) {
                    $scope.Info[key].key = key;
                }
                if ($scope.Info[key].year === undefined) {
                    $scope.Info[key].year = data[i].year;
                }
                if ($scope.Info[key].month === undefined) {
                    $scope.Info[key].month = data[i].month;
                }
               if ($scope.Info[key][name] === undefined) {
                    $scope.Info[key][name] = data[i].subtotal;
                }
            }
            $scope.Data = $scope.Info;
            console.log($scope.Info);
        }
        $scope.net = function (Datum) {
            income = parseFloat(Datum.income);
            if (!income) {
                income = 0;
            }
            expense = parseFloat(Datum.expense);
            return income+expense;
        }
        getBusinessInfo = function () {
            infoChiSpinner.show();
            $http.get('/web/banking/reports/business/' + business_id).then(function (response) {
                $scope.name = response.data.name;
                $scope.income_id = response.data.income;
                $scope.expense_id = response.data.expense;
                $scope.getData();
                infoChiSpinner.hide();
            });
        };

        business_id = $routeParams.business_id;
        console.log(business_id);
        getBusinessInfo();
        $scope.$parent.setSecond('banking');
    }]
);
