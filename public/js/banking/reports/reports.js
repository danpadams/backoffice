// $location is for Future Development
infoChi.controller('bankingReportsCtrl', ['$scope', '$http', '$localStorage',
    '$location', 'infoChiSpinner', 'infoChiFlash', '$routeParams',
    function ($scope, $http, $localStorage, $location, infoChiSpinner, infoChiFlash, $routeParams) {
        getData = function () {
            infoChiFlash.show('Retrieved Data', 'success', 1000);
            // infoChiSpinner.show();
            $http.get('/web/banking/reports').then(function (response) {
                $scope.Data = response.data;
                console.log($scope.Data);
                // infoChiSpinner.hide();
            });

        }
        $scope.getData = function () {
            getData();
        }

        $scope.getData();
        $scope.$parent.setSecond('banking');
    }]
);
