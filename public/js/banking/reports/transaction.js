// $location is for Future Development
infoChi.controller('bankingReportsTransactionCtrl', ['$scope', '$http', '$localStorage',
    '$location', 'infoChiSpinner', 'infoChiFlash', '$routeParams',
    function ($scope, $http, $localStorage, $location, infoChiSpinner, infoChiFlash, $routeParams) {
        // infoChiFlash.show('This is a test.', 'success');
        getData = function () {
            console.log('getData()');
            if (!year) {
                infoChiSpinner.show();
                $http.get('/web/banking/reports/transaction/' + report_id).then(function (response) {
                    $scope.Data = response.data;
                    genSummary();
                    infoChiSpinner.hide();
                });
            } else {
                // infoChiSpinner.show();
                $http.get('/web/banking/reports/transaction/' + report_id + '/' + year + '/' + month).then(function (response) {
                    $scope.Data = response.data;
                    genSummary();
                // infoChiSpinner.hide();
                });
            }
        }

        genSummary = function () {
            $scope.Summary = {
                'name' : $scope.Data.report.name,
                'total': 0,
                'startDate': $scope.Data.report.startdate,
                'endDate': $scope.Data.report.enddate,
            };
            $scope.Summary.total = 0;
            for (var i = 0; i < $scope.Data.response.length; i++) {
                console.log(parseFloat($scope.Data.response[i].amount));
                $scope.Summary.total = $scope.Summary.total + parseFloat($scope.Data.response[i].amount);
            }
        }

        $scope.getData = function () {
            getData();
        }

        report_id = $routeParams.report_id;
        $scope.report_id = report_id;
        year = $routeParams.year;
        month = $routeParams.month;
        $scope.getData();
        $scope.$parent.setSecond('banking');
    }]
);
