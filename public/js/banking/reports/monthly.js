// $location is for Future Development
infoChi.controller('bankingReportsMonthlyCtrl', ['$scope', '$http', '$localStorage',
    '$location', 'infoChiSpinner', 'infoChiFlash', '$routeParams',
    function ($scope, $http, $localStorage, $location, infoChiSpinner, infoChiFlash, $routeParams) {
        // infoChiFlash.show('This is a test.', 'success');
        getData = function () {
            console.log('getData()');
            // infoChiSpinner.show();
            $http.get('/web/banking/reports/monthly/' + $routeParams.report_id).then(function (response) {
                $scope.Data = response.data;
                console.log(response.data)
                // infoChiSpinner.hide();
            });
        };
        $scope.getData = function () {
            getData();
        }

        getData();
        $scope.$parent.setSecond('banking');
    }]
);
