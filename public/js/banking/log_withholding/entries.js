// $location is for Future Development
infoChi.controller('logWithholdingEntriesCtrl', ['$scope', '$http', '$localStorage',
    '$location', 'infoChiSpinner', 'infoChiFlash', '$routeParams',
    function ($scope, $http, $localStorage, $location, infoChiSpinner, infoChiFlash, $routeParams) {
        // infoChiFlash.show('This is a test.', 'success');
        getData = function () {
            infoChiSpinner.show();
            let URL = '/web/log_withholding/entries';
//             if (tracker_project_id) URL += '/' + tracker_project_id
            $http.get(URL).then(function (response) {
                $scope.Data = response.data;
                infoChiSpinner.hide();
            });
        };
        $scope.getData = function () {
            getData();
        }
        $scope.filterItem = function (item) {
            return true;
        }

        // --
        var tracker_project_id = $routeParams.tracker_project_id;

        $scope.Breadcrumbs = $scope.$parent.getBreadcrumbs('log_withholding');
        $scope.BreadcrumbsUrl = '/html/util/breadcrumbs.html'

        $scope.$parent.setSecond('banking');
        getData();
    }]
);
