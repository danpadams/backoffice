// $location is for Future Development
infoChi.controller('logWithholdingTransferCtrl', ['$scope', '$http', '$localStorage',
    '$location', 'infoChiSpinner', 'infoChiFlash', '$routeParams',
    function ($scope, $http, $localStorage, $location, infoChiSpinner, infoChiFlash, $routeParams) {
        // infoChiFlash.show('This is a test.', 'success');
        getClasses = function () {
            infoChiSpinner.show();
            $scope.selectStatus = {'name': 'Choose Project'};
            $http.get('/web/log_withholding/banking_classes').then(function (response) {
                $scope.optClasses = response.data;
                infoChiSpinner.hide();
            });
        }

        $scope.updClass = function (location, selectClass, item) {
            console.log('updClass()');
            console.log(location);
            console.log(item);
            $scope.Data[location] = item.id;
            $scope[selectClass].value = item.descrip;
        }
        $scope.saveData = function () {
            console.log('saveData()');
            console.log($scope.Data);
            // infoChiSpinner.show();
            // $http.post('/web/log_withholding/entry/save_data', $scope.Data).then(function (response) {
            //     parseData(response.data);
            //     infoChiSpinner.hide();
            // });
        }

        parseData = function (data) {
            $scope.Data = data;
            $scope.optClasses = data.optClasses;
            if ($routeParams.action_type == 'clone') {
                $scope.Data.id = 0;
                $routeParams = '';
            }
        }
        setClass = function () {
            console.log('setClass()');
            $scope.selectClass = {
                'id': $scope.Data.banking_class_id,
                'value': $scope.Data.banking_class_value
            }
        }

        $scope.BreadcrumbsUrl = '/html/util/breadcrumbs.html';
        $scope.Breadcrumbs = $scope.$parent.getBreadcrumbs('log_withholding');
        $scope.$parent.setSecond('banking');

        $scope.selectClassFrom = {id: 8, value: 'General'};
        $scope.selectClassTo = {id: 8, value: 'General'};
        $scope.Data = {fromId: 8, toId: 8};

        getClasses();
    }
]);
