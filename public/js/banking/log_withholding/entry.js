// $location is for Future Development
infoChi.controller('logWithholdingEntryCtrl', ['$scope', '$http', '$localStorage',
    '$location', 'infoChiSpinner', 'infoChiFlash', '$routeParams',
    function ($scope, $http, $localStorage, $location, infoChiSpinner, infoChiFlash, $routeParams) {
        // infoChiFlash.show('This is a test.', 'success');
        getData = function () {
            if ($routeParams.mile_id != 0) {
                // Existing
                infoChiSpinner.show();
                $http.get('/web/log_withholding/entry/' + $routeParams.mile_id).then(function (response) {
                    parseData(response.data);
                    setClass();
                    infoChiSpinner.hide();
                });
            } else {
                // Create
                $scope.Data = {};
                getProjects()
                $scope.updClass({'id': 0, 'value': 'Choose Type'});
            }
        };
        // Used for Create Ticket
        getProjects = function () {
            infoChiSpinner.show();
            $scope.selectStatus = {'name': 'Choose Project'};
            $http.get('/web/log_withholding/banking_classes').then(function (response) {
                $scope.optClasses = response.data;
                infoChiSpinner.hide();
            });
        }
        // Used for Create Ticket - Above

        $scope.updClass = function (item) {
            console.log('updClass()');
            console.log($scope.selectClass);
            $scope.Data.banking_class_id = item.id;
            $scope.Data.banking_class_value = item.descrip;
            setClass();
            console.log($scope.selectClass);
            // console.log($scope.Data);
        }
        $scope.saveData = function () {
            infoChiSpinner.show();
            $http.post('/web/log_withholding/entry/save_data', $scope.Data).then(function (response) {
                parseData(response.data);
                infoChiSpinner.hide();
            });
        }

        parseData = function (data) {
            $scope.Data = data;
            $scope.optClasses = data.optClasses;
            if ($routeParams.action_type == 'clone') {
                $scope.Data.id = 0;
                $routeParams = '';
            }
        }
        setClass = function () {
            console.log('setClass()');
            $scope.selectClass = {
                'id': $scope.Data.banking_class_id,
                'value': $scope.Data.banking_class_value
            }
        }

        $scope.Breadcrumbs = $scope.$parent.getBreadcrumbs('log_withholding');
        $scope.BreadcrumbsUrl = '/html/util/breadcrumbs.html'

        getData();
    }
]);
