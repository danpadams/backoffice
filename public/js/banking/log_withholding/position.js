// $location is for Future Development
infoChi.controller('logWithholdingPositionCtrl', ['$scope', '$http', '$localStorage',
    '$location', 'infoChiSpinner', 'infoChiFlash', '$routeParams',
    function ($scope, $http, $localStorage, $location, infoChiSpinner, infoChiFlash, $routeParams) {
        // infoChiFlash.show('This is a test.', 'success');
        getData = function () {
            infoChiSpinner.show();
            let URL = '/web/log_withholding/position/' + position_id;
            console.log(URL);
            $http.get(URL).then(function (response) {
                console.log(response.data);
                $scope.Data = response.data;
                infoChiSpinner.hide();
            });
        };
        $scope.getData = function () {
            getData();
        }

        // --
        var position_id = $routeParams.position_id;
        $scope.Breadcrumbs = $scope.$parent.getBreadcrumbs('log_withholding');
        // $scope.BreadcrumbsID = 'POS';
        console.log($scope.Breadcrumbs);
        $scope.BreadcrumbsUrl = '/html/util/breadcrumbs.html'
        $scope.$parent.setSecond('banking');
        getData();


    }]
);
