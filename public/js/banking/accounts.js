// $location is for Future Development
infoChi.controller('bankingAccountsCtrl', ['$scope', '$http', '$localStorage',
        '$location', 'infoChiSpinner', 'infoChiFlash', '$routeParams',
        function ($scope, $http, $localStorage, $location, infoChiSpinner, infoChiFlash, $routeParams) {
            // infoChiFlash.show('This is a test.', 'success');
            getData = function () {
                console.log('getData()');
                infoChiSpinner.show();
                $http.get('/web/banking/accounts').then(function (response) {
                    $scope.Data = response.data;
                    console.log($scope.Data);
                    getBalance(response.data);
                    infoChiSpinner.hide();
                });
            };
            $scope.getData = function () {
                getData();
            }
            $scope.filterItem = function (item) {
                // console.log(item);
                // console.log('filterItem');
                return true;
            }
            getBalance = function (data) {
                for (i = 0; i < data.length; i++) {
                    infoChiSpinner.show();
                    $http.get('/web/banking/accounts/get_balance/' + data[i].id).then(function (response) {
                        // console.log(response.data);
                        setBalance(response.data);
                        infoChiSpinner.hide();
                    });
                }
            }
            setBalance = function (data) {
                // console.log($scope.Data);
                for (i = 0; i < $scope.Data.length; i++) {
                    if ($scope.Data[i].id == data.account_id) {
                        $scope.Data[i].futureBalance = data.futureBalance;
                        $scope.Data[i].currentBalance = data.currentBalance;
                        $scope.Data[i].maxDate = data.maxDate;

                    }
                }
            }

            // --
            getData();
            $scope.$parent.setSecond('banking');
        }
    ]
)
;
