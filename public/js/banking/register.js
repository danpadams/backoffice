// $location is for Future Development
infoChi.controller('bankingRegisterCtrl', ['$scope', '$http', '$localStorage',
    '$location', 'infoChiSpinner', 'infoChiFlash', '$routeParams',
    function ($scope, $http, $localStorage, $location, infoChiSpinner, infoChiFlash, $routeParams) {
        // infoChiFlash.show('This is a test.', 'success');
        getData = function () {
            console.log('getData()');
            infoChiSpinner.show();
            $http.get('/web/banking/register/' + $routeParams.account_id).then(function (response) {
                $scope.Data = calculateData(response.data);
                infoChiSpinner.hide();
            });
        };
        $scope.getData = function () {
            getData();
        }
        $scope.filterItem = function (item) {
            // console.log(item);
            // console.log('filterItem');
            return true;
        }

        calculateData = function (data) {
            valCleared = 0;
            valBalance = 0;
            angular.forEach(data, function (value, key) {
                if (value.status == 1) {
                    valCleared += parseFloat(value.amount);
                }
                valBalance = value.balance;

                // Status
                switch (value.status) {
                    case -1:
                        data[key].labelStatus = 'R';
                        data[key].labelColor = "#000000";
                        break;
                    case 0:
                        data[key].labelStatus = 'U';
                        data[key].labelColor = "#AAAAAA";
                        break;
                    case 1:
                        data[key].labelStatus = 'C';
                        data[key].labelColor = "#0000FF";
                        break;
                }

                data[key].rowClass = '';
                if (data[key].Future == 1) {
                    data[key].rowClass = 'future';
                }
            });
            $scope.valBalance = valBalance;
            $scope.valCleared = valCleared;
            return data;
        }
        $scope.updStatus = function (item) {
            console.log('updStatus()');
            infoChiSpinner.show();
            $http.post('/web/banking/register/' + item.id + '/upd_item_status').then(function (response) {
                $scope.Data = calculateData(response.data);
                infoChiSpinner.hide();
            });
        }
        $scope.updateTranactions = function () {
            console.log('updateTranactions()');
            if (confirm('Are You Sure?')) {
                infoChiSpinner.show();
                $http.post('/web/banking/register/' + $routeParams.account_id + '/upd_register').then(function (response) {
                    $scope.Data = calculateData(response.data);
                    infoChiSpinner.hide();
                });
            }
        }
        $scope.getAccount = function () {
            console.log('getAccount()');
            // infoChiSpinner.show();
            $http.get('/web/banking/register/' + $routeParams.account_id + '/account').then(function (response) {
                $scope.Account = response.data;
                console.log(response.data);
                // infoChiSpinner.hide();
            });
        }

        // --
        $scope.getAccount();
        getData();
        $scope.$parent.setSecond('banking');
    }]
);
