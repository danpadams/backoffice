// $location is for Future Development
infoChi.controller('bankingPendingTransferEntryCtrl', ['$scope', '$http', '$localStorage',
    '$location', 'infoChiSpinner', 'infoChiFlash', '$routeParams',
    function ($scope, $http, $localStorage, $location, infoChiSpinner, infoChiFlash, $routeParams) {
        // infoChiFlash.show('This is a test.', 'success');
        getData = function () {
            if ($routeParams.pending_transfer_id !== '0') {
                // Existing
                // infoChiSpinner.show();
                console.log('Existing');
                $http.get('/web/banking/pending_transfer/' + $routeParams.pending_transfer_id).then(function (response) {
                    $scope.Data = response.data;
                    setClass();
                    setAccountFrom();
                    setAccountTo();
                    // infoChiSpinner.hide();
                });
            } else {
                // Create
                console.log('Create');
                $scope.Data = {};
                $scope.updClass({'id': 0, 'value': 'Choose Type'});
            }
            getClasses();
            getAccounts();
        };
        // Used for Create Ticket
        getClasses = function () {
            infoChiSpinner.show();
            $scope.selectStatus = {'name': 'Choose Class'};
            $http.get('/web/banking/classes').then(function (response) {
                $scope.optClasses = response.data;
                infoChiSpinner.hide();
            });
        }
        getAccounts = function () {
            infoChiSpinner.show();
            $scope.selectStatus = {'name': 'Choose Account'};
            $http.get('/web/banking/accounts').then(function (response) {
                $scope.optAccounts = response.data;
                infoChiSpinner.hide();
            });
        }
        // Used for Create Ticket - Above

        $scope.updClass = function (item) {
            console.log('updClass()');
            console.log(item);
            $scope.Data.banking_class_id = item.id;
            $scope.Data.banking_class_value = item.descrip;
            setClass();
            console.log($scope.selectClass);
            // console.log($scope.Data);
        }
        $scope.updAccountFrom = function (item) {
            console.log('updAccountFrom()');console.log(item);
            $scope.Data.banking_account_from_id = item.id;
            $scope.Data.banking_account_from_value = item.name;
            setAccountFrom();
            console.log($scope.selectAccountFrom);
            // console.log($scope.Data);
        }
        $scope.updAccountTo = function (item) {
            console.log('updAccountTo()');console.log(item);
            $scope.Data.banking_account_to_id = item.id;
            $scope.Data.banking_account_to_value = item.name;
            setAccountTo();
            console.log($scope.selectAccountTo);
            // console.log($scope.Data);
        }
        $scope.saveData = function () {
            infoChiSpinner.show();
            $http.post('/web/banking/pending_transfer/save_data', $scope.Data).then(function (response) {
                infoChiSpinner.hide();
            });
        }


        setClass = function () {
            console.log('setClass()');
            $scope.selectClass = {
                'id': $scope.Data.banking_class_id,
                'value': $scope.Data.banking_class_value
            }
        }
        setAccountFrom = function () {
            console.log('setAccountFrom()');
            $scope.selectAccountFrom = {
                'id': $scope.Data.banking_account_from_id,
                'value': $scope.Data.banking_account_from_value
            }
        }
        setAccountTo = function () {
            console.log('setAccountTo()');
            $scope.selectAccountTo = {
                'id': $scope.Data.banking_account_to_id,
                'value': $scope.Data.banking_account_to_value
            }
        }


        // $scope.Data = {'code': $routeParams.code};
        // $scope.Data.code = $routeParams.code;
        console.log('::bankingPendingTransferEntryCtrl')
        getData();
        $scope.$parent.setSecond('banking');
    }
]);
