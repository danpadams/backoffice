// $location is for Future Development
infoChi.controller('bankingPendingTransferIndexCtrl', ['$scope', '$http', '$localStorage',
    '$location', 'infoChiSpinner', 'infoChiFlash', '$routeParams',
    function ($scope, $http, $localStorage, $location, infoChiSpinner, infoChiFlash, $routeParams) {
        // infoChiFlash.show('This is a test.', 'success');
        getData = function () {
            // Data
            infoChiSpinner.show();
            $http.get('/web/banking/pending_transfer').then(function (response) {
                $scope.Data = response.data;
                infoChiSpinner.hide();
            });
            // Summary
            infoChiSpinner.show();
            $http.get('/web/banking/pending_transfer/summary').then(function (response) {
                $scope.Summary = response.data;
                infoChiSpinner.hide();
            });
        };
        $scope.getData = function () {
            getData();
        }
        $scope.runProcess = function () {
            console.log('runProcess()');
            if (confirm('Transfer Amounts First?')) {
                Data = {
                    "log_withholding": $scope.Data,
                    "transfer": $scope.Summary
                };
                for (var i = 0; i < Data.log_withholding.length; i++) {
                    delete Data.log_withholding[i].banking_class;
                    delete Data.log_withholding[i].banking_account_from;
                    delete Data.log_withholding[i].banking_account_to;
                }
                for (var i = 0; i < Data.transfer.length; i++) {
                    delete Data.transfer[i].banking_class;
                    delete Data.transfer[i].banking_account_from;
                    delete Data.transfer[i].banking_account_to;
                }
                console.log(Data);
                infoChiSpinner.show();
                $http.post('/web/banking/pending_transfer/process', Data).then(function (response) {
                    infoChiSpinner.hide();
                    getData();
                });
            }
            console.log('end-runProcess()');
        }
        $scope.filterItem = function (item) {
            return true;
        }

        // --
        console.log('bankingPendingTransferIndexCtrl');
        var tracker_project_id = $routeParams.tracker_project_id;
        $scope.Breadcrumbs = [];
        $scope.BreadcrumbsUrl = '/html/util/breadcrumbs.html'
        getData();
        $scope.$parent.setSecond('banking');
    }]
);
