infoChi.controller("bankingTransactionCtrl", ['$scope', '$http', '$localStorage', '$routeParams', 'infoChiSpinner',
    function ($scope, $http, $localStorage, $routeParams, infoChiSpinner) {
        // Get List from Server
        $scope.getClasses = function () {
            infoChiSpinner.show();
            $http.get('/web/banking/classes').then(function (result) {
                $scope.Classes = result.data;
                infoChiSpinner.hide();
            }, function (response) {
                infoChiSpinner.hide();
            });
        };
        // Get list from Server
        $scope.getAccounts = function () {
            $scope.Accounts = [];
            infoChiSpinner.show();
            $http.get('/web/banking/accounts').then(function (result) {
                $scope.Accounts = result.data;
                infoChiSpinner.hide();
            }, function (response) {
                infoChiSpinner.hide();
            });
        };
        // Get list from Server
        $scope.getCategories = function () {
            infoChiSpinner.show();
            $http.get('/web/banking/categories').then(function (result) {
                $scope.Categories = result.data;
                infoChiSpinner.hide();
            }, function (response) {
                infoChiSpinner.hide();
            });
        };
        $scope.choCategory = function (item, location) {
            location.cat = item.id;
            $scope.checkSplit();
        };
        $scope.selCategory = function ($location, $category_id) {
            angular.forEach($scope.Categories, function (value, key) {
                if (parseInt($category_id) === parseInt(value.id)) {
                    $location.Category = {descrip: value.descrip};
                }
            });
            $scope.checkSplit();
        };
        $scope.choClass = function (item, location) {
            location.class = item.id;
        };
        $scope.selClass = function ($location, $class_id) {
            angular.forEach($scope.Classes, function (value, key) {
                if (parseInt($class_id) === parseInt(value.id)) {
                    $location.Class = {descrip: value.descrip};
//                    $scope.Select.Class = {descrip: value.descrip};
                }
            });
        };
        $scope.choAccount = function (item) {
            $scope.Data.account = item.id;
        };
        $scope.selAccount = function () {
            angular.forEach($scope.Accounts, function (value, key) {
                if (parseInt($scope.Data.account) === parseInt(value.id)) {
                    $scope.Select.Account = {name: value.name};
                }
            });
        };
        $scope.checkSplit = function () {
            if ($scope.Data.cat == 1) {
                if ($scope.Data.Splits == undefined) {
                    $scope.Data.Splits = [];
                }
                if ($scope.Data.Splits.length === 0) {
                    addSplitLine('', 8, 0);
                    addSplitLine('', 8, 0);
                    addSplitLine('', 8, 0);
                }
            }
        };
        // Process Split Data from Server
        $scope.getSplitData = function () {
            $http.get('/web/banking/transaction/' + $scope.Data.tranid + '/split_data').then(function (result) {
                $scope.Data.Splits = result.data;
                // Only 1 element in Splits
                if ($scope.Data.Splits && $scope.Data.Splits.length == undefined) {
                    $scope.Data.Splits = [$scope.Data.Splits];
                }
                // Empty Split Data
                if ($scope.Data.Splits == undefined) {
                    $scope.Data.Splits = [];
                }
                if ($scope.Select.Splits == undefined) {
                    $scope.Select.Splits = [];
                }
                for (var i = 0, len = $scope.Data.Splits.length; i < len; i++) {
                    // $scope.Select.Splits.push({});
                    if ($scope.Data.Splits[i].class == '0') {
                        $scope.Data.Splits[i].class = 8;
                    }
                    $scope.selCategory($scope.Select.Splits[i], $scope.Data.Splits[i].category);
                    $scope.selClass($scope.Select.Splits[i], $scope.Data.Splits[i].class);
                }
                $scope.evalSplit();
                // infoChiSpinner.hide();
            }, function (response) {
//              infoChiSpinner.hide();

            });
        };
        $scope.saveTransaction = function () {
            if ($scope.Data.cat == 1) {
                $scope.Data.class = 8;
            }
            data = {
                entrydate: $scope.Data.entrydate,
                payee: $scope.Data.payee,
                amount: $scope.Data.amount,
                num: $scope.Data.num,
                category: $scope.Data.cat,
                comment: $scope.Data.comment,
                acct: $scope.Data.account,
                status: $scope.Data.status,
                'class': $scope.Data.class,
                'id': $scope.Data.id
            };
            if ($scope.Data.delete == "1") {
                data['delete'] = 1;
            }
            saveTransaction(data);
        };

        $scope.addRefund = function () {
            data = {
                entrydate: $scope.Data.entrydate,
                payee: $scope.Data.payee,
                amount: $scope.Data.amount * -1,
                num: $scope.Data.num,
                category: $scope.Data.cat,
                comment: $scope.Data.comment,
                acct: $scope.Data.account,
                status: $scope.Data.status,
                'class': $scope.Data.class,
                'id': ''
            };
            saveTransaction(data);
        }
        saveTransaction = function (data) {
            console.log('saveTransaction()');
            console.log(data);
            if ($scope.Data.Splits != undefined) {
                for (var i = 0, len = $scope.Data.Splits.length; i < len; i++) {
                    if ($scope.Data.Splits[i] && parseFloat($scope.Data.Splits[i].amount) != 0) {
                        data['Category_' + (i + 1)] = $scope.Data.Splits[i].cat;
                        data['Class_' + (i + 1)] = $scope.Data.Splits[i].class;
                        data['Comment_' + (i + 1)] = '';
                        data['Amount_' + (i + 1)] = $scope.Data.Splits[i].amount;
                    }
                }
                data['CatSize'] = i;
            }

            infoChiSpinner.show();
            $http.post('/web/banking/transaction/save_data', JSON.stringify(data)).then(function (result) {
                infoChiSpinner.hide();
                // If delete, take to register page
                infoChiSpinner.hide();
                $scope.Data.status = 'Transaction ID: ' + result.data.id;
                if (result.data.id != undefined) {
                    $scope.Data.transaction_id = result.data.id;
                    $scope.setupTransaction();
                }
                if ($scope.Data.cat == 1) {
                    $scope.getSplitData();
                }
            });
        }

        $scope.deleteTransaction = function () {
            if (confirm('Are You Sure?')) {
                infoChiSpinner.show();
                $http.post('/web/banking/transaction/delete', {'id': $scope.Data.id}).then(function (result) {
                    location.href = "#!/banking/register/" + $scope.Data.account;
                    infoChiSpinner.hide();
                });
            }
        }

        $scope.setupTransaction = function () {
            if ($scope.Data.transaction_id == 0) {
                $scope.Data.tranid = "To Be Defined.";
                $scope.Data.num = '';
                today = new Date();
                $scope.Data.entrydate = '' + today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
                $scope.Data.account = $routeParams.account_id;
                $scope.$watch('Accounts', function () {
                    $scope.selAccount();
                });
            } else {
                $scope.Data.tranid = $scope.Data.transaction_id;
                // Load Transaction

                // infoChiSpinner.show();
                $http.get('/web/banking/transaction/' + $scope.Data.tranid).then(function (result) {
                    $scope.Data = angular.extend($scope.Data, result.data);
                    if ($scope.Data.class == '0') {
                        $scope.Data.class = 8;
                    }
                    $scope.$watch('Categories', function () {
                        $scope.selCategory($scope.Select, $scope.Data.cat);
                    });
                    $scope.$watch('Classes', function () {
                        $scope.selClass($scope.Select, $scope.Data.class);
                    });
                    $scope.$watch('Accounts', function () {
                        $scope.selAccount();
                    });
                    if ($scope.Data.cat == 1) {
                        $scope.getSplitData();
                    }

//              infoChiSpinner.hide();
                }, function (response) {
//              infoChiSpinner.hide();
                });
            }
        }
        $scope.filterClass = function (item) {
            // Active Items bypass tests
            if (item.active == "1") {
                return true;
            }
            // Must have entrydate defined
            if ($scope.Data.entrydate == '') {
                return true;
            }

            tran_date = new Date($scope.Data.entrydate);
            entry_date = new Date(item.date);
            if (tran_date > entry_date) {
                return true;
            }
            return false;
        }
        // Split Stuff
        $scope.evalSplit = function () {
            splitTotal = 0;
            for (var i = 0, len = $scope.Data.Splits.length; i < len; i++) {
                splitTotal += parseFloat($scope.Data.Splits[i].amount);
            }
            $scope.Data.splitTotal = mathRound(splitTotal, 3);

            splitTransaction = parseFloat($scope.Data.amount);
            $scope.Data.splitTransaction = splitTransaction;
            splitDifference = mathRound(splitTransaction - splitTotal, 3);
            $scope.Data.splitDifference = splitDifference;
        }
        addSplitLine = function (category, tclass, amount) {
            data = {
                cat: category,
                'class': tclass,
                comment: '',
                amount: amount
            };
            $scope.Data.Splits.push(data);
        }
        $scope.addSplitLine = function () {
            addSplitLine(19, 8, 0);
        }
        // Split Stuff
        setupSubClass = function () {
            $scope.Data.subclass = [{portion: '50', type: '%', amount: ''}, {
                portion: '50',
                type: '%',
                amount: ''
            }, {portion: '*', type: '%', amount: ''}];
        }
        // Fund Stuff
        //------------------------------------
        $scope.startup = function () {
            $scope.Data = {
                transaction_id: 0
            };
            $scope.Select = {Class: {}};
            $scope.Data.transaction_id = $routeParams.transaction_id;
            if ($routeParams.transaction_id != 0) {
                $scope.new_transaction = false;
            } else {
                $scope.new_transaction = true;
            }
            $scope.setupTransaction();
            setupSubClass();
        }

        mathRound = function(Amount, Precision) {
            var Multiplier = Math.pow(10,Precision)
            return Math.round(Amount * Multiplier)/Multiplier;
        }

        // Add Year
        // --------------------------
        function formatDate(date) {
            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;

            return [year, month, day].join('-');
        }

        $scope.addYear = function () {
            console.log('addYear()');
            // Create vars for later
            entrydate_orig = formatDate(new Date($scope.Data.entrydate));
            entrydate = new Date($scope.Data.entrydate);
            entrydate.setFullYear(entrydate.getFullYear() + 1);
            entrydate_new = formatDate(entrydate);
            // Write new values
            $scope.Data.entrydate = entrydate_new;
            if ($scope.Data.comment == undefined || $scope.Data.comment == '') {
                $scope.Data.comment = entrydate_orig;
            } else {
                $scope.Data.comment = entrydate_orig + ' - ' + $scope.Data.comment;
            }
        }


        $scope.startup();
        $scope.Select = {Categories: {}, Class: {}, Account: {}};
        $scope.getClasses();
        $scope.getAccounts();
        $scope.getCategories();
        $scope.$parent.setSecond('banking');
    }
]);

