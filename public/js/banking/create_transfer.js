infoChi.controller("bankingCreateTransferCtrl", ['$scope', '$http', '$localStorage', '$routeParams', 'infoChiSpinner',
    function ($scope, $http, $localStorage, $routeParams, infoChiSpinner) {
        // Get list from Server
        $scope.getAccounts = function () {
            $scope.Accounts = [];
            infoChiSpinner.show();
            $http.get('/web/banking/accounts').then(function (result) {
                $scope.Accounts = result.data;
                $scope.selAccountFrom();
                infoChiSpinner.hide();
            }, function (response) {
                infoChiSpinner.hide();
            });
        };

        $scope.choAccountFrom = function (item) {
            $scope.Data.accountFrom = item.id;
        };
        $scope.choAccountTo = function (item) {
            console.log('choAccountTo()');
            console.log(item);
            $scope.Data.accountTo = item.id;
        };
        $scope.selAccountFrom = function () {
            console.log('selAccountFrom');
            angular.forEach($scope.Accounts, function (value, key) {
                if (parseInt($scope.Data.accountFrom) === parseInt(value.id)) {
                    $scope.Select.AccountFrom = {name: value.name};
                }
            });
        };

        $scope.saveTransfer = function () {
            console.log('saveTransfer()');
            console.log($scope.Data);
            infoChiSpinner.show();
            $http.post('/web/banking/transaction/save_transfer', $scope.Data).then(function (result) {
                infoChiSpinner.hide();
            });
        };

        $scope.Select = {};
        $scope.Data = {accountFrom: $routeParams.account_id}
        today = new Date();
        $scope.Data.entrydate = '' + today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
        $scope.getAccounts();
        $scope.$parent.setSecond('banking');
    }
]);

