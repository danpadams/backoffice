<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTickersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickers', function (Blueprint $table) {
            $table->increments('id');
            $table->char('ticker', 5);
            $table->char('name', 255);
            $table->decimal('price', 5,2);
            $table->decimal('diff', 5,2);
            $table->decimal('diff_pct', 5, 2);
            $table->decimal('ma_50', 5,2);
            $table->decimal('ma_200', 5, 2);
            $table->decimal('dma_50',5,2);
            $table->decimal('dma_50_pct',5,2);
            $table->decimal('dma_200',5,2);
            $table->decimal('dma_200_pct',5,2);
            $table->integer('inactive')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickers');
    }
}
