<!--<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">-->
<!DOCTYPE HTML>
<html>
<head>
    <title>infoChi BackOffice</title>
    <script type="text/javascript" src="js/jquery-1.11.3.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular-route.js"></script>
    <script type="text/javascript" src="lib/ui-bootstrap-tpls-2.5.0.min.js"></script>
    <!--    <script type="text/javascript" src="js/jquery.autocomplete.js"></script>-->
<!--        <script type="text/javascript" src="js/jquery.tablesorter.min.js"></script>-->
<!--    	<script type="text/javascript" src="js/jquery.tablesorter.pager.js"></script>-->
    <!--	<script type="text/javascript" src="js/jquery.metadata.js"></script>-->
    <!--	<script type="text/javascript" src="js/jquery.validate.min.js"></script>-->
    <!--	<script type="text/javascript" src="js/notifyBox.js"></script>-->
    <!--	<script type="text/javascript" src="js/queueRunner.js"></script>-->
    <!--	<script type="text/javascript" src="js/jquery-ui-1.7.1/ui.core.min.js"></script>-->
    <!--	<script type="text/javascript" src="js/jquery-ui-1.7.1/ui.accordion.min.js"></script>-->

    <script type="text/javascript" src="/lib/ngStorage.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.3/angular-sanitize.js"></script>
    <script type="text/javascript" src="/lib/select.min.js"></script>
    <script type="text/javascript" src="/lib/angular-spinners.min.js"></script>
    <script type="text/javascript" src="/lib/angular-flash.js"></script>

    <script type="text/javascript" src="/lib/dirPagination.js"></script>


    <!--    <script type="text/javascript" src="/lib/angular-route.js"></script>-->
    <!--    <script type="text/javascript" src="/lib/ngStorage.js"></script>-->
    <!-- Style Sheets-->
<!--    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">-->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
<!--    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css">-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
          integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/datetimepicker.css">
    <link rel="stylesheet" type="text/css" href="css/selectize.default.css">
    <link rel="stylesheet" type="text/css" href="css/selectize.css">
    <link rel="stylesheet" type="text/css" href="css/infochi.css">

    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <script type="text/javascript">
        var infoChi = angular.module("infoChi", [
            "ngRoute",
            "ngStorage",
            "ngFlash",
            'ngSanitize',
            'ui.select',
            'angularSpinners',
            'angularUtils.directives.dirPagination',
            // 'ui.bootstrap.datetimepicker'
            'ui.bootstrap'
        ]);
    </script>

<!--    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"></script>-->

    <script type="text/javascript" src="js/routes.js"></script>
    <script type="text/javascript" src="js/infoChi.js"></script>
    <script type="text/javascript" src="js/infoChi-flash.js"></script>

    <!-- Base-->
    <script type="text/javascript" src="/js/menu.js"></script>
    <script type="text/javascript" src="/js/util/report-generator.js"></script>

    <!-- Pages-->
    <script type="text/javascript" src="/js/banking/schedule_transaction/index.js"></script>
    <script type="text/javascript" src="/js/banking/pending_transfer/index.js"></script>
    <script type="text/javascript" src="/js/banking/pending_transfer/entry.js"></script>
    <script type="text/javascript" src="/js/banking/accounts.js"></script>
    <script type="text/javascript" src="/js/banking/register.js"></script>
    <script type="text/javascript" src="/js/banking/transaction.js"></script>
    <script type="text/javascript" src="/js/banking/create_transfer.js"></script>
    <script type="text/javascript" src="/js/banking/reports/reports.js"></script>
    <script type="text/javascript" src="/js/banking/reports/report.js"></script>
    <script type="text/javascript" src="/js/banking/reports/monthly.js"></script>
    <script type="text/javascript" src="/js/banking/reports/jadaproperties.js"></script>
    <script type="text/javascript" src="/js/banking/reports/business.js"></script>
    <script type="text/javascript" src="/js/banking/reports/businesses.js"></script>
    <script type="text/javascript" src="/js/banking/reports/payee.js"></script>
    <script type="text/javascript" src="/js/banking/reports/transaction.js"></script>
    <script type="text/javascript" src="/js/banking/log_withholding/positions.js"></script>
    <script type="text/javascript" src="/js/banking/log_withholding/position.js"></script>
    <script type="text/javascript" src="/js/banking/log_withholding/entries.js"></script>
    <script type="text/javascript" src="/js/banking/log_withholding/entry.js"></script>
    <script type="text/javascript" src="/js/banking/log_withholding/transfer.js"></script>


    <script type="text/javascript" src="js/layaway/index.js"></script>
    <script type="text/javascript" src="js/layaway/view.js"></script>
    <script type="text/javascript" src="js/layaway/edit.js"></script>
    <script type="text/javascript" src="js/layaway/create.js"></script>

    <script type="text/javascript" src="js/inventory/index.js"></script>
    <script type="text/javascript" src="js/inventory/view.js"></script>
    <script type="text/javascript" src="js/inventory/edit.js"></script>

    <script type="text/javascript" src="/js/invest/index.js"></script>
    <script type="text/javascript" src="/js/invest/view.js"></script>
    <script type="text/javascript" src="/js/invest/equity/input.js"></script>
    <script type="text/javascript" src="/js/invest/mkt-index.js"></script>
    <script type="text/javascript" src="/js/invest/mkt-index-data.js"></script>

    <script type="text/javascript" src="/js/journal/index.js"></script>
    <script type="text/javascript" src="/js/journal/entry.js"></script>
    <script type="text/javascript" src="/js/journal/edit.js"></script>

    <script type="text/javascript" src="/js/tracker/reminders.js"></script>
    <script type="text/javascript" src="/js/tracker/reminder.js"></script>
    <script type="text/javascript" src="/js/tracker/projects.js"></script>
    <script type="text/javascript" src="/js/tracker/tickets.js"></script>
    <script type="text/javascript" src="/js/tracker/ticket.js"></script>

    <script type="text/javascript" src="/js/jada_properties/rental.js"></script>
    <script type="text/javascript" src="/js/jada_properties/rentals.js"></script>
    <script type="text/javascript" src="/js/jada_properties/util_bill.js"></script>
    <script type="text/javascript" src="/js/jada_properties/util_bills.js"></script>
    <script type="text/javascript" src="/js/jada_properties/util_class.js"></script>

    <script type="text/javascript" src="/js/mileage/entries.js"></script>
    <script type="text/javascript" src="/js/mileage/entry.js"></script>

    <script type="text/javascript" src="js/concordance/nodes.js"></script>
    <script type="text/javascript" src="js/concordance/topics.js"></script>
    <script type="text/javascript" src="js/concordance/authors.js"></script>

    <script type="text/javascript" src="js/items/index.js"></script>
    <script type="text/javascript" src="js/items/edit.js"></script>

    <!-- Server Config -->
    <script type="text/javascript" src="js/server_config/apache.js"></script>
    <script type="text/javascript" src="js/server_config/username.js"></script>
    <script type="text/javascript" src="js/server_config/database.js"></script>
    <script type="text/javascript" src="js/server_config/dns.js"></script>

    <link rel="shortcut icon" type="image/jpg" href="/favicon.png"/>
</head>
    <body ng-app="infoChi" style="margin: 0">
    <spinner name="mainSpinner" class="spinner">
        <h1>
            <i class="fa fa-spinner fa-spin fa-5x"></i><br>
            <span>Please Wait!</span>
        </h1>
    </spinner>
    <div ng-controller="menuCtrl">
        <h1>
            <img alt="infoChi Christian Computing" src="img/jesusinside.gif" border="0">
            InfoChi</h1>
        <div>
            <div ng-include="template.menu"></div>
            <div class="container-fluid">
                <flash-message>
                    <div class="well"></div>
                </flash-message>
                <ng-view></ng-view>
            </div>
        </div>
    </div>
    </body>
</html>
