<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();

Route::group(['scheme' => 'https', 'middleware' => ['auth']], function () {
    // Frontend
    Route::get('/', function () {
        return View::make('index');
    });
    Route::get('/home', function () {
        return View::make('index');
    });
    Route::get('/logout', 'Auth\LoginController@logout');

    // API Stuff
    Route::group(['namespace' => 'Web'], function () {
        Route::get('/web/accounts/{account_id}/edit', 'AccountsController@edit');
        Route::get('/web/accounts/{account_id}/view', 'AccountsController@view');
        Route::get('/web/accounts', 'AccountsController@index');
        Route::post('/web/accounts/save', 'AccountsController@save');
        Route::post('/web/accounts/create_entry', 'AccountsController@create_entry');

        Route::get('/web/inventory', 'InventoryController@index');
        Route::get('/web/inventory/{inventory_id}', 'InventoryController@view');
        Route::get('/web/inventory/item/{inventory_item_id}', 'InventoryItemController@view');
        Route::post('/web/inventory/item/{inventory_item_id}', 'InventoryItemController@saveData');

        Route::get('/web/equity', 'EquityController@getTickers');
        Route::get('/web/equity/mkt-index', 'EquityController@getMktIndex');
        Route::get('/web/equity/mkt-index/{index_num}', 'EquityController@getMktIndexData');
        Route::get('/web/equity/{ticker}', 'EquityController@getTicker');
        Route::get('/web/equity/{ticker}/notes', 'EquityController@getNotes');
        Route::post('/web/equity/save', 'EquityController@saveData');

        Route::get('/web/journals', 'JournalController@index');
        Route::get('/web/journals/{journal_id}', 'JournalEntryController@index');
        Route::get('/web/journal/{journal_entry_id}', 'JournalEntryController@view');
        Route::post('/web/journal/{journal_entry_id}', 'JournalEntryController@saveData');

        Route::get('/web/banking/schedule_transaction', 'BankingScheduleTransactionController@index');
        Route::get('/web/banking/pending_transfer', 'BankingPendingTransferController@index');
        Route::get('/web/banking/pending_transfer/summary', 'BankingPendingTransferController@summary');
        Route::post('/web/banking/pending_transfer/process', 'BankingPendingTransferController@process');

        Route::get('/web/banking/pending_transfer/{id}', 'BankingPendingTransferController@entry');
        Route::post('/web/banking/pending_transfer/save_data', 'BankingPendingTransferController@save_data');
    });
    Route::group(['namespace' => 'Tracker'], function () {
        Route::get('/web/tracker/tickets', 'TicketsController@index');
        Route::get('/web/tracker/tickets/{tracker_project_id}', 'TicketsController@index');
        Route::get('/web/tracker/projects', 'ProjectsController@index');
        Route::get('/web/tracker/reminders', 'RemindersController@index');
        Route::get('/web/tracker/frequency', 'FrequencyController@index');

        Route::get('/web/tracker/{prefix}/get_next_ticket/', 'TicketsController@get_next_ticket');
        Route::get('/web/tracker/ticket/{code}', 'TicketsController@ticket');
        Route::get('/web/tracker/ticket/{code}', 'TicketsController@ticket');
        Route::post('/web/tracker/ticket/save_data', 'TicketsController@save_data');

        Route::get('/web/tracker/reminder/{id}', 'RemindersController@get_reminder');
        Route::post('/web/tracker/reminder', 'RemindersController@post_reminder');

        Route::post('/web/tracker/save_comment', 'CommentsController@save_data');
        Route::get('/web/tracker/comment/{id}', 'CommentsController@index');
    });
    Route::group(['prefix' => 'web/jada_properties', 'namespace' => 'JaDaProperties'], function () {
        Route::get('/util_bills/{util_class_id}', 'UtilBillsController@index');
        Route::get('/util_bills', 'UtilBillsController@index');
        Route::get('/util_class', 'UtilClassController@index');

        Route::get('/bookings/{rental_property_id}', 'RentalBookingsController@index');
        Route::get('/booking/{id}', 'RentalBookingsController@booking');
        Route::post('/booking', 'RentalBookingsController@save_data');

        Route::get('/platforms', 'RentalPlatformsController@index');

        Route::get('/platforms', 'RentalPlatformsController@index');
    });
    Route::group(['prefix' => 'web/banking', 'namespace' => 'Banking'], function () {
        Route::get('/accounts', 'AccountsController@index');

        Route::get('/categories', 'CategoriesController@index');
        Route::get('/classes', 'ClassesController@index');
        Route::get('/transaction/{transaction_id}', 'TransactionController@entry');
        Route::get('/transaction/{transaction_id}/split_data', 'TransactionController@split_data');
        Route::post('/transaction/save_data', 'TransactionController@save_data');
        Route::post('/transaction/save_transfer', 'TransactionController@save_transfer');
        Route::post('/transaction/delete', 'TransactionController@delete');
        Route::get('/accounts/get_balance/{account_id}', 'AccountsController@get_balance');
        Route::post('/register/{transaction_id}/upd_item_status', 'RegisterController@upd_item_status');
        Route::post('/register/{account_id}/upd_register', 'RegisterController@upd_register');
        Route::get('/register/{account_id}/account', 'RegisterController@account');
        Route::get('/register/{account_id}', 'RegisterController@index');
//        Route::get('/register/{account_id}/status', 'RegisterController@index');
        Route::get('/reports/business/{id}', 'ReportsBusinessController@data');
        Route::get('/reports/business', 'ReportsBusinessController@index');
        Route::get('/reports/monthly/{report_id}', 'ReportsController@monthly');
        Route::get('/reports/transaction/{report_id}/{year}/{month}', 'ReportsController@transaction');
        Route::get('/reports/transaction/{report_id}', 'ReportsController@transaction');
        Route::get('/report/{report_id}', 'ReportsController@get_data');
        Route::post('/report/add_category', 'ReportsController@add_category');
        Route::post('/report/add_class', 'ReportsController@add_class');
        Route::post('/report/add_account', 'ReportsController@add_account');
        Route::post('/report/del_category', 'ReportsController@del_category');
        Route::post('/report/del_class', 'ReportsController@del_class');
        Route::post('/report/del_account', 'ReportsController@del_account');

        Route::post('/report', 'ReportsController@save_data');
        Route::get('/reports', 'ReportsController@index');
    });
    Route::group(['prefix' => 'web/mileage', 'namespace' => 'Mileage'], function () {
        Route::get('/entries', 'MileageController@entries');
        Route::get('/entry/{id}', 'MileageController@entry');
        Route::post('/entry/save_data', 'MileageController@save_data');
        Route::get('/mile_types', 'MileageController@getMileTypes');
    });
    Route::group(['prefix' => 'web/log_withholding', 'namespace' => 'LogWithholding'], function () {
        Route::get('/entries', 'LogWithholdingController@entries');
        Route::get('/entry/{id}', 'LogWithholdingController@entry');
        Route::post('/entry/save_data', 'LogWithholdingController@save_data');
        Route::get('/banking_classes', 'LogWithholdingController@getClasses');
        Route::get('/positions', 'LogWithholdingController@positions');
        Route::get('/position/{id}', 'LogWithholdingController@position');
    });

    Route::group(['prefix' => 'web/concordance', 'namespace' => 'Concordance'], function () {
        Route::post('/node/saveData', 'Nodes@save_data');
        Route::get('/node/types', 'Nodes@types');
        Route::get('/node/topics', 'Nodes@topics');
        Route::get('/node/books', 'Nodes@books');
        Route::get('/node/authors', 'Nodes@authors');
        Route::get('/node/{node_id}', 'Nodes@index');

        Route::post('/authors/saveData', 'Authors@save_data');
        Route::get('/authors/{author_id}', 'Authors@index');

        Route::post('/topics/saveData', 'Topics@save_data');
        Route::get('/topics', 'Topics@index');
        Route::get('/topics/{topic_id}', 'Topics@view');
    });
    Route::group(['prefix' => 'web/items', 'namespace' => 'Items'], function () {
        Route::get('/', 'ItemsController@index');
        Route::get('/{barcode_id}/barcode', 'ItemsController@barcode');
        Route::get('/{item_id}/found_in', 'ItemsController@found_in');
        Route::get('/{item_id}/contains', 'ItemsController@contains');
        Route::get('/contain', 'ItemsController@contain');
        Route::post('/container', 'ItemsController@choiceContainer');
        Route::post('/contains', 'ItemsController@choiceContains');
        Route::post('/save_data', 'ItemsController@save_data');
        Route::post('/remove_item_container', 'ItemsController@remove_item_container');
    });
    Route::group(['prefix' => 'web/server_config', 'namespace' => 'ServerConfig'], function () {
        Route::get('/website', 'WebsiteController@index');
    });
});
